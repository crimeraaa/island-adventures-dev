#define VERTEX_SHADER

uniform mat4 MatrixPVW;

attribute vec3 POSITION;
attribute vec3 TEXCOORD0_LIFE;
attribute vec4 DIFFUSE;

varying vec3 PS_POS;
varying vec2 PS_TEXCOORD;
varying vec4 PS_COLOUR;

#include "textureindex.glh"
#include "consts.glh"

void main() {
	vec2 texture_index = get_texture_index(TEXCOORD0_LIFE.z);
	vec3 pos = floor(POSITION + 0.5);

	gl_Position = MatrixPVW * vec4(pos, 1.0);

	PS_POS.xyz = pos;
	PS_TEXCOORD = MASK_START + (texture_index * MASK_INDEX_SIZE) + (TEXCOORD0_LIFE.xy * MASK_SIZE);
	PS_COLOUR = DIFFUSE;
	PS_COLOUR.rgb *= PS_COLOUR.a;
}

