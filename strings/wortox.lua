-- This speech is for Wortox
return {

	ACTIONFAIL =
	{
		REPAIRBOAT = 
		{
			GENERIC = "Oh I simply couldn't.",
		},
		EMBARK = 
		{
			INUSE = "I could hop across, I suppose",
		},
		INSPECTBOAT = 
		{
			INUSE = GLOBAL.STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.STORE.INUSE
		},
		OPEN_CRAFTING  = 
   		{
			FLOODED = "With much regret, it's far too wet.",
		}, 
	},
	
	ANNOUNCE_MAGIC_FAIL = "There's a time and place for everything.",
	
	ANNOUNCE_SHARX = "I've been tricked!",
	
	ANNOUNCE_TREASURE = "Ooh, what have we here?",
	ANNOUNCE_TREASURE_DISCOVER = "Treasure, how delightful!",
	ANNOUNCE_MORETREASURE = "We're on a roll, hyuyu!",
	ANNOUNCE_OTHER_WORLD_TREASURE = "Not from this world or the one below!",
	ANNOUNCE_OTHER_WORLD_PLANT = "It doesn't fit in this world. Like me!",
	
	ANNOUNCE_IA_MESSAGEBOTTLE =
	{
		"The message is long gone. And that's done.",
	},
	ANNOUNCE_VOLCANO_ERUPT = "Hyuyu... Whoopsie.",
	ANNOUNCE_MAPWRAP_WARN = "The mist is drawing close.",
	ANNOUNCE_MAPWRAP_LOSECONTROL = "I haven't the foggiest where it's taking me!",
	ANNOUNCE_MAPWRAP_RETURN = "Have I mist my mark? Hyuyu!",
	ANNOUNCE_CRAB_ESCAPE = "Slippery soul!",
	ANNOUNCE_TRAWL_FULL = "The toil's over!",
	ANNOUNCE_BOAT_DAMAGED = "Oh dear, this water bodes ill.",
	ANNOUNCE_BOAT_SINKING = "I'm up to my tail in water!",
	ANNOUNCE_BOAT_SINKING_IMMINENT = "Into the drink I go, hyuyu!",
	ANNOUNCE_WAVE_BOOST = "Hyuyu!",
	
	ANNOUNCE_WHALE_HUNT_BEAST_NEARBY = "I love a game of hide and seek!",
	ANNOUNCE_WHALE_HUNT_LOST_TRAIL = "You win this once, whale!",
	ANNOUNCE_WHALE_HUNT_LOST_TRAIL_SPRING = "Sabotaged by this realm's mushiness.",
	
	DESCRIBE = {
	
		FLOTSAM = "Flotsam and jetsam, bits and bobs.",
		SUNKEN_BOAT = 
		{
			GENERIC = "A talking bird partner! Think of all the pranks we could do!",
			ABANDONED = "So long, my feathered friend.",
		},
		SUNKEN_BOAT_BURNT = "That's just mean.",

		BOAT_LOGRAFT = "It's not so strong, but I don't have long.",
		BOAT_RAFT = "It's an improvement, I suppose.",
		BOAT_ROW = "Gently down the merry stream!",
		BOAT_CARGO = "I dislike being burdened while traveling.",
		BOAT_ARMOURED = "Delightfully decorated!",
		BOAT_ENCRUSTED = "A chalky boat with a rock-hard coat.",
		CAPTAINHAT = "Hyuyu, are we playing sailors?",

		BOAT_TORCH = "A light to lead me through the night.",
		BOAT_LANTERN = "Guide my way among the waves!",
		BOATREPAIRKIT = "To keep those boats afloat.",
		BOATCANNON = "Ready to fire, but decidedly unready to brimstone.",

		BOTTLELANTERN = "Little jittery, glittery souls inside.",
		BIOLUMINESCENCE = "See how they glitter in the night.",

		BALLPHIN = "Hyuyu, it's so badly behaved!",
		BALLPHINHOUSE = "A soggy home I shall never enter.",
		DORSALFIN = "For a nice afternoon of terrorizing mortals.",
		TUNACAN = "Looks like it preserves all but the best part.",

		JELLYFISH = "Now to sting some unsuspecting mortal.",
		JELLYFISH_DEAD = "It's a stinging little thing.",
		JELLYFISH_COOKED = "I'm afraid I'd rather not.",
		JELLYFISH_PLANTED = "A prankster of the sea, it seems to be.",
		JELLYJERKY = "Looks chewy. I hate chewing.",
		RAINBOWJELLYFISH = "I'll try not to suck you dry.",
		RAINBOWJELLYFISH_PLANTED = "A bright, colorful sight!",
		RAINBOWJELLYFISH_DEAD = "A sad end to my many-hued friend.",
		RAINBOWJELLYFISH_COOKED = "I guess these ones do look kind of good.",
		JELLYOPOP = "Mortals innovate when they're deprived of snacks.",

		CROCODOG = "Hyuyu, just what are you?",
		POISONCROCODOG = "Yikes! It packs sick bites!",
		WATERCROCODOG = "Someone's got wet fur smell, hyuyu.",
	
		PURPLE_GROUPER = "This one's mostly chin.",
		PIERROT_FISH = "Time to land on a plate, little soul.",
		NEON_QUATTRO = "A cool little fish, this one.",
		PURPLE_GROUPER_COOKED = "Hyuyu, how unsightly.",
		PIERROT_FISH_COOKED = "Bland, even in mortal food standarts.",
		NEON_QUATTRO_COOKED = "No soul left in this one.",
		TROPICALBOUILLABAISSE = "Is this a stew, or a magic brew?",

		FISH_FARM = 
		{
			EMPTY = "'Farming souls' sounds just a bit too cruel.",
			STOCKED = "Good night, my dears.",
			ONEFISH = "There's a fish, down there, in the deep.",
			TWOFISH = "Hyuyu, it's brought a friend!",
			REDFISH = "There's a third, they've formed a herd.",
			BLUEFISH  = "So many souls! I'll hate to part with them.",
		},
	
		ROE = "There's many little fish, slumbering, inside.",
		ROE_COOKED = "These poor fish children didn't even have souls yet.",
		CAVIAR = "Mortals consider this a fancy appetizer.",

		CORMORANT = "I don't think these are the ones that make good pie.",
		SEAGULL = "Ravenous, but not a raven.",
		SEAGULL_WATER = "Ravenous, but not a raven.",
		TOUCAN = "Hyuyu, this one's mostly beak.",
		PARROT = "We could be good friends, you and I.",
		PARROT_PIRATE = "Oh ho, you're a dashing one!",

		SEA_YARD =
		{
			ON = "It toils for me so that I can play instead.",
			OFF = "It is not working.",
			LOWFUEL = "Its tummy grumbles for some tar.",
		},
	
		SEA_CHIMINEA = 
		{
			EMBERS = "Soon to extinguish.",
			GENERIC = "It's an afloat flame.",
			HIGH = "Those are some spicy flames.",
			LOW = "It burns so low, so low!",
			NORMAL = "It burns safe from the vicious winds.",
			OUT = "Cinders, cinders, cinders.",
		}, 

		CHIMINEA = "So that I may warm up amidst the cruel storms.",

		TAR_EXTRACTOR =
		{
			ON = "Much better than getting it on my fur.",
			OFF = "It is ready to extract at a moment's notice.",
			LOWFUEL = "It could use some of its own produce, if you ask me.",
		},

		TAR = "I wouldn't prank with something this awful.",
		TAR_TRAP = "They won't be so quick with this goopy trick.",
		TAR_POOL = "It's an oceanic soup of black goop.",
		TARLAMP = "Better to bear the smell than the darkness.",
		TARSUIT = "Perhaps I'd rather be a soggy imp for the day.",

		PIRATIHATITATOR =
		{
			GENERIC = "To pillage the forbidden knowledge!",
			BURNT = "That's one way to deal with pirate magic.",
		},

		PIRATEHAT = "To strike fear to those far and near.",

		MUSSEL_FARM =
		{
			GENERIC = "There are many shellfish, down there, in the deep.",
			STICKPLANTED = "The mussels crave those sticks."
		},

		MUSSEL = "They don't have souls to be sucked.",
		MUSSEL_COOKED = "I think I'll go find a nice delicious soul instead.",
		MUSSELBOUILLABAISE = "A clam broth, for what that's worth.",
		MUSSEL_BED = "Time to get to bed, little ones. Hyuyu!",
		MUSSEL_STICK = "The mussels are sure to stick around with this. Hyuyu!",

		LOBSTER = "Hyuyu, it's a creature I shelldom care to sea!",
		LOBSTER_DEAD = "It is quite thoroughly dead.",
		LOBSTER_DEAD_COOKED = "The best part's already gone.",
		LOBSTERHOLE = "Even crustaceans need a place to sleep every now and den.",
		WOBSTERBISQUE = "Mortals seem to enjoy turning anything into a soup.",
        WOBSTERDINNER = "I'm afraid I don't seafood the way most do.",
		SEATRAP = "I'm a rather cunning imp.",

		BUSH_VINE =
		{
			BURNING = "Whoopsadoodle.",
			BURNT = "Well, that was fun.",
			CHOPPED = "It is quite thoroughly chopped.",
			GENERIC = "I sense no souls within, no no.",
		},
		VINE = "It's a fine vine of mine.",
		DUG_BUSH_VINE = "Back in the dirt, while it's unhurt.",
	
		ROCK_LIMPET =
		{
			GENERIC = "Those don't seem like they'd bear souls.",
			PICKED = "Gone, all gone.",
		},

		LIMPETS = "I hear these are a mortal delicacy.",
		LIMPETS_COOKED = "Definitely one of the more offensive mortal flavors.",
		BISQUE = "Mortals will turn anything into a soup.",

		MACHETE = "To chop and to slash!",
		GOLDENMACHETE = "Fit for a sophisticated imp!",

		THATCHPACK = "It won't hold much, but it might come in clutch.",
		PIRATEPACK = "Is there a nymph slipping in coins every now and then?",
		SEASACK = "Chilly, chilly as the morning waves!",

		SEAWEED_PLANTED =
        {
            GENERIC = "It's a seaweed indeed!",
            PICKED = "Gone, all gone.",
        },

		SEAWEED = "Perhaps I'll find a hungry mortal to pass this off to.",
		SEAWEED_COOKED = "I would rather have a pile of souls.",
		SEAWEED_DRIED = "I do not want it.",
		SEAWEED_STALK = "Not quite as whip-like as I'd like.",

		DUBLOON = "Pirate money is just as soulless.",
		SLOTMACHINE = "Who could resist such a test of fate?",
		
		SOLOFISH = "Pardon me if I don't give you belly rubs.",
		SOLOFISH_DEAD = "Poor little critter.",
		SWORDFISH = "It's a swimming swashbuckler!",
		SWORDFISH_DEAD = "I don't want to eat anything pointy.",
		CUTLASS = "It's a cut above the rest!",

		SUNKEN_BOAT_TRINKET_1 = "What a silly contraption!", --sextant
		SUNKEN_BOAT_TRINKET_2 = "It's a toy for little mortals.", --toy boat
		SUNKEN_BOAT_TRINKET_3 = "I hear these smell pleasant when lit.", --candle
		SUNKEN_BOAT_TRINKET_4 = "Ooh, I sense a tinge of magic about this.", --sea worther
		SUNKEN_BOAT_TRINKET_5 = "Not to toot my own horn, but I seem to be getting better at landing soles!", --boot
		TRINKET_IA_13 = "Not even the mortals want to open this one.", --orange soda
		TRINKET_IA_14 = "Ooohoho, mischief magicks!", --voodoo doll
		TRINKET_IA_15 = "To strum and sing.", --ukulele
		TRINKET_IA_16 = "I have no earthly clue what this is for.", --license plate
		TRINKET_IA_17 = "Not to toot my own horn, but I seem to be getting better at landing soles!", --boot
		TRINKET_IA_18 = "An artifact, keep the curses.", --vase
		TRINKET_IA_19 = "My head's clouded as it is.", --brain cloud pill
		TRINKET_IA_20 = "Mortals invent the strangest tools.", --sextant
		TRINKET_IA_21 = "It's a toy for little mortals.", --toy boat
		TRINKET_IA_22 = "I hear these smell pleasant when lit.", --wine candle
		TRINKET_IA_23 = "What in the world is this for?", --broken aac device
		EARRING = "It needs only a pit of fire to be cast into.",

		TURF_BEACH = "Floor or ceiling, depending on your perspective.",
		TURF_JUNGLE = "Floor or ceiling, depending on your perspective.",
		TURF_MAGMAFIELD = "Floor or ceiling, depending on your perspective.",
		TURF_TIDALMARSH = "Floor or ceiling, depending on your perspective.",
		TURF_ASH = "Floor or ceiling, depending on your perspective.",
		TURF_MEADOW = "Floor or ceiling, depending on your perspective.",
		TURF_VOLCANO = "Like a little bit of dear old home.",
		TURF_SWAMP = "Floor or ceiling, depending on your perspective.",
		TURF_SNAKESKIN = "That's a stylish red!",

		WHALE_BLUE = "How do you do? Why are you blue?",
		WHALE_CARCASS_BLUE = "Oh dear.",
		WHALE_WHITE = "This one's white! We're in for a fight!",
		WHALE_CARCASS_WHITE = "Well, that was fun.",
		WHALE_TRACK = "These must lead to fresh trouble.",
		WHALE_BUBBLES = "Bubbles, bubbles.",
		BLUBBERSUIT = "Perhaps those belong better in a mortal's stew.",
		BLUBBER = "Quite squishy!",
		HARPOON = "Impaling things feels terrifyingly natural.",

		SAIL_PALMLEAF = "No faster than hopping, but it will do.",
		SAIL_CLOTH = "I shall sail forth with this slick white cloth!",
		SAIL_SNAKESKIN = "The stripes complement the red, I say, I say.",
		SAIL_FEATHER = "To fly over the open seas!",
		IRONWIND = "It's brimming with wind energy.",

		BERMUDATRIANGLE = "Oh, neat! I've no clue where it'll lead.",
	
		PACKIM_FISHBONE = "Good fish sir, are you aware you've no soul?",
		PACKIM = "Good day to you, my ravenous friend.",

		TIGERSHARK = "What manner of seaborne abomination is that?",
		MYSTERYMEAT = "Goodness gracious, no no no!",
		SHARK_GILLS = "A souvenir, to remember fun times by.",
		TIGEREYE = "I thought it was meant to be a gemstone.",
		DOUBLE_UMBRELLAHAT = "It really is the peak of fashion, hyuyu.",
		SHARKITTEN = "Forgive me that I'd rather not pet you.",
		SHARKITTENSPAWNER = 
		{
			GENERIC = "The den of that most fearsome beast.",
			INACTIVE = "It's deserted.",
		},

		WOODLEGS_KEY1 = "A crumbling key we found at sea.",--Unused
		WOODLEGS_KEY2 = "I daresay we got on his good side!",--Unused
		WOODLEGS_KEY3 = "A key without a lock is like a ship without a dock.",--Unused
		WOODLEGS_CAGE = "Hyuyu, someone's been naughty!",--Unused

		CORAL = "How vibrant!",
		ROCK_CORAL = "I've no quarrel with the corals.",
		LIMESTONENUGGET = "And not a hint of citrus!",
		NUBBIN = "I'll tell you nothin', nubbin.",
		CORALLARVE = "I surmise it's not at full size.",
		WALL_LIMESTONE = "To keep you out, or to keep me in?",
		WALL_LIMESTONE_ITEM = "It's of no use there on the ground.",
		WALL_ENFORCEDLIMESTONE = "To keep you out, or to keep me in?",
		WALL_ENFORCEDLIMESTONE_ITEM = "It's of no use off the sea.",
		ARMORLIMESTONE = "It weights down on my chest tuft.",
		CORAL_BRAIN_ROCK = "Ooh, it tickles my little impy brain!",
		CORAL_BRAIN = "What a curious thing!",
		BRAINJELLYHAT = "So much knowledge! What power, what fun!",

		SEASHELL = "A souvenir, from the sea.",
		SEASHELL_BEACHED = "Oh swell, it's a shell!",
		ARMORSEASHELL = "Good for a bad chest tuft day, if nothing else.",

		ARMOR_LIFEJACKET = "Drowning would be a nasty way to lose my soul.",
		ARMOR_WINDBREAKER = "Must it come in pink? Red would be nicer, I think.",

		SNAKE = "Well 'tssssss' to you too!",
		SNAKE_POISON = "Do not bite me, oh please!",
		SNAKESKIN = "I still prefer fur to scales.",
		SNAKEOIL = "Hyuyu, mortals come up with strange pranks.",
		SNAKESKINHAT = "If only it had horn holes.",
		ARMOR_SNAKESKIN = "To keep my impish fur slick.",
		SNAKEDEN =
		{
			BURNING = "Whoopsadoodle.",
			BURNT = "Well, that was fun.",
			CHOPPED = "It has been thoroughly chopped.",
			GENERIC = "It's a home for the slithering sirs.",
		},

		OBSIDIANFIREPIT =
		{
			EMBERS = "Soon to extinguish.",
			GENERIC = "It's a pit meant for the fire.",
			HIGH = "Those are some really spicy flames.",
			LOW = "It burns so low, so low!",
			NORMAL = "Quite cozy!",
			OUT = "Cinders, cinders, cinders.",
		},

		OBSIDIAN = "It's red and bright, like me!",
		ROCK_OBSIDIAN = "Swinging a tool would be the errand of a fool.",
		OBSIDIAN_WORKBENCH = "A hot spot for doing some volcanic magic.",
		OBSIDIANAXE = "To chop and to set ablaze!",
		OBSIDIANMACHETE = "I do believe the color suits me.",
		SPEAR_OBSIDIAN = "Its magic makes fire as I swing it.",
		VOLCANOSTAFF = "An imp could pull off some nasty pranks with this.",
		ARMOROBSIDIAN = "It will keep my fur scald-free.",
		COCONADE =
		{
			BURNING = "Now's the time, it's do or die!",
			GENERIC = "A newfound use for a mortal's produce.",
		},

		OBSIDIANCOCONADE =
		{
			BURNING = "Now's the time, it's do or die!",
			GENERIC = "I wouldn't dare pass this off as a mortal snack. Much too cruel.",
		},

		VOLCANO_ALTAR =
		{
			GENERIC = "Keep it still, we will, we will.",
			OPEN = "The great volcano demands offerings!",
		},

		VOLCANO = "It reminds me of home, it does.",
		VOLCANO_EXIT = "Back to the mortal plane.",
		ROCK_CHARCOAL = "Pre-burnt boulders.",
		VOLCANO_SHRUB = "Someone played a prank on this tree.",
		LAVAPOOL = "I won't dip my toe in.",
		COFFEEBUSH =
		{
			BARREN = "What shall I burn to feed it?",
			WITHERED = "It needs ashes. Dust won't do.",
			GENERIC = "It thrives in the heat.",
			PICKED = "Gone, all gone.",
		},

		COFFEEBEANS = "Beans from a fiery bush we picked.",
		COFFEEBEANS_COOKED = "The fire brings out the magic within.",
		DUG_COFFEEBUSH = "It can't work its magic as it is.",
		COFFEE = "Sure to put a pep in someone's step!",

		ELEPHANTCACTUS =
		{
			BARREN = "A little ash, and it will be just as brash.",
			WITHERED = "I don't mind keeping it as it is.",
			GENERIC = "Hyuyu, what a nasty plant!",
			PICKED = "The greater prankster wins this one.",
		},

		DUG_ELEPHANTCACTUS = "One could devise a nasty surprise.",
		ELEPHANTCACTUS_ACTIVE = "Hyuyu, it's a prankster plant!",
		ELEPHANTCACTUS_STUMP = "The greater prankster wins this one.",
		NEEDLESPEAR = "Sharp as can be, as you can see.",
		ARMORCACTUS = "A decent prankster knows how to blend in.",
		
		TWISTER = "An elemental of wind!",
		TWISTER_SEAL = "Just how did you stir up so much trouble, little one?",
		TURBINE_BLADES = "I have no earthly clue what one does with this.",
		MAGIC_SEAL = "A treasure, to remember your ferocious gales by.",
		WIND_CONCH = "I do enjoy blowing this.",
		WINDSTAFF = "I go wherever the wind takes me. Literally!",

		DRAGOON = "A horrible brute, and a jock to the boot!",
		DRAGOONHEART = "Aside from the heat, I feel a slight beat.",
		DRAGOONSPIT = "I won't dip my toe in.",
		DRAGOONEGG = "We had best rid of it fast.",
		DRAGOONDEN = "An abode I'm not keen on intruding.",

		ICEMAKER = 
		{
			OUT = "And out it goes.",
			VERYLOW = "It needs some fuel, if it's to toil.",
			LOW = "It craves something to burn into ice.",
			NORMAL = "Chilly, chilly!",
			HIGH = "It may now dispense a large amount of ice.",
		},

		HAIL_ICE = "I could slide these into a mortal's shirt.",
	
		BAMBOOTREE =
		{
			BURNING = "Oopsie.",
			BURNT = "Well, that was fun.",
			CHOPPED = "Let's get a move on. Chop chop!",
			GENERIC = "I could chop it down if I wished.",
		},

		BAMBOO = "Bamboo sticks, strong yet slender.",
		FABRIC = "To rest my sweet little head on.",
		DUG_BAMBOOTREE = "Put in in the dirt, before it gets hurt!",
		
		JUNGLETREE =
		{
			BURNING = "Twiddle dee dee, a burning tree!",
			BURNT = "Well, that was fun.",
			CHOPPED = "A funny jungle prank.",
			GENERIC = "A lofty tree of vines and leaves.",
		},

		JUNGLETREESEED = "A teensy little baby tree!",
		JUNGLETREESEED_SAPLING = "Grow big and tall, or not at all.",
		LIVINGJUNGLETREE = "A tree as perplexing as it is vexing.",

		OX = "A majestic beast of curved horns.",
		BABYOX = "A sweet little one in momma's care.",--unused
		OX_HORN = "Hmph, mines are better.",
		OXHAT = "Horns to wear over your horns.",
		OX_FLUTE = "It plays a song for the world to weep along.",

		MOSQUITO_POISON = "Do I even have blood for you?!",
		MOSQUITOSACK_YELLOW = "What manner of blood is this?",

		STUNGRAY = "You're no fun at all!",
		POISONHOLE = "I wouldn't prank with something like that.",
		GASHAT = "It keeps nasty stenches away from one's snout.",

		ANTIVENOM = "A potent cure does reassure.",
		VENOMGLAND = "Now to land this gland in a brew of some sort.",
		POISONBALM = "Hyuyu, someone's getting crafty!",
		
		SPEAR_POISON = "I don't like this one bit, oh no.",
		BLOWDART_POISON = "This is the worst kind of prank!",

		SHARX = "I think nature played a prank on that one.",
		SHARK_FIN = "A severed fin that will never again swim.",
		SHARKFINSOUP = "It swam again after all, hyuyu!",
		SHARK_TEETHHAT = "It sits nicely upon my horns.",
		AERODYNAMICHAT = "How silly and ingenious!",

		IA_MESSAGEBOTTLE = "Ooh, a message!",
		IA_MESSAGEBOTTLEEMPTY = "An empty bottle for bits and bobs.",
		BURIEDTREASURE = "Ooh! Dig it up, dig it up!",

		SAND = "It's a handful of sand.",
		SANDDUNE = "Maybe they're all stones and I grew really big.",
		SANDBAGSMALL = "I'd rather not have my things get drenched.",
		SANDBAGSMALL_ITEM = "The flier dubs this 'ballast'.",
		SANDCASTLE =
		{
			SAND = "But will it weather the sands of time?",
			GENERIC = "Hyuyu, it's lovely little mortal kingdom!"
		},

		SUPERTELESCOPE = "Nothing can hide from this imp, hyuyu!",
		TELESCOPE = "It's the perfect tool to stalk mortals with.",
		
		DOYDOY = "I'd rather mess with a smarter mortal.",
		DOYDOYBABY = "Why you silly little thing, you're so cute!",
		DOYDOYEGG = "A meal fit for a mortal.",
		DOYDOYEGG_COOKED = "Poor little creature.",
		DOYDOYFEATHER = "Hyuyu, it's enormous!",
		DOYDOYNEST = "The humble abode of that most endearing bird.",
		TROPICALFAN = "Many large feathers to brave hot weather.",
	
		PALMTREE =
		{
			BURNING = "Oopsadoodle.",
			BURNT = "Well, that was fun.",
			CHOPPED = "A silly tree prank.",
			GENERIC = "To rest my hooves under.",
		},

		COCONUT = "It's nutty, like me!",
		COCONUT_HALVED = "It may be eaten now.",
		COCONUT_COOKED = "I hear it's tastier this way.",
		COCONUT_SAPLING = "You know how to grow!",
		PALMLEAF = "Hyuyu, fan me!",
		PALMLEAF_UMBRELLA = "To keep my head fur nice and dry.",
		PALMLEAF_HUT = "Some delightful shade that we made.",
		LEIF_PALM = "It is no longer bound to the sands!",

		CRAB = 
		{
			GENERIC = "Crabby, but not a hermit.",
			HIDDEN = "A slippery soul, this one.",
		},

		CRABHOLE = "The hole to which all crabbits go!",

		TRAWLNETDROPPED = 
		{
			SOON = "Soon it'll sink deep into the sea.",
			SOONISH = "I'll claim my haul, ere sea swallows it all.",
			GENERIC = "The trawl net floats for now.",
		},

		TRAWLNET = "Ooh, are we hunting for treasure?",
		IA_TRIDENT = "Holding a pitchfork feels disturbingly natural.",

		KRAKEN = "A gigas from the depths!",
		KRAKENCHEST = "The beast has left behind its trove.",
		KRAKEN_TENTACLE = "Keep those to yourself, you brutish beast.",
		QUACKENBEAK = "To gnaw no more.",
		QUACKENDRILL = "Much drier than digging with paws or claws.",
		QUACKERINGRAM = "I'll launch my attack with a mighty QUACK!",

		MAGMAROCK = "What a shock! It's a rock!",
		MAGMAROCK_GOLD = "Glitter, glitter, glitter.",
		FLAMEGEYSER = "Watch that flame!",

		TELEPORTATO_SW_RING = "It's a thing. For the thing!",--unused
		TELEPORTATO_SW_BOX = "It's a thing. For the thing!",--unused
		TELEPORTATO_SW_CRANK = "It's a thing. For the thing!",--unused
		TELEPORTATO_SW_POTATO = "It's a thing. For the thing!",--unused
		TELEPORTATO_SW_BASE = "How very, very curious!",--unused
		
		PRIMEAPE = "How do you do, little sir?",
		PRIMEAPEBARREL = "Goodness gracious, the stench!",
		MONKEYBALL = "It's so delightfully silly!",
		WILBUR_UNLOCK = "A refined ape in not-so great shape.",--unused
		WILBUR_CROWN = "I'm more of a clown than he who bears a crown.",--unused

		MERMFISHER = "Hyuyu, you're all work and no fun.",
		MERMHOUSE_FISHER = "The home of some dilligent fisherman.",

		OCTOPUSKING = "We may earn a favor if we offer him some labor.",
		OCTOPUSCHEST = "Thank-you, thank-you kind sir.",

		SWEET_POTATO = "I do not want it.",
		SWEET_POTATO_COOKED = "At least it's softer now.",
		SWEET_POTATO_PLANTED = "It's a tuber that's in the ground.",
		SWEET_POTATO_SEEDS = "Grow a seed and you shall feed!",
		SWEETPOTATOSOUFFLE = "I could be convinced to have a taste.",

		BOAT_WOODLEGS = "Hmm, I could've sworn I felt a curse lingering in there.",
		WOODLEGSHAT = "Not quite my style, I'm afraid.",
		SAIL_WOODLEGS = "A pirate needs his flag, of course.",

		PEG_LEG = "May the rest of the mortals never need it.",
		PIRATEGHOST = "A seafaring, swashbuckling soul!",

		WILDBORE = "Are you down to play with me?",
		WILDBOREHEAD = "Yikes.",
		WILDBOREHOUSE = "A house I may blow down.",

		MANGROVETREE = "What do I see? A seaborne tree!",
		MANGROVETREE_BURNT = "Hyuyu, how ironic!",

		PORTAL_SHIPWRECKED = "I've had enough of plane-hopping.",
		SHIPWRECKED_ENTRANCE = "The mortals are finally figuring out plane-hopping.",
		SHIPWRECKED_EXIT = "I'll admit, hopping like that is convinient.",

		TIDALPOOL = "I could stare at my reflection all day!",
		FISHINHOLE = "Extra fishy!",
		FISH_TROPICAL = "Ooh, the ones here are quite eye-catching.",
		TIDAL_PLANT = "A little leafy plant I see.",
		MARSH_PLANT_TROPICAL = "Move, I'm gazing here.",

		FLUP = "Eye can see you!",
		BLOWDART_FLUP = "Without its eye, it's just a 'fsh'. Fshhh.",

		SEA_LAB = "The bamboo legs seem a bit flimsy, they do.",
		BUOY = "A little mark to light my way.", 
		WATERCHEST = "It's a chest, nothing to impress.",

		LUGGAGECHEST = "It wouldn't hurt to take an itty bitty peek inside.",
		WATERYGRAVE = "It's sad. Full of regrets.",
		SHIPWRECK = "The crew is long gone.",
		BARREL_GUNPOWDER = "That's a dangerous prank!",
		RAWLING = "A soulless partner for a lonely mortal. Or is it?",
		GRASS_WATER = "It's waterlogged grass.",
		KNIGHTBOAT = "A knight in rust-proof armor!",

		DEPLETED_BAMBOOTREE = "Salted earth, none will return.",--unused?
		DEPLETED_BUSH_VINE = "Salted earth, none will return.",--unused?
		DEPLETED_GRASS_WATER = "It does not seem in the highest spirits.",--unused?

		WALLYINTRO_DEBRIS = "I had no hand in that. I promise!", --unused
		BOOK_METEOR = "A tome reminiscent of my home.",
		CRATE = "A crate we may desecrate.",
		SPEAR_LAUNCHER = "Shoot, and I'll move!",
		MUTATOR_TROPICAL_SPIDER_WARRIOR = "A tasty treat to those tiny terrors!",
		
		CHESSPIECE_KRAKEN = "I never knew trawling could get so fun!",
		CHESSPIECE_TIGERSHARK = "It does not leap, nor does it bite.",
		CHESSPIECE_TWISTER = "A storm in stone form.",
		CHESSPIECE_SEAL = "This sweet carved seal does have appeal.",

		--SWC
		BOAT_SURFBOARD = "I'd rather not risk getting drenched.",
		SURFBOARD_ITEM = "It belongs with the lax mortal.",

		WALANI = {
            GENERIC = "Hello hello, my amicable friend!",
            ATTACKER = "%s plays rough at times.",
            MURDERER = "That wasn't a prank! That mortal's dead!",
            REVIVER = "A sweet soul %s has!",
            GHOST = "Relax, you wouldn't even notice a nibble!",
            FIRESTARTER = "Ooh, have you been playing pranks, %s?",
		},

		WILBUR = {
            GENERIC = "Greetings, little sir %s!",
            ATTACKER = "Isn't that enough monkeying around, my friend?",
            MURDERER = "Have mercy, your majesty!",
            REVIVER = "%s is a compassionate ruler.",
            GHOST = "Don't get your hairs in a bunch, I won't eat you.",
            FIRESTARTER = "I didn't expect you to go that far, %s.",
		},

		WOODLEGS = {
            GENERIC = "Good day to you, %s! Tell me, have you any curses?",
            ATTACKER = "I didn't lie about any treasure! I promise!",
            MURDERER = "Eep! Don't hurt me, It'll curse you!",
            REVIVER = "Thanks for the double peg leg up! Hyuyu!",
            GHOST = "Do seafarer souls taste salty?",
            FIRESTARTER = "Hyuyu, that was a poorly thought-out prank.",
		},
	},
}
