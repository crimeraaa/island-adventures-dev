return
{
	ACTIONFAIL =
	{
        APPRAISE =
        {
            NOTNOW = "Th'captain always be busy!",
        },
	    REPAIR =
        {
            WRONGPIECE = "Thet don't belong thar.",
        },
	    BUILD =
        {
            MOUNTED = "Woodlegs won't be reachin' thar anytime soon.",
			HASPET = "I ain't havin' no more.",
			TICOON = "Arr! I be keepin' ta me hearty crew!.",
        },
		SHAVE =
		{
			AWAKEBEEFALO = "Woodlegs ain't no fool!",
			GENERIC = "It don't be shave day.",
			NOBITS = "Thar don't be nuthin'ta shave.",
			REFUSE = "Me beard ain't gonna tug, 'tis of a true pirate!",
			SOMEONEELSESBEEFALO = "Thet ain't no beast o'mine!",
		},
		STORE =
		{
			GENERIC = "It b'full.",
			NOTALLOWED = "Thet don't b'longin'thar.",
		    INUSE = "I ain't be a fan o'waitin'",
			NOTMASTERCHEF = "Woodlegs don't work in th'galley!",
		},
		CONSTRUCT =
        {
            INUSE = "Arr! It already be bein' built!",
            NOTALLOWED = "Thet ain't accordin' ta plans!",
            EMPTY = "I ain't buildin' ships with nuthin'!",
            MISMATCH = "Th'map don't be matchin' th'plans.",
        },
		RUMMAGE =
		{	
			GENERIC = "No need! Yarrr!",
			INUSE = "Let ol'Woodlegs get a peek o'treasure!",	
			NOTMASTERCHEF = "Woodlegs don't work in th'galley!",
		},
		UNLOCK =
        {
        	WRONGKEY = "Bah! This ain't b'th'right key!",
        },
		USEKLAUSSACKKEY =
        {
        	WRONGKEY = "Arrr! Th'key ain't be fer this treasure!",
        	KLAUS = "Th'beast be blockin' me from treasures!",
			QUAGMIRE_WRONGKEY = "This key be th'wrong fit! Bah!",
        },
		ACTIVATE = 
		{
			LOCKED_GATE = "It be locked.",
			HOSTBUSY = "Hark, bird o'feather! Hark!",
            CARNIVAL_HOST_HERE = "Thet bird be makin' off wit' our dubloons!",
            NOCARNIVAL = "Th'birds be makin' off wit' our dubloons!",
			EMPTY_CATCOONDEN = "Bah! Ain't no treasure!",
			KITCOON_HIDEANDSEEK_NOT_ENOUGH_HIDERS = "I can still counts 'em as far as I sees 'em!",
			KITCOON_HIDEANDSEEK_NOT_ENOUGH_HIDING_SPOTS = "There ain't nuthin' on this land!",
			KITCOON_HIDEANDSEEK_ONE_GAME_PER_DAY = "Hit th'sleepin' quarters, had 'nuff o'thet t'day!",
            MANNEQUIN_EQUIPSWAPFAILED = "It ain't wantin' ta don no bilge rat's garb!",
            PILLOWFIGHT_NO_HANDPILLOW = "Adorn yer cap'n wit' th'finest feathers ye can plunder!",
		},
		-- Keeping the duplicate around, but commented out, for reference. -crimeraaa
		OPEN_CRAFTING =
		{
            PROFESSIONALCHEF = "Woodlegs don't work in th'galley!",
			SHADOWMAGIC = "Only swabs would trust thet kinda devil magic.",
			FLOODED = "'Tis soggier than me ship cook's crumpet.",
		},
		-- OPEN_CRAFTING  = 
		-- {
		-- 	FLOODED = "'Tis soggier than me ship cook's crumpet.",
		-- },
        COOK =
        {
            GENERIC = "This be not th'time fer cookin'!",
            INUSE = "Th'galley be packed tanight!",
            TOOFAR = "'Tis leagues away!",
        },
		START_CARRAT_RACE =
        {
            NO_RACERS = "Tharr be no beasts ta set sail!",
        },
		DISMANTLE =
		{
			COOKING = "It be cookin' grub.",
			INUSE = "Ye ought ta give ol'Woodlegs a try at thet!",
			NOTEMPTY = "Arr! Which grub lubber ain't cleanin' up th'grub!",
        },
		FISH_OCEAN =
		{
			TOODEEP = "Bah! This ain't no true pirate's fishin' tool!",
		},
		OCEAN_FISHING_POND =
		{
			WRONGGEAR = "Th'rod b'fer true blue fishin'!",
		},
        GIVE =
        {
		    GENERIC = "Me wonts be doin' thet.",
            DEAD = "It be dead.",
            SLEEPING = "It be sleepin'.",
            BUSY = "It be ignorin' ol'Woodlegs!",
            ABIGAILHEART = "Best be leavin' th'spirits be.",
            GHOSTHEART = "Thet won't calm this ol'spirit!",
			NOTGEM = "Take this instead, not me treasure!",
            WRONGGEM = "Arrr, never be picky wit' ye gems!",
            NOTSTAFF = "Arrr, thet be th'incorrect staff!",
			MUSHROOMFARM_NEEDSSHROOM = "A 'shroom be needed.",
            MUSHROOMFARM_NEEDSLOG = "We be needin' a log o'sorts.",
			MUSHROOMFARM_NOMOONALLOWED = "These ain't yer average 'shrooms!",
            SLOTFULL = "Are ye donnin' two patches? There be already somethin' thar!",
            DUPLICATE = "I ain't 'ave much room in me noggin' fer dupes.",
            NOTSCULPTABLE = "Thet ain't be me brightest idea.",
			NOTATRIUMKEY = "This be wastin' me time!",
            CANTSHADOWREVIVE = "It'll be restin' fer now.",
			WRONGSHADOWFORM = "Thet'd be one funky ol'devil.",
			NOMOON = "I ain't seein' any moons doon 'ere!",
			PIGKINGGAME_MESSY = "Get th'swabs ta mop th'deck!",
			PIGKINGGAME_DANGER = "Nay th'time fer thet!",
			PIGKINGGAME_TOOLATE = "Dark be on th'horizon, nay time fer games.",
			CARNIVALGAME_INVALID_ITEM = "Arrr! Picky wit' yer gold, are ye?",
			CARNIVALGAME_ALREADY_PLAYING = "Let ol'Woodlegs show ye how ta play 'em!",
			SPIDERNOHAT = "No cap'ns hat'll be donned on ye t'day!",
			TERRARIUM_REFUSE = "Ain't just no doohickey will do it!",
            TERRARIUM_COOLDOWN = "Gots ta wait if yer wantin' gold!",
            NOTAMONKEY = "No pirate worth 'is salt speaks landlubbin' upper crust! Bah!",
            QUEENBUSY = "I ain't fer loiterin' about when thar's treasure about!",
        },
        GIVETOPLAYER = 
        {
        	FULL = "Load incomin'! Open th'hatches!",
            DEAD = "I'll bury it with ye.",
            SLEEPING = "Show a peg-leg, matey! Treasure be yours t'day!",
            BUSY = "Arrr, Tis somethin' here for ye.",
    	},
    	GIVEALLTOPLAYER = 
        {
        	FULL = "Open ye hatches!",
            DEAD = "I'll put 'em in ye grave.",
            SLEEPING = "Show a peg-leg, matey! Treasure on board!",
            BUSY = "Don't be ignorin' ol'Woodlegs! O'er here!",
    	},
        WRITE =
        {
            GENERIC = "Alls I can write be an \"X\"!",
            INUSE = "What be so important, lad? Ye know whar treasure be?",
        },
		DRAW =
        {
            NOIMAGE = "Ain't no 'X' ta mark anywheres.",
        },
        CHANGEIN =
        {
            GENERIC = "Woodlegs'll not be changin' t'day!",
            BURNING = "'Tis a roarin' fire!",
            INUSE = "Arr! Ye best be dressin' like a pirate!",
			NOTENOUGHHAIR = "Arr! 'Tis a hairless beast!",
            NOOCCUPANT = "Ain't no beast be hitched!",
        },
        ATTUNE =
        {
            NOHEALTH = "Ain't me brightest idea.",
        },
        MOUNT =
        {
            TARGETINCOMBAT = "It be in th'midst o'battle!!",
            INUSE = "Aye, these legs ain't be th'fastest.",
			SLEEPING = "Show a leg, matey! We gots ta set sail!",
        },
        SADDLE =
        {
            TARGETINCOMBAT = "Woodlegs ain't no fool!",
        },
        TEACH =
        {
            KNOWN = "Arrrr.",
            CANTLEARN = "Yarrrrrr!",
			WRONGWORLD = "Th'map don't be matchin' th'land.",
			--MapSpotRevealer/messagebottle
			MESSAGEBOTTLEMANAGER_NOT_FOUND = "This 'X' ain't be from 'round 'ere!",--Likely trying to read messagebottle treasure map in caves
			STASH_MAP_NOT_FOUND = "Whut bilge rat be writin' th'maps!?",-- Likely trying to read stash map  in world without stash
        },
		WRAPBUNDLE =
        {
            EMPTY = "Thar be nothin' ta wrap!",
        },
		PICKUP =
        {
			RESTRICTION = "Me can't make heads 'er tails o'this'un.",
			INUSE = "Ye ought ta give ol'Woodlegs a try at thet!",
			NOTMINE_YOTC =
            {
                "Arr! Yer in no crew o'mine!",
                "Ye ain't be apart o'me crew!",
            },
			FULL_OF_CURSES = "Woodlegs knows a cursed artifact when 'e sees'un!",
        },
		SLAUGHTER =
        {
            TOOFAR = "Yer too far away from th'slaughter!",
        },
        REPLATE =
        {
            MISMATCH = "Blast!", 
            SAMEDISH = "It don't be needin' double!", 
        },
		SAIL =
        {
        	REPAIR = "She be in a mighty fine condition!",
        },
        ROW_FAIL =
        {
            BAD_TIMING0 = "Can't row thet fast, lubbers!",
            BAD_TIMING1 = "Thet ain't proper rowin'!",
            BAD_TIMING2 = "Arrg! Get me a sail!",
        },
		LOWER_SAIL_FAIL =
        {
            "This ain't no proper sail!",
            "Whut kinda mast ain't be closin'!?",
            "Arrr! Thet be no mast o'mine!",
        }, 
        BATHBOMB =
        {
            GLASSED = "It be glassed.",
            ALREADY_BOMBED = "A cannon b'dropped in thar!",
        },
		GIVE_TACKLESKETCH =
		{
			DUPLICATE = "I ain't 'ave much room in me noggin' fer dupes.",
		},
		COMPARE_WEIGHABLE =
		{
			TOO_SMALL = "Bah! 'Tis a wee fish!",
			OVERSIZEDVEGGIES_TOO_SMALL = "If it ain't makin' ye sweat, it ain't 'nuff!",
		},
		STEER_BOAT = --CUSTOM
		{
			NOTSURFER = "Me legs ain't be made fer thet!",
		},
		PLANTREGISTRY_RESEARCH_FAIL =
        {
            GENERIC = "I knows all me needs ta know bout thet!",
            FERTILIZER = "'Tis poop. Nuthin' more.",
        },
        FILL_OCEAN =
        {
            UNSUITABLE_FOR_PLANTS = "Ye be insultin' me salty heart!",
        },
        POUR_WATER =
        {
            OUT_OF_WATER = "Need ta fetch me a new pale o'water!",
        },
        POUR_WATER_GROUNDTILE =
        {
            OUT_OF_WATER = "Need ta fetch me a new pale o'water!",
        },
        USEITEMON =
        {
            --GENERIC = "I can't use this on that!",

            --construction is PREFABNAME_REASON
            BEEF_BELL_INVALID_TARGET = "No matey o'mine!",
            BEEF_BELL_ALREADY_USED = "Th'beast ain't part o'me crew!",
            BEEF_BELL_HAS_BEEF_ALREADY = "Th'cabin quarters gots enough beasts!",
        },
        HITCHUP =
        {
            NEEDBEEF = "Needs me a beast, an' make 'er hairy!",
            NEEDBEEF_CLOSER = "Avast ye! Come o'er here!",
            BEEF_HITCHED = "E's hitched!",
            INMOOD = "Arr. E's a moody'un.",
        },
        MARK = 
        {
            ALREADY_MARKED = "Already gots gold in this'un.",
            NOT_PARTICIPANT = "Bah! It ain't no contest wit' out ol'Woodlegs!",
        },
        YOTB_STARTCONTEST = 
        {
            DOESNTWORK = "Arr! Show up and show me yer gold!",
            ALREADYACTIVE = "Plunderin' elsewaters.",
        },
        YOTB_UNLOCKSKIN = 
        {
            ALREADYKNOWN = "I's know it, I knows I do!",
        },
		CARNIVALGAME_FEED =
        {
            TOO_LATE = "Blast! Th'game be rigged!",
        },
		HERD_FOLLOWERS =
        {
            WEBBERONLY = "Ain't no lubber gots respect fer ol'Woodlegs 'round 'ere!",
        },
        HARVEST =
        {
            DOER_ISNT_MODULE_OWNER = "Ain't interested in listenin' to a cap'n, are ye?",
        },

		-- IA Quotes
		REPAIRBOAT =
        {
            GENERIC = "It be jus'like new!",
        },
		EMBARK = 
		{
			INUSE = "Th't be taken."
		},
		INSPECTBOAT = 
		{
			INUSE = GLOBAL.STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.STORE.INUSE
		},
		FISH_FLOTSAM = 
        {
			TOOFAR = "I be missin' extending arms.",
		},
	},
	ANNOUNCE_CANNOT_BUILD =
	{
		NO_INGREDIENTS = "I be missin' th'needed parrrts.",
		NO_TECH = "Thet ain't locked up in me noggin'!",
		NO_STATION = "Bah! Too many science-do-its whuts-its!",
	},
	ACTIONFAIL_GENERIC = "Me can'ts be doin' thet.",
    ANNOUNCE_BOAT_LEAK = "She's takin' on water!",
	ANNOUNCE_BOAT_SINK = "Be this th'day th'sea claims me?",
	ANNOUNCE_MOUNT_LOWHEALTH = "Th'beast be worse fer th'wear!",
	ANNOUNCE_DIG_DISEASE_WARNING = "Th'roots were rottin' 'ere!",
    ANNOUNCE_PICK_DISEASE_WARNING = "Thet plant be catchin' scurvy.",
	ANNOUNCE_ACCOMPLISHMENT = "Glory fer ol'Woodlegs!",
	ANNOUNCE_ACCOMPLISHMENT_DONE = "If only me shipmates could see me now!",
	ANNOUNCE_ADVENTUREFAIL = "Thet didn't go by plans. Might'n try again.",
	ANNOUNCE_BEES = "Back ye tiny winged devils!",
	ANNOUNCE_BOOMERANG = "Infernal weapon!",
	ANNOUNCE_BURNT = "Ouch!",
	ANNOUNCE_CANFIX = "\nI can fix this, methinks.",
	ANNOUNCE_CHARLIE = "Whut'n th'blazes was thet!",
	ANNOUNCE_CHARLIE_ATTACK = "I be bitten!",
	ANNOUNCE_COLD = "ArrrrrrBrrrrrrr!",
	ANNOUNCE_CRAFTING_FAIL = "I be missin' th'needed parrrts.",
	ANNOUNCE_DAMP = "Me britches be soaked!",
	ANNOUNCE_DEERCLOPS = "Some big devil be upon me!",
	ANNOUNCE_CAVEIN = "Hit th'deck!",
	ANNOUNCE_ANTLION_SINKHOLE = 
	{
		"Arrr me pegs givin' oot?",
		"Th' ground be belchin'!",
		"Th' earth be crackin'!",
	},
	ANNOUNCE_ANTLION_TRIBUTE =
	{
        "They forced me ta give ye one o'me shiny treasures.",
        "Take 'er n'get oota here!",
        "This be hard fer ol'Woodlegs ta do, but only fer ye.",
	},
	ANNOUNCE_SACREDCHEST_YES = "Glory fer ol'Woodlegs!",
	ANNOUNCE_SACREDCHEST_NO = "Give me yer treasures! Woodlegs'll give 'em a good 'ne!",
	ANNOUNCE_DUSK = "It be gettin' late. Dark be on th'horizon.",
	ANNOUNCE_EAT =
	{
		GENERIC = "Arrr thet be good.",
		INVALID = "Me won't be eatin' thet.",
		PAINFUL = "Me don't feel so good.",
		SPOILED = "Thet don't be agreein' wit' me guts.",
		STALE = "Thet were past th'freshness date.",
	    YUCKY = "Thet be bad eatin'!",
	},
	ANNOUNCE_ENCUMBERED =
    {
        "Luggin' a haul...!",
        "Boat load's on th'way...!",
        "Nay prey, nay pay, they say... Yarr...",
        "Woodlegs gots a fresh idea fer punishment out at sea, he do...",
        "Arrrr...",
        "Part th'sea... fer ol'Woodlegs...",
        "It be worth its weight in gold...",
        "Haulin' me gold gives Woodlegs a wooden leg up on th'others...!",
        "Arrr... Remind me ta... tie me a raft to me ship's stern...",
        "I can hear th'crack o'me bones... Arr!",
    },
	ANNOUNCE_ATRIUM_DESTABILIZING = 
    {
		"Get oot'a th'way!!",
		"'Tis nigh time me be leavin' this dread place.",
		"Th'cave dwellers be belchin'!",
	},
	ANNOUNCE_RUINS_RESET = "Th'beasts return!",
	ANNOUNCE_SNARED = "Its got ol'Woodlegs in its locks!",
	ANNOUNCE_SNARED_IVY = "Ye ain't takin' Woodlegs down, lubber! Yarhar!",
	ANNOUNCE_REPELLED = "It be blockin' me shots!",
	ANNOUNCE_THURIBLE_OUT = "Blast!",
	ANNOUNCE_ENTER_DARK = "It be dark!",
	ANNOUNCE_ENTER_LIGHT = "I's can see again!",
	ANNOUNCE_FREEDOM = "I be free!",
	ANNOUNCE_HIGHRESEARCH = "Ol'Woodlegs noggin' be workin' today!",
	ANNOUNCE_HOT = "Needs me a... ice chip er two...",
	ANNOUNCE_HOUNDS = "Whut be thet noise signallin'?",
	ANNOUNCE_WORMS = "Whut be those rumbles comin' from th'ground?",
	ANNOUNCE_HUNGRY = "I be hungry...",
	ANNOUNCE_HUNT_BEAST_NEARBY = "Th'tracks be fresh. Th'devil be near hereabouts.",
	ANNOUNCE_HUNT_LOST_TRAIL = "Th'trail be lost.",
	ANNOUNCE_HUNT_LOST_TRAIL_SPRING = "Th'trail be washed out.",
	ANNOUNCE_HUNT_START_FORK = "Avast! Th'trails be treacherous hereabouts!",
    ANNOUNCE_HUNT_SUCCESSFUL_FORK = "Hark! Th'devil be in our grasps!",
    ANNOUNCE_HUNT_WRONG_FORK = "Arrr! Be thet mutiny me nose pickin' up?",
    ANNOUNCE_HUNT_AVOID_FORK = "We be smooth sailin'.",
	ANNOUNCE_INSUFFICIENTFERTILIZER = "De ye need more poop, plant?",
	ANNOUNCE_INV_FULL = "I can't be luggin' anymore things!",
	ANNOUNCE_KNOCKEDOUT = "Arrrr me head...",
	ANNOUNCE_LIGHTNING_DAMAGE_AVOIDED = "Ye didn't get ol'Woodlegs, sky bolts!",
	ANNOUNCE_LOWRESEARCH = "This ol'dawg didn't be learnin' any new tricks thar.",
	ANNOUNCE_MOSQUITOS = "Back ye blood-sucking devils!",
	ANNOUNCE_NODANGERSIESTA = "This ain't be th'time fer nappin'.",
	ANNOUNCE_NOWARDROBEONFIRE = "Thet be a poor idea, lad!",
    ANNOUNCE_NODANGERGIFT = "No presents fer th'wicked.",
	ANNOUNCE_NOMOUNTEDGIFT = "Th'steed be blockin' th'way.",
	ANNOUNCE_NODANGERSLEEP = "This ain't be th'time fer snoozin'.",
	ANNOUNCE_NODAYSLEEP = "It be too bright ta shut me eye.",
	ANNOUNCE_NODAYSLEEP_CAVE = "Me ain't be tired.",
	ANNOUNCE_NOHUNGERSIESTA = "I be too hungry fer a nap.",
	ANNOUNCE_NOHUNGERSLEEP = "Me tummy be grumblin' too loud ta sleep.",
	ANNOUNCE_NONIGHTSIESTA = "Night be fer slumber not fer siestas.",
	ANNOUNCE_NONIGHTSIESTA_CAVE = "This ain't th'sorta place fer nappin'.",
	ANNOUNCE_NOSLEEPONFIRE = "Me don't likes th'idea o'a burnin' bed.",
	ANNOUNCE_NOSLEEPHASPERMANENTLIGHT = "Yer shinin' bright as a lighthouse, and me ship's runnin' aground!",
	ANNOUNCE_NODANGERAFK = "Woodlegs'll never back down from a good fightin'!",
	ANNOUNCE_NO_TRAP = "No trouble!",
	ANNOUNCE_PECKED = "Keep yer beak ta yerself!",
	ANNOUNCE_QUAKE = "Th'world be belchin'.",
	ANNOUNCE_RESEARCH = "Th'wheels be turnin'.",
	ANNOUNCE_SHELTER = "Ye done right by me, tree.",
	ANNOUNCE_SOAKED = "Me soaked ta th'bones!",
	ANNOUNCE_WASHED_ASHORE = "Th'sea won't be claimin' Woodlegs t'day! Yarrr!",
	ANNOUNCE_THORNS = "Ye poky devils ye!",
	ANNOUNCE_TOOL_SLIP = "Slipped right from me hands!",
	ANNOUNCE_TORCH_OUT = "Me light went out!",
	ANNOUNCE_FAN_OUT = "Me fan be gone!",
    ANNOUNCE_COMPASS_OUT = "Where me trusty gadget go?",
	ANNOUNCE_TRAP_WENT_OFF = "Blast!",
	ANNOUNCE_UNIMPLEMENTED = "It ain't be ready yet.",
	ANNOUNCE_WET = "Me pirate blouse be takin' on water.",
	ANNOUNCE_WETTER = "I likes me water ta stay 'neath me boat.",
	ANNOUNCE_WORMHOLE = "Me must be oot me mind.",
	ANNOUNCE_TOWNPORTALTELEPORT = "Gots dust in me pirate blouse.",
	ANNOUNCE_TOADESCAPING = "Catch th'demon! It be escapin'!",
	ANNOUNCE_TOADESCAPED = "Th'demon be goin' ta bed.",
	--
    ANNOUNCE_TREASURE_COIN =
    {
        "Treasure! More'n more treasure!!",
        "Me pirate sense be tinglin'.",
        "Arrr! Avert ye eyes!",
        "Dropped 'un!",
        "A bit o'coin!",
        "Thet be Woodlegs' treasure!",
        "Me coins be slippin' oot me hat!",
        "Me eyes arrre doon 'ere!",
        "Piece o'gold!",
        "Me silver!",
    },
	--
	ANNOUNCE_DESPAWN = "Treasure be callin' me name! Whut be thet blindin' light?",
	ANNOUNCE_GHOSTDRAIN = "Arrr... Me head...",
	ANNOUNCE_PETRIFED_TREES = "Thet be th'sound o'lost opportunities!",
	ANNOUNCE_KLAUS_ENRAGE = "Woodlegs'll be runnin' as fast as 'is pegs can carry 'im!",
	ANNOUNCE_KLAUS_UNCHAINED = "It be free from its locks!",
	ANNOUNCE_KLAUS_CALLFORHELP = "It be callin' its crew, stand ye ground!",
	--hallowed nights
    ANNOUNCE_SPOOKED = "Arr! Whut in th'blazes was thet?",
	ANNOUNCE_BRAVERY_POTION = "Yarr! I's ain't afraid of no trees!",
	ANNOUNCE_MOONPOTION_FAILED = "Bah! This b'witched potion ain't b'workin'!",
	--winter's feast
	ANNOUNCE_EATING_NOT_FEASTING = "Woodlegs don't fancy sharin' 'is feast...",
	ANNOUNCE_WINTERS_FEAST_BUFF = "Yarr! I be a glowin' bit'o'silver!",
	ANNOUNCE_IS_FEASTING = "Yo-Ho-Ho an' a bottle o'rum!",
	ANNOUNCE_WINTERS_FEAST_BUFF_OVER = "Arr! 'Least me treasure still sparklin'!",
    --lavaarena event
    ANNOUNCE_REVIVING_CORPSE = "Let ol'Woodlegs patch ya up!",
    ANNOUNCE_REVIVED_OTHER_CORPSE = "Ain't no scuff ruff 'nuff fer no pirate!",
    ANNOUNCE_REVIVED_FROM_CORPSE = "Thanks, me mateys!",
	--quagmire event
    QUAGMIRE_ANNOUNCE_NOTRECIPE = "Thet ain't no grub!",
    QUAGMIRE_ANNOUNCE_MEALBURNT = "Arrr! Too much fire!",
    QUAGMIRE_ANNOUNCE_LOSE = "Ol'Woodlegs hear th'sea callin' 'im...",
    QUAGMIRE_ANNOUNCE_WIN = "Yo-ho-ho! More treasure fer Woodlegs!",

	ANNOUNCE_FLARE_SEEN = "A ship be callin' fer help!",
	ANNOUNCE_MEGA_FLARE_SEEN = "Arr! Them's fightin' word's in th'sky!",
    ANNOUNCE_OCEAN_SILHOUETTE_INCOMING = "Avast ye!",
	ANNOUNCE_MOONALTAR_MINE =
	{
		GLASS_MED = "Thar be treasure within!",
		GLASS_LOW = "Yer treasure's be nearly mine!",
		GLASS_REVEAL = "Here b'treasure!",
		IDOL_MED = "Thar be treasure within!",
		IDOL_LOW = "Yer treasure's be nearly mine!",
		IDOL_REVEAL = "Here b'treasure!",
		SEED_MED = "Thar be treasure within!",
		SEED_LOW = "Yer treasure's be nearly mine!",
		SEED_REVEAL = "Here b'treasure!",
	},
	--
	ANNOUNCE_ATTACH_BUFF_ELECTRICATTACK    = "I be channellin' th'sky bolts now! Yarrr!",
    ANNOUNCE_ATTACH_BUFF_ATTACK            = "Arrrrrr!",
    ANNOUNCE_ATTACH_BUFF_PLAYERABSORPTION  = "I'd like ta see 'em take on Woodlegs now!",
    ANNOUNCE_ATTACH_BUFF_WORKEFFECTIVENESS = "Yarr! I could sail all th'seas and plunder all th'treasures!",
    ANNOUNCE_ATTACH_BUFF_MOISTUREIMMUNITY  = "No sea will be slickin' ol'Woodlegs now!",
	ANNOUNCE_ATTACH_BUFF_SLEEPRESISTANCE   = "Th' cap'n never takes 'is eyes off th'wheel!",

    ANNOUNCE_DETACH_BUFF_ELECTRICATTACK    = "Arr! Me bolts be fizzlin'!",
    ANNOUNCE_DETACH_BUFF_ATTACK            = "Only gots so much lootin' an' fightin' in these ol'bones.",
    ANNOUNCE_DETACH_BUFF_PLAYERABSORPTION  = "Arr... ain't as young as I used ta be.",
    ANNOUNCE_DETACH_BUFF_WORKEFFECTIVENESS = "Even th'pirate captain's gots ta have a break now an'then.",
    ANNOUNCE_DETACH_BUFF_MOISTUREIMMUNITY  = "Arrr, me old bones ain't be waterproof afterall.",
	ANNOUNCE_DETACH_BUFF_SLEEPRESISTANCE   = "Arrr... me noggin' be groggin'.",
	
	ANNOUNCE_OCEANFISHING_LINESNAP = "Arrr! Me line be bitten!",
	ANNOUNCE_OCEANFISHING_LINETOOLOOSE = "Tighten down th'hatches!",
	ANNOUNCE_OCEANFISHING_GOTAWAY = "Bah! Ain't no pirate worth 'is salt loses a line!",
	ANNOUNCE_OCEANFISHING_BADCAST = "Arrrgh!",
	ANNOUNCE_OCEANFISHING_IDLE_QUOTE = 
	{
		"Bring Woodlegs th'fish!",
		"Come ta Woodlegs! I's dun bite! Ain't got th'teeth fer it!",
		"Arrr... Whar be th'fish!",
		"Fishin's fer patient sailors. And I ain't no sailor! And I ain't patient!",
	},

	ANNOUNCE_WEIGHT = "Weight: {weight}",
	ANNOUNCE_WEIGHT_HEAVY  = "Weight: {weight}\nMatches me weight in gold!",
	ANNOUNCE_WINCH_CLAW_MISS = "Arrgh! It be missin' th'treasure!",
	ANNOUNCE_WINCH_CLAW_NO_ITEM = "Me hates comin' up empty handed!",
    ANNOUNCE_WEAK_RAT = "Ye be a sickly scallywag.",

    ANNOUNCE_CARRAT_START_RACE = "Get th'gold! Yarrr!",

    ANNOUNCE_CARRAT_ERROR_WRONG_WAY = {
        "Fix yer sails! They be blowin' backwards!",
        "Turn yer ship around, matey!",
    },
    ANNOUNCE_CARRAT_ERROR_FELL_ASLEEP = "Arrr! A pirate nevers naps when treasures a brewin'!",    
    ANNOUNCE_CARRAT_ERROR_WALKING = "Get goin'!",    
    ANNOUNCE_CARRAT_ERROR_STUNNED = "Arr! Need some peg-legs?",
	ANNOUNCE_POCKETWATCH_PORTAL = "Steer clear o'me rear! Arrr!",
	ANNOUNCE_ARCHIVE_NEW_KNOWLEDGE = "Bah! Get th'lubbers ta build ye this fancy boat!",
    ANNOUNCE_ARCHIVE_OLD_KNOWLEDGE = "I's be knowin' thet 'un!",
	ANNOUNCE_ARCHIVE_NO_POWER = "It be needin' a spot o'rum!",
	
	ANNOUNCE_PLANT_RESEARCHED =
    {
        "Bah! Land lubbin' learnin'!",
    },

    ANNOUNCE_PLANT_RANDOMSEED = "Best be growin', lest we be knowin'!",

    ANNOUNCE_FERTILIZER_RESEARCHED = "Aye, thet's poop.",

	ANNOUNCE_FIRENETTLE_TOXIN = 
	{
		"Me stomach's feelin' th'volcano now!",
		"ARR! I be blowin' me top!",
	},
	ANNOUNCE_FIRENETTLE_TOXIN_DONE = "Thet spice near done me tongue in, it did.",

	ANNOUNCE_TALK_TO_PLANTS = 
	{
        "Get ta growin'!",
        "If ye wantin' in me crew ye gots ta earn it!",
		"Stuck in dirt ain't no way ta be!",
        "Ye'll grow ta love th'ocean blue one day, I knows it.",
        "Me crew ain't acceptin' shrimps, best get growin'!",
	},

	ANNOUNCE_KITCOON_HIDEANDSEEK_START = "Avast ye! 'ere comes ol'Woodlegs!",
	ANNOUNCE_KITCOON_HIDEANDSEEK_JOIN = "Let ol'Woodlegs do th'seekin' for ye!",
	ANNOUNCE_KITCOON_HIDANDSEEK_FOUND = 
	{
		"Found me a purrin' treasure!",
		"Yarrr!!",
		"Got ye, ya scallywag!",
		"Shiver me timbers!",
	},
	ANNOUNCE_KITCOON_HIDANDSEEK_FOUND_ONE_MORE = "Greatest kit what ever hid!",
	ANNOUNCE_KITCOON_HIDANDSEEK_FOUND_LAST_ONE = "Hark! This purrin' treasure be mine!",
	ANNOUNCE_KITCOON_HIDANDSEEK_FOUND_LAST_ONE_TEAM = "{name} got th'gold!",
	ANNOUNCE_KITCOON_HIDANDSEEK_TIME_ALMOST_UP = "Woodlegs' time ne'er ends! Arrr!",
	ANNOUNCE_KITCOON_HIDANDSEEK_LOSEGAME = "Whut kind o'battle be one whut Woodlegs loses!?",
	ANNOUNCE_KITCOON_HIDANDSEEK_TOOFAR = "Too far out ta spy wit' th'eye!",
	ANNOUNCE_KITCOON_HIDANDSEEK_TOOFAR_RETURN = "Me furball sense be tinglin'!",
	ANNOUNCE_KITCOON_FOUND_IN_THE_WILD = "Whut has ol'Woodlegs drudged up t'day?",

	ANNOUNCE_TICOON_START_TRACKING	= "Ye gots a scent fer treasure, do ye?",
	ANNOUNCE_TICOON_NOTHING_TO_TRACK = "He be fibbin' bout thet gold sniffin'!",
	ANNOUNCE_TICOON_WAITING_FOR_LEADER = "Who died and made ye cap'n? Treasure ye say? Well what're ye waitin' for!",
	ANNOUNCE_TICOON_GET_LEADER_ATTENTION = "Arr! This best be good an'shiny!",
	ANNOUNCE_TICOON_NEAR_KITCOON = "Whut do ye spy wit'yer eye?",
	ANNOUNCE_TICOON_LOST_KITCOON = "Blast! We been beat to th'loot!",
	ANNOUNCE_TICOON_ABANDONED = "Let me pirate senses take me where thar winds blows!",
	ANNOUNCE_TICOON_DEAD = "Dead cats ain't got no trails!",
    -- YOTB
    ANNOUNCE_CALL_BEEF = "Git o'er 'ere!",
    ANNOUNCE_CANTBUILDHERE_YOTB_POST = "Ain't no judge be 'ere!",
    ANNOUNCE_YOTB_LEARN_NEW_PATTERN =  "Thet'd make a pretty flag, it would!",

	ANNOUNCE_EYEOFTERROR_ARRIVE = "Avast ye! Eyes ahead! Arrr! Not yer eyes!",
    ANNOUNCE_EYEOFTERROR_FLYBACK = "Come back fer a dig at ol'Woodlegs again have ye?",
    ANNOUNCE_EYEOFTERROR_FLYAWAY = "Ye ain't seen th'last o'me blades!",

    -- PIRATES
    ANNOUNCE_CANT_ESCAPE_CURSE = "Blast! It be cursed!",
    ANNOUNCE_MONKEY_CURSE_1 = "Arr! Me pirate curse senses be tinglin'!",
    ANNOUNCE_MONKEY_CURSE_CHANGE = "ARRR! It be th'witchin' hour!",
    ANNOUNCE_MONKEY_CURSE_CHANGEBACK = "Ol'Woodlegs ain't feel a lick o'difference! Yarr-har!",

    ANNOUNCE_PIRATES_ARRIVE = "Avast ye! Enemy vessel dead ahead! Run a shot across th'bow!",

    ANNOUNCE_OFF_SCRIPT = "Arrr! Quit yer blabberin'!",

	ANNOUNCE_COZY_SLEEP = "Yarr! Show a leg, mateys! Yer cap'n be a new man!",

	--
	ANNOUNCE_TOOL_TOOWEAK = "Arrgh! Scupper thet!",

    ANNOUNCE_LUNAR_RIFT_MAX = "Bewitched glow! Stay 'way, 'lest ye wish ta crack up on th'rocks!",
    ANNOUNCE_SHADOW_RIFT_MAX = "Thar blows a mermaid's call, washin' o'er th'land!",

    ANNOUNCE_SCRAPBOOK_FULL = "Me tally book be full t'th'brim!",

    ANNOUNCE_CHAIR_ON_FIRE = "'Tis fine.",

	--IA Quotes
	ANNOUNCE_MAGIC_FAIL = "Thet's not fer this land!",

	ANNOUNCE_SHARX = "Aye, me ol'frenemies back ta take a bite outta me hide!",

	-- TODO: duplicate, which ANNOUNCE_TREASURE to be used ingame? -crimeraaa
	ANNOUNCE_TREASURE = "Me pirate sense be tinglin'.", --Now used for Woodlegs' hat
	ANNOUNCE_TREASURE = "Th'map marks th'spot o'treasure!",
	ANNOUNCE_TREASURE_DISCOVER = "Me pirate sense be tinglin'.",
	ANNOUNCE_MORETREASURE = "Treasure! More'n more treasure!!!",
    ANNOUNCE_OTHER_WORLD_TREASURE = "Th'map don't be matchin' th'land.",
	ANNOUNCE_OTHER_WORLD_PLANT = "I don't be thinkin' this plant belongs here.",

	ANNOUNCE_IA_MESSAGEBOTTLE =
	{
		"Th' message be faded...",
	},
	ANNOUNCE_VOLCANO_ERUPT = "Th'fire devils be belchin'!",
	ANNOUNCE_MAPWRAP_WARN = "A veil o'mist approaches...",
	ANNOUNCE_MAPWRAP_LOSECONTROL = "Into th'murk and mist wit' me...",
	ANNOUNCE_MAPWRAP_RETURN = "Ta th'edge o'th'world and back!",
	ANNOUNCE_CRAB_ESCAPE = "Where'd th'lil devil go?",
	ANNOUNCE_TRAWL_FULL = "Me net be full!",
	ANNOUNCE_BOAT_DAMAGED = "She's takin' on water!",
	ANNOUNCE_BOAT_SINKING = "She be goin' doooon!", -- I'm assuming this is SW boats -crimeraaa
	ANNOUNCE_BOAT_SINKING_IMMINENT = "Be this th'day th'sea claims me?",
	ANNOUNCE_WAVE_BOOST = "We be a silver streak on th'blue now!",

	ANNOUNCE_WHALE_HUNT_BEAST_NEARBY = "A big devil be near'abouts.",
	ANNOUNCE_WHALE_HUNT_LOST_TRAIL = "Me lost th'devils trail...",
	ANNOUNCE_WHALE_HUNT_LOST_TRAIL_SPRING = "Like findin' a wet needle inna wet haystack.",

	BATTLECRY =
	{
		GENERIC = "Ye messed wit' th'wrong pirate!",
		PIG = "Thar be ham on th'menu tonight!",
		PREY = "Meet yer death!",
		SPIDER = "I'm gunna peg leg ye ta death!",
		SPIDER_WARRIOR = "I'm gunna peg leg ye ta death!",
		DEER = "Meet yer death!",
	},
	COMBAT_QUIT =
	{
		GENERIC = "How'd ye like yer taste o'pirate?",
		PIG = "Ye live ta ham it up 'nother day.",
		PREY = "It be too fast fer me peg leg.",
		SPIDER = "Try me again ye leggy devil.",
		SPIDER_WARRIOR = "Try me again ye leggy devil.",
	},
	DESCRIBE =
	{
	    MULTIPLAYER_PORTAL = "Yarr! I heard tales o' pirates returnin' from th'ferry o'th'damned such like it!",
		MULTIPLAYER_PORTAL_MOONROCK = "Be wary o' those whut be sparklin' wit' bewitched moon magic!",
        CONSTRUCTION_PLANS = "It ain't fer buildin' a proper ship, be it?",
        MOONROCKIDOL = "Arr, me hates givin' me treasures away!",
        MOONROCKSEED = "Yer gem be bewitched, but I be wantin' it!",

		ANTLION = 
		{
			GENERIC = "Don't be eyein' me treasures!",
			VERYHAPPY = "Treasure soothes me soul too, lad.",
			UNHAPPY = "Th'big devil ain't be havin' our treasures!",
		},
		ANTLIONTRINKET = "Build me a great sand castle wit' this!",
		SANDSPIKE = "Woodlegs be wary. Lands be treacherous.",
        SANDBLOCK = "It be a sandcastle master builder!",
        GLASSSPIKE = "Better on land than 'neath me boat.",
        GLASSBLOCK = "Why can't ye use all thet glass fer a bottle 'er two?",

		ABIGAIL_FLOWER = 
		{ 
			GENERIC = "A bewitched flower a soul whut been lost!",
			LEVEL1 = "It ain't wantin' no scallywags about!",
			LEVEL2 = "I's sees some devil magics comin' about.",
			LEVEL3 = "Thet flower be givin' Woodlegs a spook!",
			-- deprecated
			LONG = "Somethin' be dyin' ta get out.",
			MEDIUM = "I's sees some devil magics comin' about.",
			SOON = "Thet flower be givin' Woodlegs a spook!",
			HAUNTED_POCKET = "I don't think it be likin' me pockets.",
			HAUNTED_GROUND = "Thet flower be a spectre!",
		},

		BALLOONS_EMPTY = "Wee balloons.",
		BALLOON = "'Tis a mighty floatin' ball.",
		BALLOONPARTY = "Thet balloon be pregnant!",
		BALLOONSPEED =
        {
            DEFLATED = "'Tis a ballon fer sure.",
            GENERIC = "Puts tha sails into me peg-legs!",
        },
		BALLOONVEST = "No cap'n worth 'is salt would be caught dead wearin' it!",
		BALLOONHAT = "This 'as gots ta be th'worst pirate's hat me ever seent.",

		BERNIE_INACTIVE =
		{
			BROKEN = "Thet's th'end o'thet.",
			GENERIC = "Thet be a cursed lil' teddy bear.",
		},
		BERNIE_ACTIVE = "Thet teddy bear be cursed wit' devil magic o'sorts!",
		BERNIE_BIG = "Arrr! Ye be kraken sized now!",

		BOOKSTATION =
		{
			GENERIC = "Whut kinda swab brings a library on deck!?",
			BURNT = "Nay time fer readin' when thar be cannon fire about!",
		},
		BOOK_BIRDS = "Summons a bunch 'a cacklers from th'sky!",
		BOOK_TENTACLES = "Devil magic thet summons demons from th'ground!",
		BOOK_GARDENING = "Bah! Lubber talk!",
		BOOK_SILVICULTURE = "Sticks an' bones.",
		BOOK_HORTICULTURE = "Bah! Lubber talk!",
		BOOK_SLEEP = "Ol'Woodlegs's eyes are gettin' 'eavy.",
		BOOK_BRIMSTONE = "It be thet cursed devil magic!",
        BOOK_FISH = "Me tastiest grub, immortalized!",
        BOOK_FIRE = "Arr! I can show ye a bit o'firepower!",
        BOOK_WEB = "Thar be webbin' in them words!",
        BOOK_TEMPERATURE = "Bewitched, cursed pages!",
        BOOK_LIGHT = "Yer meant ta avoid lighthouses, so ye don't crack up on th'rocks!",
        BOOK_RAIN = "Yer makin' th'sky weep! Thet's mermaid magic!",
        BOOK_MOON = "Follow th'moon!",
        BOOK_BEES = "Me crew inna book!",

        BOOK_HORTICULTURE_UPGRADED = "No time fer lubber blubber! Yarr-harr!",
        BOOK_RESEARCH_STATION = "Bring a scribe on yer crew, let 'em ne'er forget yer plunders!",
        BOOK_LIGHT_UPGRADED = "Thet lighthouse'd put me ship ta sleep!",

        FIREPEN = "Be it a flaming cutlass?",
		WAXWELLJOURNAL =
		{
			GENERIC = "Only swabs would trust thet devil's magic!",
--fallback to speech_wilson.lua			NEEDSFUEL = "only_used_by_waxwell",
		},
		SPICEPACK = "This ain't fit fer no pirate's back!",
		LIGHTER = "Tis' be a pretty wee lighter.",
		LUCY = "Be th'axe talkin' or be Woodlegs gettin' crazier?",
		SPEAR_WATHGRITHR = "A dagger not o'mine or th'sea's.",
	    WATHGRITHRHAT = "Keeps yer melon safe wit' its winged sails.",
		
        PLAYER =
        {
            GENERIC = "Ahoy, %s!",
            ATTACKER = "Yer messin' wit' th'wrong pirate!",
            MURDERER = "Meet yer death!",
            REVIVER = "%s be a fine matey.",
            GHOST = "Ye put up a good fight, %s.",
			FIRESTARTER = "Keep yer distance, lad. I ain't a fire lubber.",
        },
		WILSON = 
		{
			GENERIC = "Ahoy, %s ye cabin's boy!",
			ATTACKER = "Don't be temptin' fate, %s. I'll keep me eye on ye.",
			MURDERER = "Meet yer death, %s! Where's yer science ta save ye now!",
			REVIVER = "%s helps me mateys. Doin' 'is up'n'comin' science doohickies.",
			GHOST = "%s be restless. Yer head's gettin' too big fer yer britches!",
			FIRESTARTER = "Keep yer wordy fires away from me woody legs!",
		},
		WOLFGANG = 
		{
			GENERIC = "Ahoy, %s ye brutish buccaneer",
			ATTACKER = "Ol'Woodlegs can take ye! Have at ye!",
			MURDERER = "Time ta walk th'plank! Blow th'man down!",
			REVIVER = "%s's muscles be keeping me mateys in check. ",
			GHOST = "Ye put up a mighty good fight, %s. Ye ought ta try th'sea.",
			FIRESTARTER = "%s, stay back ye scallywag! Fire elsewheres!",
		},
		WAXWELL = 
		{
			GENERIC = "Ahoy %s, ye hornswogglin' salty dog!",
			ATTACKER = "Arr, I'll 'ave ye black spotted, ye sour devil!",
			MURDERER = "Ye ain't hornswagglin' me this day! Meet me pirate's blade, %s!",
			REVIVER = "%s be changin' fer good. Ye ain't off th'hook yet!",
			GHOST = "%s be needin' a heart o'sorts. Ye sure ye had one?",
			FIRESTARTER = "Now I'd really like ta throw ye 'n yer flames overboard!",
		},
		WX78 = 
		{
			GENERIC = "Ahoy, %s ye bucket o'bolts!",
			ATTACKER = "Ye need a squirt o'oil %s, yer joints arrre squeakin'.",
			MURDERER = "Back ye tin devil! I'll 'ave ye marooned!",
			REVIVER = "%s be a good tin can o'sorts.",
			GHOST = "%s be a sore loser needin' a heart. If ever there were one.",
			FIRESTARTER = "Those flames ain't gonna be protectin' yer joints forever!",
		},
		WILLOW = 
		{
			GENERIC = "Ahoy, %s ye flame lovin' picaroon!",
			ATTACKER = "%s got them villainous eyes. Me cutlass'll keep eyes on those eyes.",
			MURDERER = "Ta th'locker wit'ya! An'thar's nary a flame!",
			REVIVER = "%s be helpin' Woodlegs well. Nay th'fire lightin' part.",
			GHOST = "Ye but up a fiery fight o'sorts, %s.",
			FIRESTARTER = "Keep off me legs, lass! I ain't one fer fire lubbers!",
		},
		WENDY = 
		{
			GENERIC = "Ahoy, %s ye wee lass!",
			ATTACKER = "%s is growin' up ta be a real pirate fer sure.",
			MURDERER = "Arrrg! She's all vinegar!",
			REVIVER = "%s will be a fine lil' pirate some day.",
			GHOST = "Ye did fine, %s. I'll teach ye th'ship ropes!",
			FIRESTARTER = "Keep off me legs ye wee lass!",
		},
		WOODIE = 
		{
			GENERIC = "Ahoy, %s ye hairy freebooter!",
			ATTACKER = "%s got them villainous eyes. Me cutlass'll keep ye in check.",
			MURDERER = "It be th'villainous beard ye got! Walk th'plank!",
			REVIVER = "I'd take ol' %s here in as one of me mateys. If ye stop starin' at me pegs!",
			GHOST = "%s be needin' a heart outta wood.",
			BEAVER = "Why arrre ye starin' at me pegs like thet?",
			BEAVERGHOST = "Best this way than eatin' me pegs, %s.",
			MOOSE = "Arr! Ye be a cursed lubber! What treasures are ye guardin'!",
            MOOSEGHOST = "Yer time's ran out, matey.",
            GOOSE = "Be wary, I ain't lookin' ta catch no pirate's curse.",
            GOOSEGHOST = "Did ye catch a spot o'sunlight?",
			FIRESTARTER = "Ye 'ave betrayed yer wooden brothers!",
		},
		WICKERBOTTOM = 
		{
			GENERIC = "Ahoy, %s ye provost!",
			ATTACKER = "Yer luck be runnin' low, %s! Get th'witch!",
			MURDERER = "Ye'll be in th'readin' fer a bewitched death, %s!",
			REVIVER = "%s be a good ol'mate. Me ain't had enuff learnin' to say words like ye.",
			GHOST = "%s be eternally restless. A spot o'rum outta solve ye.",
			FIRESTARTER = "%s be writin' devil's magic 'bout fires!",
		},
		WES = 
		{
			GENERIC = "Ahoy, %s ye scallywag!",
			ATTACKER = "%s's knave silence be pirate talk fer death!",
			MURDERER = "Ta th'locker wit'ya, jester!",
			REVIVER = "%s has proven themselves a fair matey, ta swab th'decks!",
			GHOST = "At least ye tried, %s. Thet'll be 'nuff. Back ta th'cabins wit'ye.",
			FIRESTARTER = "I knew ye weren't up ta any good, ye jestin' scallywag.",
		},
		WEBBER = 
		{
			GENERIC = "Ahoy, %s, ye wee spider!",
			ATTACKER = "Don't go tryin' me, ya leggy devil.",
			MURDERER = "I'm gunna peg leg ye ta death, %s!",
			REVIVER = "%s be a nice lil' bugger.",
			GHOST = "%s be needin' a heart. Do ye take spiders hearrrts?",
			FIRESTARTER = "Stay back from me legs! Ye already 'ave too many!",
		},
		WATHGRITHR = 
		{
			GENERIC = "Ahoy, %s ye feisty marauder!",
			ATTACKER = "%s be swingin' 'er dagger around at me mates! Swing no more!",
			MURDERER = "%s be scourgin' th'seven seas! Yer pure vinegar!",
			REVIVER = "%s be a true marauder. Have ye ever sailed a ship?",
			GHOST = "Ye put up a good fight, %s.",
			FIRESTARTER = "%s be roarin' up fires! Enuff wit'it!",
		},
		WINONA =
        {
            GENERIC = "Ahoy, %s ye boatswain!",
            ATTACKER = "Yer messin' wit' th'wrong pirate, lass!",
            MURDERER = "%s has gone mad wit'power! Get'er!",
            REVIVER = "%s be a fine crafty mate.",
            GHOST = "Ye can put up a good fight, %s.",
			FIRESTARTER = "Don't be lightin' up me ol'pegs, lass!",
        },
		WORTOX =
        {
            GENERIC = "Arr! I've heard tales o'this devil!",
            ATTACKER = "Stay wary o'this tricky devil!",
            MURDERER = "Ye ain't runnin' a rig on ol'Woodlegs! I'll throw ye overboard, %s!",
            REVIVER = "I ain't trustin' no devils, but I'll let ye keep yer life fer now.",
            GHOST = "Ack! You ney 'ave business wit' me, %s! Ta th'locker ye go!",
            FIRESTARTER = "Back ye horned devil! Back from me pegs!",
        },
		WORMWOOD =
        {
            GENERIC = "Ahoy, %s ye walkin' bit o'veg!",
            ATTACKER = "Don't be testin' yer fate, lad. Scrubbed off plenty o'sea veg in me day.",
            MURDERER = "I'll 'ave ta scrub ye off th'side o'me boat! Wit'th'rest o'th'weeds!",
            REVIVER = "Th'real treasure be in your chest! And I be wantin' it!",
            GHOST = "I ain't givin' ye any o'me gems fer a new hearrt, lad.",
            FIRESTARTER = "Away from me pegs! Ye be messin' wit'fires ye dun understand!",
        },
		WURT =
        {
            GENERIC = "Ahoy, %s ye wee walkin' fish!",
            ATTACKER = "Yarr! A fighter! Ye'd grow up ta be a good b'witched cap'in someday!",
            MURDERER = "Arr! Ye can't trust a walkin' fish! Keep 'er far from me boat!",
            REVIVER = "Yer a good'un, but fish's still me favorite treat!",
            GHOST = "Ye be feedin' th'fishes now!",
            FIRESTARTER = "Blast! Away from me peg ye lil' sea-devil!",
        },
		WALTER =
        {
            GENERIC = "Ahoy, %s, ye wee scout!",
            ATTACKER = "Yer pickin' a losin' battle, laddie!",
            MURDERER = "Arr! Ne'er told ye 'bout pirates did they?",
            REVIVER = "Yarr! Ye'd make a great cabin's boy!",
            GHOST = "Ye tried, lad. Let ol'Woodlegs teach ya th'ways o'th'cutlass!",
            FIRESTARTER = "Th'fires stay off me legs, boy!",
        },
		WANDA =
        {
            GENERIC = "Ahoy, %s, ye tinkerer!",
            ATTACKER = "Ye ain't gonna sail outta dodge fer this'un!",
            MURDERER = "Ye won't be gettin' away wit' fightin' me crew!",
            REVIVER = "Yarr! Yer skills would make fer a fine crew mate!",
            GHOST = "Yer past deeds have come ta haunt ye like an ol'sea dog, aye?",
            FIRESTARTER = "Keep yer timey-whutsit to yerself!",
        },
        WONKEY =
        {
            GENERIC = "Ahoy, ye scurvy monkey!",
            ATTACKER = "Ya lily-livered scoundrel be stinkin' up me crew!",
            MURDERER = "Be it mutiny, monkey?",
            REVIVER = "Ye a heart o'gold under all thet fur!",
            GHOST = "Ye make good feed fer th'fish!",
            FIRESTARTER = "Yer firin' up a storm, an' Woodlegs ain't intendin' ta sail in it!",
        },
		WALANI = 
		{
	        GENERIC = "Ahoy, %s ye  swashbucklin' seafarer!",
	        ATTACKER = "Don't go temptin' fate, %s. Yer raft o'wood ain't match fer a mighty sea vessel!",
	        MURDERER = "Time to walk th'plank o'yours!",
	        REVIVER = "%s helps me mateys. Ye ever been on a real crew, lass?",
	        GHOST = "%s be needin' a heart. Trawl 'er up somethin' fine!",
	        FIRESTARTER = "Take yer own advice, %s. Back way from th'fire!",
		},
	    WARLY = 
		{
	        GENERIC = "Ahoy, %s ye cook!",
	        ATTACKER = "Don't be temptin' fate, %s. Me cutlass'll be watchin' ya.",
	        MURDERER = "Thar be %s on th'menu tonight!",
	        REVIVER = "%s be helpin' Woodlegs eat well.",
	        GHOST = "%s be needin' a heart, we ain't wanna be late fer grub!",
	        FIRESTARTER = "%s be smokin' up a storm. Arr, bit more than smokes.",
		},
	    WILBUR = 
		{
	    	GENERIC = "Ahoy, %s, ye fancy monkey!",
	        ATTACKER = "%s be an unsettlin' one ta keep on board.",
	        MURDERER = "Ta th'locker wit'ya, monkey!",
	        REVIVER = "%s be a delight ta 'ave runnin' 'bout a ship!",
	        GHOST = "Give me yer gold 'n I'll fetch ye a heart.",
	        FIRESTARTER = "Ye be a monkey o'riches, not o'fire. Get yerself t'gether!",
		},
	    WOODLEGS = 
		{
			GENERIC = "Ye be quite familiar, %s.",
	        ATTACKER = "Arr! Show me what ye got, ye ol'carouser!",
	        MURDERER = "Ye messed wit'th'wrong pirate! Arr... Th'same pirate!",
	        REVIVER = "Always knew ol'Woodlegs was th'captain fer a reason!",
	        GHOST = "'Tis a pirate's life, even in death.",
	        FIRESTARTER = "Be wary of ye ol'legs, ye fiendish seadog.",
		},
        MIGRATION_PORTAL = 
        {
            GENERIC = "What new trap be this?",
            OPEN = "I don't be knowin' where it leads, but it be ready ta take me.",
            FULL = "It ain't havin' ol'Woodlegs.",
        },

		ABIGAIL =
		{
            LEVEL1 =
            {
                "A lady specter!",
                "A lady specter!",
            },
            LEVEL2 = 
            {
                "A lady specter!",
                "A lady specter!",
            },
            LEVEL3 = 
            {
                "A lady specter!",
                "A lady specter!",
            },
		},
		ACCOMPLISHMENT_SHRINE = "Ye be knowin'th'glory of pirate Woodlegs, world!",
		ACORN = "It be rattlin' when ye shake it.",
        ACORN_SAPLING = "Thar be a tree in its future.",
		ACORN_COOKED = "Not bad fer a landlubbin' nut.",
		ADVENTURE_PORTAL = "What new trap be this?",
		AMULET = "A wee bit o'insurance round me neck.",
		ANCIENT_ALTAR = "A statue from ye old times.",
		ANCIENT_ALTAR_BROKEN = "It be broke.",
		ANCIENT_STATUE = "Me ain't be likin'th'vibes it be givin'.",
		ANIMAL_TRACK = "Th'beast left a trail leadin'ta its hide.",
		ARMORDRAGONFLY = "She be hot ta th'touch o'me enemies!",
		ARMORGRASS = "Only a landlubber would think o'makin' armor from grass.",
		ARMORMARBLE = "This'd sink like a stone in th'sea.",
		ARMORRUINS = "Feels like I be wearin' treasure.",
		ARMORSKELETON = "Me'd like ta see them try ta hit Woodlegs now!",
		SKELETONHAT = "Th'feistiest o'pirates don their enemies' bones.",
		ARMORSLURPER = "How bad do I wanna protect myself?",
		ARMORSNURTLESHELL = "Don't seem right ta turtle in a fight.",
		ARMORWOOD = "At least wood floats.",
		ARMOR_SANITY = "Feels good ta put it on.",
		ASH =
		{
			GENERIC = "Th'fire claimed its price.",
			REMAINS_EYE_BONE = "Luckily I kept me own eye in me travels.",
			REMAINS_GLOMMERFLOWER = "Th'flower weren't made fer travellin'.",
			REMAINS_THINGIE = "I s'pose it weren't ash ta begin wit'.",
		},
		AXE = "I be axe'n th'questions 'round here!",
		BABYBEEFALO = 
		{
			GENERIC = "Yer cute fer dumb, hairy meat.",
		    SLEEPING = "Sleep while ya can, scallywag.",
        },
        BUNDLE = "Treasure wrapped up n'safe.",
        BUNDLEWRAP = "Ta wrap me treasure up an' keep 'em safe from peepin' eyes.",
		BACKPACK = "I be packin' now.",
		BACONEGGS = "Th'most important meal o'th'day!",
		BANDAGE = "Fer layabout swabs.",
		BASALT = "Strong it be.",
		BAT = "Back ye rat-faced, flyin' devil!",
		BATBAT = "I'd let this fly at me enemies.",
		BATWING = "Wit' a wee bit o'ranch dressin'...",
		BATWING_COOKED = "Tastes like cackler.",
		BATCAVE = "Ain't no place I'd call 'ome.",
		BEARDHAIR = "Chinny whiskers.",
		BEARGER = "Back ye badgerin' devil!",
		BEARGERVEST = "Toasty warm.",
		BEARGER_FUR = "Smells badgerin' me nose.",
		BEDROLL_FURRY = "Sweet, hairy dreams.",
		BEDROLL_STRAW = "Don't look fit fer a dry nights sleep.",
		BEEQUEEN = "Take a gander at th'stinger on thet beast!",
		BEEQUEENHIVE = 
		{
			GENERIC = "Would be needin' new pegs if I were ta walk o'er thar!",
			GROWING = "Make th'yellow devils walk th'plank before they finish!",
		},
        BEEQUEENHIVEGROWN = "Arr! I know ye be guardin' gooey treasures!",
        BEEGUARD = "Get out me face, ye swab!",
		MINISIGN =
        {
            GENERIC = "A wee landmarker.",
            UNDRAWN = "It be missin' an 'X'!",
        },
        MINISIGN_ITEM = "Put it in th'ground!",
		HIVEHAT = "This ain't be th'kinda crown Woodlegs be lookin' fer! Whar's th'jewels!?",
		BEE =
		{
			GENERIC = "A honey-makin' devil.",
			HELD = "Yield yer sweet treasure!",
		},
		BEEBOX =
		{
			BURNT = "I smell burnt honey...",
			FULLHONEY = "Thar be gold in this box!",
			GENERIC = "Thar be bees.",
			NOHONEY = "'Tis empty o'th'golden treasure.",
			SOMEHONEY = "'Tis a wee bit o'gold left.",
			READY = "Thar be gold in this box!",
		},
		MUSHROOM_FARM =
		{
			STUFFED = "A tad too much thar.",
			LOTS = "Make way, th'shrooms are comin' through!",
			SOME = "It's just gettin' started.",
			EMPTY = "It be empty.",
			ROTTEN = "All rotten, like me teeth.",
			BURNT = "Burnt 'shrooms.",
			SNOWCOVERED = "It be plugged wit' snow.",
		},
		BEEFALO =
		{
			FOLLOWER = "Ye be knowin' who yer master be.",
			GENERIC = "'E's got them bewitched eyes on 'im!",--Here's th'beef!
			NAKED = "Ye should be ashamed o'yerself.",
			SLEEPING = "Its snorin' would wake th'dead.",
			--Domesticated states:
            DOMESTICATED = "This one knows how ta be a good beast.",
            ORNERY = "'Tis an ornery beast.",
            RIDER = "Fit fer a sail.",
            PUDGY = "Th'spoils o'war.",
			MYPARTNER = "Me first mate.",
		},
		BEEFALOHAT = "Keeps me ears warm.",
		BEEFALOWOOL = "Carries a musk wit' it.",
		BEEHAT = "Woodlegs ain't no honey farmer!",
		BEESWAX = "Nay good as me own earwax!",
		BEEHIVE = "A den o'honey makers.",
		BEEMINE = "Step 'n get stung.",
		BEEMINE_MAXWELL = "Step 'n get stung.",
		BELL = "Thar be dingin' 'n 'lingin'.",
		BERRIES = "Stains me blouse.",
		BERRIES_COOKED = "Could use a crust o'bread ta spread o'er.",
		BERRIES_JUICY = "Could be juiced fer a nice bottle o'wine.",
        BERRIES_JUICY_COOKED = "They be rottin' quick.",
		BERRYBUSH =
		{
			BARREN = "It needs a spot o'poop.",
			GENERIC = "Ripe fer th'pickin'.",
			PICKED = "Will thar be berries again?",
			WITHERED = "Th'bush can't beat th'heat.",
			DISEASED = "Thet bush be havin' scurvy.",
			DISEASING = "Thet bush be gettin' scurvy",
			BURNING = "'Tis a blaze!",
		},
		BERRYBUSH_JUICY =
		{
			BARREN = "It needs a spot o'poop.",
			WITHERED = "Th'heat takes anotha'bush.",
			GENERIC = "Them berries be lookin' mighty fragile.",
			PICKED = "Give me yer juicy treasures!",
			DISEASED = "Thet bush be havin' scurvy.",
			DISEASING = "Thet bush be gettin' scurvy",
			BURNING = "She's burnin' bright!",
		},
		BIGFOOT = "Watch oot ye big trottin' devil ye!",

		BIRCHNUTDRAKE = "Yer a mad nutter ain't ye?",
		BIRDCAGE =
		{
			GENERIC = "No parrot o'mine could bear a cagin'.",
			OCCUPIED = "How ye like yer cell, matey?",
			SLEEPING = "Awww, look at thet. Th'peeper's sleepin'.",
			HUNGRY = "Y'feelin' a wee bit peckish?",
			STARVING = "Woodlegs b'hungry too, y'know!",
			DEAD = "Yer soul's gon ta th'depths.",
			SKELETON = "Davey Bones.",
		},
		BIRDTRAP = "Once ye fly in, ye don't fly oot.",
		BIRD_EGG = "A cackler's egg.",
		BIRD_EGG_COOKED = "If only I had me a crust o'bread.",
		BISHOP = "Ye need a squirt o'oil mate, yer joints arrre squeakin'.",
		BISHOP_NIGHTMARE = "Back ye tin devil!",
		BLOWDART_FIRE = "I be breathin' fire.",
		BLOWDART_PIPE = "A swab's weapon.",
		BLOWDART_SLEEP = "Whisper 'em ta sleep.",
		BLOWDART_YELLOW = "It be tinglin'",
		BLUEAMULET = "Chilly.",
		BLUEGEM = "It gleams true blue!",
		BLUEPRINT = 
		{ 
            COMMON = "I ain't a learned man.",
            RARE = "Me nose be sniffin' somethin' valuable!",
        },
	    SKETCH = "Sculpt me a figurehead worthy o'me 'Sea Legs', tried an' true!",
		COOKINGRECIPECARD = 
		{
			GENERIC = "It be a scallywag's gibberish!",
		},
		BLUE_CAP = "Me know enough not ta pop this'n me mouth wit'no questions asked.",
		BLUE_CAP_COOKED = "Smells eat-able...",
		BLUE_MUSHROOM =
		{
			GENERIC = "I got room fer mush in me belly.",
			INGROUND = "Ye only come oot at night?",
			PICKED = "Will th'shroom bloom again?",
		},
		BOARDS = "Good n' straight.",
		BONESHARD = "Bits o'bone.",
		BONESTEW = "Good fer th'scurvy.",
		BOOMERANG = "I prefer somethin' thet don't come back at me head.",
		BUGNET = "Keeps th'buggies outta me no teeth.",
		BUNNYMAN = "Hop along ya floppy eared scallywag.",
		BUSHHAT = "Fer blendin' in wit' th'landlubbers.",
		BUTTER = "Look whut churned up!",
		BUTTERFLY =
		{
			GENERIC = "Pretty as a sunset o'er th'water.",
			HELD = "All that pretty in me pocket.",
		},
		BUTTERFLYMUFFIN = "Fibre gives ye wings.",
		BUTTERFLYWINGS = "I got me th'prettiest part.",
		BUZZARD = "Ye carrion-eatin' devil!",

	    SHADOWDIGGER = "Ye 'ave a cursed crew wit'ya too?",
        SHADOWDANCER = "ARR! A bewitched crew performin' a jig most foul!",
		
		CACTUS =
		{
			GENERIC = "Ye have a sharp taste ye do.",
			PICKED = "Ye been freshly pricked. I mean picked.",
		},
		CACTUS_FLOWER = "Might put me eye out sniffin' ye.",
		CACTUS_MEAT = "Dry and prickly, but 'tis still meat.",
		CACTUS_MEAT_COOKED = "Not bad when it ain't stickin' ye.",
		CAMPFIRE =
		{
			EMBERS = "Th'fire needs ta be fed.",
			GENERIC = "Anyone know a good sea shanty 'side me?",
			HIGH = "She be a roarin'.",
			LOW = "Fire's goin'oot.",
			NORMAL = "Warm n' cozy.",
			OUT = "She be done.",
		},
		CANE = "I got me a built-in cane.",
		CARROT = "Fer me night vision in me eye.",
		CARROT_COOKED = "Nice 'n soft fer me no teeth.",
		CARROT_PLANTED = "Buried treasure of a kind.",
		CARROT_SEEDS = "Landlubbers.",
		CARTOGRAPHYDESK = 
		{	
			GENERIC = "It be a perfect fit fer me! Like good ol'times on me ship!",
			BURNING = "Whut filthy landlubber be burnin' me maps?",
			BURNT = "This be personal. Some scallywag will be walkin' th'plank t'night!",
		},
		CATCOON = "Ye look like ye got a taste fer garbage.",
		CATCOONDEN =
		{
			EMPTY = "Yer home be already looted!",
			GENERIC = "Whut lives in here. I'm stumped.",
		},
		CATCOONHAT = "Hope none o'me mates catch me in this gettup.",
		CAVE_BANANA = "I must be goin' banana's down here.",
		CAVE_BANANA_COOKED = "Not bad.",
		CAVE_BANANA_TREE = "Now ol'Woodlegs has seen it all.",
		CAVE_ENTRANCE = "Thet be coverin' somethin' suspicious.",
        CAVE_ENTRANCE_RUINS = "'Tis be a devilish hole.",
       
       	CAVE_ENTRANCE_OPEN = 
        {
            GENERIC = "I ain't one fer climbin' in holes like a swab.",
            OPEN = "I bet thar ain't no sea down there.",
            FULL = "Thar be too many explorin'.",
        },
        CAVE_EXIT = 
        {
            GENERIC = "Guess Woodlegs be stayin' doon here.",
            OPEN = "Back ta open air.",
            FULL = "They be exiling ol'Woodlegs!",
        },

		CAVE_FERN = "How did ye grow doon here wit' no light?",
		CHARCOAL = "Burnt ta a brickette.",
		CHESSPIECE_PAWN = 
        {
		GENERIC = "Whut? Whar be th'pirate piece?",
		},
        CHESSPIECE_ROOK = 
        {
			GENERIC = "Ye think yer so tough, aye?",
			STRUGGLE = "It's movin'!",
		},
        CHESSPIECE_KNIGHT = 
        {
			GENERIC = "Whut?",
			STRUGGLE = "It's movin'!",
		},
        CHESSPIECE_BISHOP = 
        {
			GENERIC = "Get out me face, ya swab.",
			STRUGGLE = "It's movin'!",
		},
		CHESSPIECE_MUSE = "Me ships sails fer th'sea, nay queen!",
        CHESSPIECE_FORMAL = "Woodlegs ain't no privateer fer no \"king\"!",
        CHESSPIECE_HORNUCOPIA = "Nay th'edible kind.",
        CHESSPIECE_PIPE = "Me lungs may be made o'stone already!",
        CHESSPIECE_DEERCLOPS = "Me victories encased in stone!",
        CHESSPIECE_BEARGER = "Ye'd do well wardin' off vengeful spirits in me quarters!",
        CHESSPIECE_MOOSEGOOSE = "How'd ye like t'be mounted on me ship?",
        CHESSPIECE_DRAGONFLY = "Th'treasure keeper be th'real treasure now!",
		CHESSPIECE_MINOTAUR = "Gives ye a bit o'stink eye!",
		CHESSPIECE_BUTTERFLY = "Bah! Lubber lovin' bit o'beauty.",
        CHESSPIECE_ANCHOR = "Ye don't wish ta be bilged on 'er, do ye?",
        CHESSPIECE_MOON = "Yer tide turnin' days be stoned!",
		CHESSPIECE_CARRAT = "Gives yer teeth a chip 'er two.",
		CHESSPIECE_MALBATROSS = "No more breakin' 'bout me boat!",
        CHESSPIECE_CRABKING = "Woodlegs be th'king o'crabs 'round 'ere!",
        CHESSPIECE_TOADSTOOL = "Nary a taste o'shroom.",
        CHESSPIECE_STALKER = "Pay yer respects ta ward off any lingerin' devil curses!",
        CHESSPIECE_KLAUS = "A figurehead worthy o'mine!",
        CHESSPIECE_BEEQUEEN = "Get 'er mounted up on me port side!",
        CHESSPIECE_ANTLION = "Dirty boat breaker.",
		CHESSPIECE_BEEFALO = "'Tis a beefy statue wit' no beef.",
		CHESSPIECE_KITCOON = "Thet ol'treasure seeker!",
		CHESSPIECE_CATCOON = "Would sink a hull.",
        CHESSPIECE_MANRABBIT = "Yer wavin' a white flag wit' a figurehead such as thet!",
        CHESSPIECE_GUARDIANPHASE3 = "Ye best be laid to rest, dredged up ol' wounds!",
		CHESSPIECE_EYEOFTERROR = "'Aye. Don't let 'er meet yer gaze.",
        CHESSPIECE_TWINSOFTERROR = "Two fer th' price o'free!",
        CHESSPIECE_DAYWALKER = "Keep yer peepers off its gaze, 'lest ye end up like it.",
        CHESSPIECE_DEERCLOPS_MUTATED = "Pay th'devil no heed!",
        CHESSPIECE_WARG_MUTATED = "Let them eyes roam th'sea fore'er more.",
        CHESSPIECE_BEARGER_MUTATED = "Scares them freebooters britches off!",

		CHESSPIECE_KRAKEN = "We bested th'mightiest beast!",
		CHESSPIECE_TIGERSHARK = "Ward off all devils whut think o'messin' wit' me 'Sea Legs'!",
		CHESSPIECE_TWISTER = "Yer boat whippin' days be set in stone.",
		CHESSPIECE_SEAL = "Thar be nuthin' but stone left behind them sad ol'eyes.",

		CHESSJUNK1 = "Somebody's a sore loser.",
		CHESSJUNK2 = "Somebody's a sore loser.",
		CHESSJUNK3 = "Somebody's a sore loser.",
		CHESTER = "Whut'n blazes arre ye?",
		CHESTER_EYEBONE =
		{
			GENERIC = "Ye got a nice eye.",
			WAITING = "'Tis gettin' some shut-eye.",
		},
		COLDFIRE =
		{
			EMBERS = "Th'fire be goin'oot.",
			GENERIC = "Th'flames be lickin' th'dark.",
			HIGH = "I be glad this fire is in a pit and not on me ship.",
			LOW = "Th'fire be gettin' low.",
			NORMAL = "Good ta warm me'self by.",
			OUT = "Th'fire be out.",
		},
		COLDFIREPIT =
		{
			EMBERS = "Th'fire be goin'oot.",
			GENERIC = "Th'flames be lickin' th'dark.",
			HIGH = "I be glad this fire is in a pit and not on me ship.",
			LOW = "Th'fire be gettin' low.",
			NORMAL = "Good ta warm me'self by.",
			OUT = "Th'fire be out.",
		},

		COMPASS =
		{
			E = "Ta th'East!",
			GENERIC = "'Tis a compass.",
			N = "Ta th'North!",
			NE = "Ta th'Nor' East!",
			NW = "Ta th'Nor' West!",
			S = "Ta th'South!",
			SE = "Ta th'Sou' East!",
			SW = "Ta th'Sou' West!",
			W = "Ta th'West!",
		},
		COOKEDMANDRAKE = "Woodlegs did cook'im good.",
		COOKEDMEAT = "'Tis a fine meal.",
		COOKEDMONSTERMEAT = "Thet be bad eatin'.",
		COOKEDSMALLMEAT = "'Tis but a morsel.",
		COOKPOT =
		{
			BURNT = "Th'pot be burnt up.",
			COOKING_LONG = "'Tis a slow recipe.",
			COOKING_SHORT = "Ye'll be done soon 'nuff.",
			DONE = "'Tis ready for eatin'.",
			EMPTY = "Th'pot be empty.",
		},
		COONTAIL = "Thet beast dropped 'is tail!",
		CORN = "Fresh corn! 'Tis me lucky day.",
		CORN_COOKED = "Roasted corn, me favorite.",
		CORN_SEEDS = "Ye'll be food soon 'nuff",
		CANARY =
        {
            GENERIC = "Ye keep watch of me treasure now, but don't be takin'.",
            HELD = "Yer still livin'?",
        },
		CANARY_POISONED = "Th'bird's got scurvy now too!",

		CRITTERLAB = "Keep yer peepers away from me treasures.",
        CRITTER_GLOMLING = "Cute lil' thing, ain't ye?",
        CRITTER_DRAGONLING = "A flyin' lil' devil.",
		CRITTER_LAMB = "If ye don't make a mess, ye can stay.",
        CRITTER_PUPPY = "I'll keep ye by me side, ye scurvy dog.",
        CRITTER_KITTEN = "I prefer me a parrot.",
		CRITTER_PERDLING = "Ye look like ye got 'n eye fer treasure!",
		CRITTER_LUNARMOTHLING = "Guide me boat ta th'moon's treasure, fae!",

		CROW =
		{
			GENERIC = "Thet is one oily parrot!",
			HELD = "Ye prove hard ta clean, parrot.",
		},
		CUTGRASS = "Ye'll be helpin' Woodlegs later.",
		CUTLICHEN = "Sea-grass!",
		CUTREEDS = "But Woodlegs can't reed.",
		CUTSTONE = "Thet be well tooled stone.",
		DEADLYFEAST = "'Tis a dreadful meal.",
		DECIDUOUSTREE =
		{
			BURNING = "Ye won't be standin' fer long.",
			BURNT = "Thet tree be worthless.",
			CHOPPED = "Ye'll make a fine vessel.",
			GENERIC = "Yer hair be pretty.",
			POISON = "Ye don't seem very nice.",
		},
		DEER = 
		{
			GENERIC = "Yer needin' an eye-patch 'er two!",
			ANTLER = "I'll wrestle th'antler off of ya!",
		},
		DEER_ANTLER = "Yer calcium be Woodlegs' now!",
        DEER_GEMMED = "Ye got somethin' in ye head Woodlegs be wantin'!",
		DEERCLOPS = "Arrrr ye serious?",
		DEERCLOPS_EYEBALL = "'Tis a giant eyeball.",
		DEPLETED_GRASS =
		{
			GENERIC = "Ye won't be much use ta Woodlegs.",
		},
		GOGGLESHAT = "I be a sea pirate, not a sky pirate.",
        DESERTHAT = "A real pirate can take th'sand in th'eyes!",
		ANTLIONHAT = "Bah! Must be oot yer mind if ye wanna cap'n th'sands, nay th'seas.",
		DEVTOOL = "Aye, 'tis a truly wondrous item.",
		DEVTOOL_NODEV = "Aye, 'tis a truly wondrous item.",
		DIRTPILE = "Thet be lookin like a pile o'treasures.",
		DIVININGROD =
		{
			COLD = "Thar is nary a whiff of treasure nearby.",
			GENERIC = "She'll help ol'Woodlegs sniff out treasures.",
			HOT = "Whar be th'treasures?!",
			WARM = "Take Woodlegs ta th'treasures!",
			WARMER = "Woodlegs be closing in!",
		},
		DIVININGRODBASE =
		{
			GENERIC = "Aye, 'tis a mystery.",
			READY = "'Tis a keyhole.",
			UNLOCKED = "Thar she goes!",
		},

		DIVININGRODSTART = "Thet rod be treasure.",
		DRAGONFLY = "Take Woodlegs ta yer tresures!",
		DRAGONFLYCHEST = "'Tis keepin me treasures safe n'sound.",
		DRAGONFLYFURNACE = 
		{
			HAMMERED = "Ye be lookin' sour, lad.",
			GENERIC = "I ain't seen one o'these before.", --no gems
			NORMAL = "It be cookin' a tad.", --one gem
			HIGH = "Now ye can really feel th'heat!", --two gems
		},
		DRAGONFRUIT = "'Tis full o'awe!",
		DRAGONFRUIT_COOKED = "Ye'll make a fine meal.",
		DRAGONFRUIT_SEEDS = "Ye need a home in th'ground.",
		DRAGONPIE = "'Tis treasure a pie.",
		DRAGON_SCALES = "'Tis a wondrous treasure.",
		DRUMSTICK = "Meat treat for Woodlegs!",
		DRUMSTICK_COOKED = "Hot meat fer supper.",
		DUG_BERRYBUSH = "Thet will be needin' some ground ta be useful.",
		DUG_BERRYBUSH_JUICY = "Thet will be needin' some ground ta be useful.",
		DUG_GRASS = "Thet will be needin' some ground ta be useful.",
		DUG_MARSH_BUSH = "Thet will be needin' some ground ta be useful.",
		DUG_SAPLING = "Thet will be needin' some ground ta be useful.",
		DURIAN = "Thet does ney smell like food!",
		DURIAN_COOKED = "Cookin' it up don't make it better.",
		DURIAN_SEEDS = "Seeds, see?",
		EARMUFFSHAT = "Thet hat be makin' squeakin' sounds.",
		EEL = "Ye scalawag!",
		EEL_COOKED = "Grilled urchin!",
		EGGPLANT = "She's a nice shade o'purple.",
		EGGPLANT_COOKED = "Look whut Woodlegs cooked.",
		EGGPLANT_SEEDS = "Seeds, see?",
		ENDTABLE = 
		{
			BURNT = "Thet be a burnt table.",
			GENERIC = "Woodlegs be wary.",
			EMPTY = "It be holdin' nuthin'.",
			WILTED = "It be wiltin'.",
			FRESHLIGHT = "Aye, thet's bright.",
			OLDLIGHT = "Get me some better lights, lubbers.",
		},		
		EVERGREEN =
		{
			BURNING = "Yer usefulness is running out.",
			BURNT = "Ye can't spread yer seeds now.",
			CHOPPED = "Yer earthly bonds run deep.",
			GENERIC = "'Tis some fine ship buildin' material.",
		},
		EVERGREEN_SPARSE =
		{
			BURNING = "Thet tree be burnin'.",
			BURNT = "Me won't be buildin' a ship from ye.",
			CHOPPED = "Yer lookin' a bit short.",
			GENERIC = "Thet tree be lookin' lumpy.",
		},
		EYEBRELLAHAT = "Ye'll keep an eye out fer ol'Woodlegs.",
		TWIGGYTREE = 
		{
			BURNING = "Yer usefulness be running out. If ever there were any.",
			BURNT = "Ye can't spread yer landy sticks no more.",
			CHOPPED = "Yer lookin' a bit short.",
			GENERIC = "Thet ain't boat-buildin' materiel.",			
			DISEASED = "Caught a case o'scurvy.",
		},
		TWIGGY_NUT_SAPLING = "It be growin' wee wood.",
        TWIGGY_OLD = "A shame, ye be.",
		TWIGGY_NUT = "I ain't no land lubbin' farmer.",
		EYEPLANT = "Aye sea ya.",
		EYETURRET = "'Tis a magical cannon.",
		EYETURRET_ITEM = "'Twill need a base ta be useful.",
		FARMPLOT =
		{
			BURNT = "'Tis blackened.",
			GENERIC = "'Spose th'ground be somewhat useful.",
			GROWING = "Aye, 'tis breaking free o'its earthly bonds.",
			NEEDSFERTILIZER = "Th'ground be lookin' hungry.",
		},
		FEATHERFAN = "'Tis a sail for me face.",
		MINIFAN = "'Tis a baby sail.",
		FEATHERHAT = "'Tis a hat o'birdy treasures.",
		FEATHER_CROW = "Thet feather be mighty oily.",
		FEATHER_ROBIN = "Me feet are tickle proof.",
		FEATHER_ROBIN_WINTER = "Whar did all th'colors go?",
		FEATHER_CANARY = "Thet be a gold-like feather.",
		FEATHERPENCIL = "It be fer markin' treasures! Like back when I sailed th'open waters!",
		COOKBOOK = "Th'galley's menu!",
		FEM_PUPPET = "Ye seem ta be in quite a bind.",
		FERTILIZER = "Feed some plants wit' this.",
		FIREFLIES =
		{
			GENERIC = "Tiny constellations. No good for navigation.",
			HELD = "Wee stars in me pockets.",
		},
		FIREHOUND = "Stay beck from me legs!",
		FIREPIT =
		{
			EMBERS = "'Tis nearly burnt out.",
			GENERIC = "Need ta be careful ta not burn me legs.",
			HIGH = "'Tis a mercy thet me ship is ney nearby.",
			LOW = "'Tis goin' ta need some fuel soon.",
			NORMAL = "Better keep me distance.",
			OUT = "Th'fire has left.",
		},
		FIRESTAFF = "Woodlegs learned not ta play wit' fire.",
		FIRESUPPRESSOR =
		{
			LOWFUEL = "'Tis nearly empty.",
			OFF = "Hope no fires start 'round 'ere.",
			ON = "This area be protected from fires.",
		},
		FISH = "Fish! One o'me favorites.",
		FISHINGROD = "'Tis a useful tool.",
		FISHSTICKS = "Woodlegs turned th'fish ta sticks.",
		FISHTACOS = "Th'crunchy shell be delicious.",
		FISH_COOKED = "Th'fish roasted up good.",
		FLINT = "Thet rock 'olds an edge.",
		FLOWER = 
		{
            GENERIC = "'Tis a pretty flower.",
            ROSE = "'Tis an evilly pretty flower.",
        },
		FLOWER_WITHERED = "Too much sun nay 'nough sea for ye.",
		FLOWERHAT = "'Tis a pretty flower 'at.",
		FLOWERSALAD = "'Tis a pretty flower meal.",
		FLOWER_CAVE = "Thet thar be a pretty flower.",
		FLOWER_CAVE_DOUBLE = "Thet thar be a pretty flower.",
		FLOWER_CAVE_TRIPLE = "Thet thar be a pretty flower.",
		FLOWER_EVIL = "'Tis an evil flower. Stay away!",
		FOLIAGE = "Landy stuff.",
		FOOTBALLHAT = "Keeps me melon safe.",
		FOSSIL_PIECE = "I've seen quite a bit o'bone in me days.",
        FOSSIL_STALKER =
        {
			GENERIC = "Any ol'pirate knows not ta mess wit'th'dead.",
			FUNNY = "Don't be makin' a mockery o'th'devil!",
			COMPLETE = "Ain't thet th'devil.",
        },
		STALKER = "It be a nasty shadow devil from th'depths!",
		STALKER_ATRIUM = "No nasty shadow devil ain't takin' ol'Woodlegs down!",
        STALKER_MINION = "Don't nibble me pegs!",
		THURIBLE = "It be beast baitin'!",
        ATRIUM_OVERGROWTH = "Me cants understand no landlubber symbols!",
		FROG =
		{
			DEAD = "Ta th'locker wit' ya!",
			GENERIC = "Dun hop near Woodlegs!",
			SLEEPING = "Th'demon sleeps.",
		},
		FROGGLEBUNWICH = "Newt sandwich!",
		FROGLEGS = "Somewhere there be a frog walkin' around wit' two peglegs!",
		FROGLEGS_COOKED = "Rubber chicken, me likes it.",
		GEMSOCKET =
		{
			GEMS = "'Tis lackin' treasure.",
			VALID = "'Tis full o'treasure.",
		},
		GHOST = "Ye 'ave ney business wit' me, spirit!",
		GLOMMER = "Ye'll be my first mate.",
		GLOMMERFLOWER =
		{
			DEAD = "'Tis drained.",
			GENERIC = "Somethin' magical be linked ta this.",
		},
		GLOMMERFUEL = "Leaves a mess on me deck, but Woodlegs loves 'em.",
		GLOMMERWINGS = "'E left me a lil gooey treasure.",
		GOATMILK = "Aye, fresh milk is rare at sea!",
		GOLDENAXE = "'Tis one way ta use treasure.",
		GOLDENPICKAXE = "Thet pick is made o'mighty soft metal.",
		GOLDENPITCHFORK = "Th'ground be lucky ta touch me treasure.",
		GOLDENSHOVEL = "Perfect for buryin' me treasures.",
		GOLDNUGGET = "Havin' gold around soothes me soul.",
		GOOSE_FEATHER = "Thet bird must be 'uge!",
		GRASS =
		{
			BARREN = "Ye won't be much use ta Woodlegs.",
			BURNING = "Stay off me legs.",
			GENERIC = "Looks like some tufts o'grass.",
			PICKED = "'Urry up an' grow back.",
			WITHERED = "Ye' won't be much use ta Woodlegs like thet.",
			DISEASED = "Thet plant ain't good for pickin'.",
			DISEASING = "I sense some scurvy comin' on thar!",
		},
		GRASSGEKKO = 
		{
			GENERIC = "It be a grass lizard.",	
			DISEASED = "Looks like ye attracted a case o'scurvy, matey.",
		},
		GRASS_UMBRELLA = "A lil water ne'er hurt nobody.",
		GREENAMULET = "Wearin' this ol'amulet makes me feel so smart!",
		GREENGEM = "Emerald treasures.",
		GREENSTAFF = "I's ain't against some helpful magic.",
		GREEN_CAP = "Not me top choice o'grub.",
		GREEN_CAP_COOKED = "Th'witch doctor told Woodlegs it'd cure me crazies.",
		GREEN_MUSHROOM =
		{
			GENERIC = "Don't see many o'these at sea.",
			INGROUND = "'Tis below sea level.",
			PICKED = "'Tis out sailin'.",
		},
		GUACAMOLE = "Woodlegs'll find more chips fer dippin'!",
		GUNPOWDER = "Packs quite th'punch.",
		HAMBAT = "Made wit' metal from th'finest meat mines.",
		HAMMER = "Ye could put quite a 'ole in a boat wit' tis.",
		HAWAIIANSHIRT = "'Tis light an stylish.",
		HEALINGSALVE = "Thet will soothe me wounds.",
		HEATROCK =
		{
			COLD = "Me chilly rock friend.",
			FROZEN = "Me rock friend be freezin'!",
			GENERIC = "Me rock friend.",
			HOT = "Me rock friend be gettin' toasty.",
			WARM = "Me cozy rock friend.",
		},
		HOME = "Captain's quarters.",
		HOMESIGN =
		{
			BURNT = "Used ta be markin' somethin'.",
			UNWRITTEN = "It be blank.",
			GENERIC = "Must be somethin' important 'round 'ere.",
		},
		ARROWSIGN_POST =
		{
			GENERIC = "It be a navigation tip.",
            UNWRITTEN = "It be poitin' ta nowhere.",
			BURNT = "It ain't be a good tip now.",
		},
		ARROWSIGN_PANEL =
		{
			GENERIC = "It be a navigation tip.",
            UNWRITTEN = "It be poitin' ta nowhere.",
			BURNT = "It ain't be a good tip now.",
		},
		HONEY = "Sweet an' sticky.",
		HONEYCOMB = "Thar be larvae a-wrigglin' in ther.",
		HONEYHAM = "A feast fit fer piratin'!",
		HONEYNUGGETS = "Golden nuggets!",
		HORN = "Play th' song o'th'Beefalo.",
		HOTCHILI = "Makes m'grateful me tongue's wooden.",
		HOUND = "Hey thar, beastie.",
		HOUNDCORPSE =
		{
			 GENERIC = "Need me crew ta clean up afterwards.",
			 BURNING = "Send 'er ta sea, be it a true pirate's funeral.",
			 REVIVING = "By Davy Jones' locker!!",
		},
		HOUNDBONE = "Th'remains o'somethin'.",
		HOUNDMOUND = "Th'ounds be livin' thar.",
		HOUNDSTOOTH = "A few moor o'these and Woodlegs'll have a full set!",
		ICE = "Ney good fer sailin'.",
		ICEBOX = "Good fer keepin' me food.",
		ICECREAM = "Refreshin' on a hot day.",
		ICEHAT = "Keeps me 'ead cool.",
		ICEHOUND = "Packs quite a chilly bite!",
		ICEPACK = "A mighty chill runs through this here pack.",
		ICESTAFF = "Thet staff be chillin'.",
		INSANITYROCK =
		{
			ACTIVE = "Thet rock be givin' Woodlegs a 'eadache.",
			INACTIVE = "Thet rock 'as a bad aura.",
		},
		JAMMYPRESERVES = "Prepared ta perfection.",
		KABOBS = "All food be piratin' food!",
		KILLERBEE =
		{
			GENERIC = "Ye don't be lookin' very friendly.",
			HELD = "Yer mine, now.",
		},
		KNIGHT = "Cavalry's'ere!",
		KNIGHT_NIGHTMARE = "Yer not lookin' very good.",
		KOALEFANT_SUMMER = "Woodlegs be knowin' yer fur be mighty warm.",
		KOALEFANT_WINTER = "Woodlegs be knowin' yer fur be mighty warm. An 'tis so thick.",
		KOALEFANT_CARCASS = "Been sent down ta th' ol'gravy basket ready fer feastin'.",
		KRAMPUS = "What did ol'Woodlegs do?",
		KRAMPUS_SACK = "Woodlegs'll ne'er leave treasure behind again!",
		LANTERN = "'Tis a fine light.",

		HUTCH = "Whut'n blazes arre ye?",
        HUTCH_FISHBOWL =
        {
            GENERIC = "'Tis a nice bowl o'fish.",
            WAITING = "'Tis be takin' its ocean nap.",
        },
		LAVASPIT =
		{
			COOL = "Th'fire be bitten.",
			HOT = "I sees a sea o'red!",
		},
		LAVA_POND = "Hope me legs don't catch.",
		LAVAE_COCOON = "It ain't be so hot now.",
		LAVAE = "Stay away for me legs.",
		LAVAE_PET = 
		{
			STARVING = "Th'creature be wantin' some grub.",
			HUNGRY = "Thet creature be hungry.",
			CONTENT = "Thet be a happy little critter.",
			GENERIC = "Aye don't know what ye are but you be cute.",
		},
		LAVAE_EGG = 
		{
			GENERIC = "Thet be a warm egg right thar.",
		},
		LAVAE_EGG_CRACKED =
		{
			COLD = "Th'warm egg ain't be warm!",
			COMFY = "Th'egg be needin' a pirate hat. ",
		},
		LAVAE_TOOTH = "What in th'blazes?",

		LEIF = "Glad me legs don't do that!",
		LEIF_SPARSE = "Glad me legs don't do that!",
		LICHEN = "Munch on thet!",
		LIGHTBULB = "Bright night light.",
		LIGHTNINGGOAT =
		{
			CHARGED = "Me better keep me distance.",
			GENERIC = "Them arr some mean lookin'orns.",
		},
		LIGHTNINGGOATHORN = "Looks good fer hittin' stuff.",
		LIGHTNING_ROD =
		{
			CHARGED = "Dodged thet!",
			GENERIC = "Protectin' me from th'eavens.",
		},
		LITTLE_WALRUS = "He'll grow in ta a fine pirate.",
		LIVINGLOG = "It be cursed wood.",
		LIVINGTREE = "Don't be wakin'im up.",
		LOG =
		{
			BURNING = "Keep back!",
			GENERIC = "Thet's me legs' brother.",
		},
		LUREPLANT = "Plunderin' plant, best keep me wits about me.",
		LUREPLANTBULB = "Th'heart o'th'plunder plant.",
		MALE_PUPPET = "Ye be trapped!",
		MANDRAKE_ACTIVE = "Quit yer squawkin'!",
		MANDRAKE_PLANTED = "A plant o'legend.",
		MANDRAKE = "Twas a noisy little beast.",
		MANDRAKESOUP = "'Tis a magic brew.",
		MANDRAKE_COOKED = "Woodlegs cooked it up!",
		MANRABBIT_TAIL = "A bit o'luck!",
		MAPSCROLL = "Thet be great treasure map potential!",
		MARBLE = "Strong and sandy smooth.",
		MARBLEBEAN = "Bring me a shiny stalk o'treasure!",
		MARBLEBEAN_SAPLING = "Growin' wee treasures.",
        MARBLESHRUB = "Bright 'n shiny ye be!",
		MARBLEPILLAR = "A marble peg leg. For a giant.",
		MARBLETREE = "Strange trees...",
		MARSH_BUSH =
		{
		    BURNT = "Ye be spiked no more!",
			BURNING = "Burnin'.",
			GENERIC = "She's got wee swords.",
			PICKED = "Ye poked me!",
		},
		MARSH_PLANT = "Green like me mermaid's eyes.",
		MARSH_TREE =
		{
			BURNING = "Burnin'.",
			BURNT = "Better 'em then me!",
			CHOPPED = "Thet be all thet's left.",
			GENERIC = "A tree wit' daggers.",
		},
		MAXWELL = "I'd like ta throw 'em overboard.",
		MAXWELLHEAD = "'E's got them villainous eyes.",
		MAXWELLLIGHT = "'Tis a dark flame.",
		MAXWELLLOCK = "Me don't think thet be unlockin' any treasure...",
		MAXWELLPHONOGRAPH = "A bewitched tune it plays.",
		MAXWELLTHRONE = "Not a seat me wants ta take.",
		MEAT = "Flesh o'th'beast.",
		MEATBALLS = "Me lunch!",
		MEATRACK =
		{
			BURNT = "Thet be a shame.",
			DONE = "She's ready!",
			DRYING = "Swing, swing, into me mouth!",
			DRYINGINRAIN = "Th'rain don't be helpin'.",
			GENERIC = "Hangin' spot fer me meats.",
			DONE_NOTMEAT = "She's ready!",
            DRYING_NOTMEAT = "Swing, swing, into me mouth!",
            DRYINGINRAIN_NOTMEAT = "Th'rain don't be helpin'.",
		},
		MEAT_DRIED = "Jerky lasts me months at sea!",
		MERM = "Ye scalawag.",
		MERMHEAD =
		{
			BURNT = "'Tis crispy.",
			GENERIC = "Beware...",
		},
		MERMHOUSE =
		{
			BURNT = "Good riddance.",
			GENERIC = "I don't trust 'em.",
		},
		MINERHAT = "A light at sea keeps me calm.",
		MINOTAUR = "I've 'eard tales o'this devil...",
		MINOTAURCHEST = "How does Woodlegs get it back ta th'ship?!",
		MINOTAURHORN = "Aye, a treasure indeed!",
		MOLE =
		{
			ABOVEGROUND = "Whut's'e sniffin'?",
			HELD = "Quiet me pet.",
			UNDERGROUND = "'Tis swimmin' under th'dirt!",
		},
		MOLEHAT = "Gives eyes of a ghost in th'night!",
		MOLEHILL = "E's 'avin' a sleep in th'dirt.",
		MONKEY = "Me once saw a monkey wit' a hook claw.",
		MONKEYBARREL = "They be keepin' treasures in thar, no doubt.",
		MONSTERLASAGNA = "Not me favorite.",
		MONSTERMEAT = "Bit sour thet is.",
		MONSTERMEAT_DRIED = "Dried thet beast, I did.",
		MOOSE = "Keep'er off me boat!",
		MOOSEEGG = "Whut beast lurks wit'in!",
		MOSQUITO =
		{
			GENERIC = "'Is buzzin' makes me nervous.",
			HELD = "Better not bite me through me pocket!",
		},
		MOSQUITOSACK = "Looks like a jellyfish.",
		MOSSLING = "Ahoy big duck!",
		MOUND =
		{
			DUG = "I had ta check fer treasure!",
			GENERIC = "Not a bad place ta hide yer silver.",
		},
		MULTITOOL_AXE_PICKAXE = "'Tis multipurpose.",
		MUSHROOMHAT = "This ain't be proper pirate gear.",
        MUSHROOM_LIGHT2 =
        {
            ON = "Thet's a magical 'shroom for ye.",
            OFF = "Whar th'light go?",
            BURNT = "It be burnt.",
        },
        MUSHROOM_LIGHT =
        {
            ON = "Thet be a glowy 'shroom.",
            OFF = "'Tis a 'shroom.",
            BURNT = "Be smellin' bad.",
        },
		SLEEPBOMB = "Packs a punch'o'sleep.",
        MUSHROOMBOMB = "Hit th'deck! Thisn's a hitter!",
        SHROOM_SKIN = "It be warty like an ol'pirate's skin.",
        TOADSTOOL_CAP =
        {
            EMPTY = "No treasure lies thar.",
            INGROUND = "Somethin's rumblin' down thar!",
            GENERIC = "Me ain't seen no shroom like thet before.",
        },
        TOADSTOOL =
        {
            GENERIC = "Thet be an ugly devil!",
            RAGE = "Th'devil's gettin' angry!",
        },
        MUSHROOMSPROUT =
        {
            GENERIC = "I ain't wantin' ta be messin' wit thet.",
            BURNT = "Whut scoundrel be messin' wit it?",
        },
        MUSHTREE_TALL =
        {
            GENERIC = "Whut's thet 'shroom doin' way up thar?",
            BLOOM = "It be smelly, thet.",
        },
        MUSHTREE_MEDIUM =
        {
            GENERIC = "'Shroom tree, now me's seen it all.",
            BLOOM = "It be bloomin' in here.",
        },
        MUSHTREE_SMALL =
        {
            GENERIC = "It glows!",
            BLOOM = "You do you, lad.",
        },
        MUSHTREE_TALL_WEBBED = "Spider's be pickin' favorites.",
        SPORE_TALL = "It be a driftin' lad.",
        SPORE_MEDIUM = "Hope it ain't be spyin' on me treasure.",
        SPORE_SMALL = "It ain't no treasure, but it be pretty.",
        SPORE_TALL_INV = "Got ye in me grasps.",
        SPORE_MEDIUM_INV = "Got ye in me grasps.",
        SPORE_SMALL_INV = "Got ye in me grasps.",
		NIGHTLIGHT = "Don't provide much comfort, do it?",
		NIGHTMAREFUEL = "I've seen thet dark magic asea...",
		NIGHTMARELIGHT = "Woodlegs don't trust this light.",
		NIGHTMARE_TIMEPIECE =
		{
			CALM = "All be calm.",
			DAWN = "It be slippin' away...",
			NOMAGIC = "Not a peep o'magic.",
			STEADY = "Th'nightmare's at its peak!",
			WANING = "Me pegs feel it be dyin'.",
			WARN = "Somethin' feels off...",
			WAXING = "It be buildin'!",
		},
		NIGHTSTICK = "'Tis a glowin' sword!",
		NIGHTSWORD = "Me cursed cutlass.",
		NITRE = "Cannon food thet be.",
		ONEMANBAND = "Whut's th'point o'this?",
		OASISLAKE = 
		{
			GENERIC = "I sees me future...",
			EMPTY = "Nothin' like th'feel o'sand 'neath me pegs.",
		},
		ORANGEAMULET = "Me magic chain.",
		ORANGEGEM = "Woodlegs loves all th'gems!",
		ORANGESTAFF = "Magic peg legs!",
		OPALSTAFF = "Whut does this thing do?",
        OPALPRECIOUSGEM = "Me mates would be clawing ta get their mitts on this one! Yarrharr!",
		PANDORASCHEST = "Yo-ho-ho! Treasure for me!",
		PANFLUTE = "Blankets ye inta a bewitched sleep.",
		PAPYRUS = "Aye! A fresh map!",
		WAXPAPER = "Nay good fer maps.",
		PENGUIN = "A sure sign o'ice at sea.",
		PERD = "Whut a maniac!",
		PEROGIES = "Woodlegs could eat a chest full o'these!",
		PETALS = "Them flowers ain't comin' back ta life.",
		PETALS_EVIL = "Bewitched flowers.",
		PICKAXE = "Let's get them treasures!",
		PIGGUARD = "Don't ye be snortin' at me!",
		PIGGYBACK = "From th'flesh o'th'boar.",
		PIGHEAD =
		{
			BURNT = "Crispy.",
			GENERIC = "Sometimes them eyes be glowin'...",
		},
		PIGHOUSE =
		{
			BURNT = "Fire's done 'is work here.",
			FULL = "'Ere piggy, piggy.",
			GENERIC = "Looks like an outhouse, don't it?",
			LIGHTSOUT = "Gettin' some shut eye, 'e is.",
		},
		PIGKING = "'E's rich, this one.",
		PIGMAN =
		{
			DEAD = "Goodnight pig.",
			FOLLOWER = "Come along piggy!",
			GENERIC = "Ye think 'e speaks Latin?",
			GUARD = "Don't ye be snortin' at me!",
			WEREPIG = "Whut happened ta ye?!",
		},
		PIGSKIN = "'E lost 'is bottom.",
		PIGTENT = "Whut's inside?",
		PIGTORCH = "Bet thet be worth a pretty penny...",
		PIKE_SKULL = "Keep walkin'.",
		PINECONE = "Th'seed o'th'tree.",
        PINECONE_SAPLING = "Grow inta ship wood me sweet.",
		LUMPY_SAPLING = "Me ain'ts gonna question ship wood.",
		PITCHFORK = "Can I be diggin' treasures wit' this?",
		PLANTMEAT = "A suspect meat.",
		PLANTMEAT_COOKED = "Cooked it up, I did.",
		PLANT_NORMAL =
		{
			GENERIC = "Can Woodlegs eat it?",
			GROWING = "Waitin' and waitin'.",
			READY = "Arr, she's ready!",
			WITHERED = "Too hot fer her wee roots.",
		},
		POMEGRANATE = "Me mates tell me this be good fer me heart.",
		POMEGRANATE_COOKED = "Seedy.",
		POMEGRANATE_SEEDS = "Seeds thet grow more seeds.",
		POND = "Ye couldn't fit a real ship in this.",
		POND_ALGAE = "Ye couldn't fit a real ship in this.",
		POOP = "What scallywag left their poop layin' about!",
	    GIFT = "Why would ye store yer treasure in thet?",
        GIFTWRAP = "This ain't touchin' me treasure.",
		POTTEDFERN = "Now whut use 'ave I fer a house plant?",
		SUCCULENT_POTTED = "I ain't 'ave any use fer no house plant.",
		SUCCULENT_PLANT = "Land seaweed, methinks.",
		SUCCULENT_PICKED = "Whut's me doin' carryin' this?",
		SENTRYWARD = "Keep ye nasty looks off me treasure.",
		TOWNPORTAL =
        {
			GENERIC = "This magic's muddin' up me noggin'.",
			ACTIVE = "It be ready fer rowin'.",
		},
        TOWNPORTALTALISMAN = 
        {
			GENERIC = "Ye'd never see these out in th'open waters.",
			ACTIVE = "I'd rather sail th'sea.",
		},
        WETPAPER = "Every ol'pirate knows how ta keep grips on wet maps!",
        WETPOUCH = "'Tis be treasure from down below!",
		MOONROCK_PIECES = "Ye don't see thet everyday.",
		MOONBASE =
        {
            GENERIC = "Where thar be devil's magic, thar be treasure!",
            BROKEN = "Be it ancient ruins? Treasure must be about!",
            STAFFED = "Arrr! Open th'gates ta treasure t'b'sure!",
            WRONGSTAFF = "Ye have th'wrong idea fer treasure swappin'!",
			MOONSTAFF = "Looks good fer th'grabbin' fer me.",
        },
		MOONDIAL = 
        {
			GENERIC = "It be following th'moon lights.",
			NIGHT_NEW = "Th'moon be hidin'.",
			NIGHT_WAX = "Th'moon be waxin'.",
			NIGHT_FULL = "Th'moon shows 'er face!",
			NIGHT_WANE = "Th'moon be wanin'.",
			CAVE = "Ain't nuthin' ta measure down 'ere.",
			GLASSED = "If only it could tell me fortune!",
        },
		POWCAKE = "This really be food?",
		PUMPKIN = "Would make a good cannon ball.",
		PUMPKINCOOKIE = "I have a sweet no tooth.",
		PUMPKIN_COOKED = "Me ships cook used ta add a dash o'cinnamon.",
		PUMPKIN_LANTERN = "A wee bit heavy fer a lantern.",
		PUMPKIN_SEEDS = "I ain't th'farmin' type, but I do loves me some grub...",
		PURPLEAMULET = "Looks good on Woodlegs!",
		PURPLEGEM = "Shiny and valuable!",
		RABBIT =
		{
			GENERIC = "Twitchy wee thing.",
			HELD = "Stay in me pockets now.",
		},
		RABBITHOLE =
		{
			GENERIC = "Think they got treasures down thar?",
			SPRING = "Them lil' rabbits ain't comin' out.",
		},
		RABBITHOUSE =
		{
			BURNT = "Burnt to th'earth!",
			GENERIC = "Thet be one big carrot.",
		},
		RAINCOAT = "Keeps me blouse dry.",
		RAINHAT = "Keeps yer head dry.",
		RAINOMETER =
		{
			BURNT = "Ye couldn't predict fire could ye!",
			GENERIC = "Sailor's arre walkin' rain-thingies!",
		},
		RATATOUILLE = "Scurvy-fightin' super food!",
		RAZOR = "Shavin's fer princes and lawyers.",
		REDGEM = "It gleams like th'sun!",
		RED_CAP = "Hope th'color ain't a warnin'...",
		RED_CAP_COOKED = "Me favorite color o'shroom.",
		RED_MUSHROOM =
		{
			GENERIC = "It be an alarming colored 'shroom.",
			INGROUND = "It don't wanna 'shroom up yet.",
			PICKED = "It's outta 'shrooms.",
		},
		REEDS =
		{
			BURNING = "They burn up fast.",
			GENERIC = "Betcha I can whistle a tune wit' these!",
			PICKED = "It can't reed.",
		},
		REFLECTIVEVEST = "Right back at ye, sunny boy.",
		RELIC =
		{
			BROKEN = "It ain't worth even half a dubloon now.",
			GENERIC = "Me wonders if me could sell it on th'black market?",
		},
		RESEARCHLAB =
		{
			BURNT = "I guess science ain't so hot after all.",
			GENERIC = "I heard 'bout this science stuff. 'Tis up'n comin'.",
		},
		RESEARCHLAB2 =
		{
			BURNT = "It won't be doin' whatever it done before.",
			GENERIC = "It does whut?",
		},
		RESEARCHLAB3 =
		{
			BURNT = "Th'dark arts didn't save ye.",
			GENERIC = "Witchcraft wit' this'un, no two ways about'er.",
		},
		RESEARCHLAB4 =
		{
			BURNT = "Good! I didn't like saying yer name anyhow.",
			GENERIC = "Me ain't had enuff learnin' ta say a word like thet.",
		},
		RESURRECTIONSTATUE =
		{
			BURNT = "Won't be resurrectin' nuthin' anymores.",
			GENERIC = "Don't capture me likeness at all!",
		},
		RESURRECTIONSTONE = "Me nose smells witchcraft...",
		ROBIN =
		{
			GENERIC = "Ye don't talk do ye?",
			HELD = "Woodlegs'll takes good care o'ye.",
		},
		ROBIN_WINTER =
		{
			GENERIC = "She's a beaut.",
			HELD = "She's a cold birdy.",
		},
		ROBOT_PUPPET = "Locked up!",
		ROCK = "Would wreck a hull.",
		ROCKS = "They sink.",
		ROCKY = "Back ye clawed devil!",
		PETRIFIED_TREE = "Ye can't make wood outta rocks.",
		ROCK_PETRIFIED_TREE = "Ye can't make wood outta rocks.",
		ROCK_PETRIFIED_TREE_OLD = "Ye can't make wood outta rocks.",
		ROCK_ICE =
		{
			GENERIC = "Better here than 'neath me boat tearin' at her hull.",
			MELTED = "Needs ta chill out.",
		},
		ROCK_ICE_MELTED = "Need a cold snap so's it can freeze again.",
		ROCK_LIGHT =
		{
			GENERIC = "A puddle o'fire.",
			LOW = "It be goin' out.",
			NORMAL = "Not bad fer settin' by.",
			OUT = "She's out.",
		},
		CAVEIN_BOULDER =
        {
            GENERIC = "Thet'd give ye quite th'wreck at sea.",
            RAISED = "Other hauls be blockin' th'path.",
        },
		ROOK = "She's full steam ahead!",
		ROOK_NIGHTMARE = "Thet beast's been cursed.",
		ROPE = "Fer lashin' stuff ta stuff.",
		ROTTENEGG = "Smells like crew quarters after chili night.",
		ROYAL_JELLY = "Thet ain't me favored kind o'jelly.",
        JELLYBEAN = "I ain't got teeth left fer 'em t'stick to!",
		SADDLE_BASIC = "'Tis a sail o'saddles.",
        SADDLE_RACE = "Puts th'wind in me sails.",
        SADDLE_WAR = "Fer only th'fearless.",
        SADDLEHORN = "A hook fer hookin' saddles!",
		SALTLICK = "Saltier than ol'Woodlegs.",
        BRUSH = "Fur be muckin' up th'bristles.",
		RUBBLE = "Just a pile o'rubble.",
		RUINSHAT = "A crown o'jewels.",
		RUINS_BAT = "Whut a beaut!",
		RUINS_RUBBLE = "Woodlegs can build this back up.",
		SANITYROCK =
		{
			ACTIVE = "Thar be a crazy lookin' rock.",
			INACTIVE = "Thet crazy rock be missin' pieces.",
		},

		SAPLING =
		{
			BURNING = "Th'wood burns.",
			GENERIC = "Wee wood.",
			PICKED = "Thar be no more pickin' o'this one fer a spell.",
			WITHERED = "Needs a splash o'rum.",
			DISEASED = "Not even ol'rum could save thet plant.",
			DISEASING = "Thet be a sign o'scurvy.",
		},
		SCARECROW = 
   		{
			GENERIC = "What are ye staring at ye scallywag?",
			BURNING = "Learnin' yer lesson fer bein' a looker.",
			BURNT = "Thet be ashes.",
   		},
		SCULPTINGTABLE=
   		{
			EMPTY = "I ain't no sculptin' landlubber.",
			BLOCK = "Sculpt me some new legs.",
			SCULPTURE = "Yarrr!",
			BURNT = "Arrrr!",
   		},
		SCULPTURE_KNIGHTHEAD = "Th'bust what sinks a ship!",
		SCULPTURE_KNIGHTBODY = 
		{
			COVERED = "Ain't seen me a statue like thets before.",
			UNCOVERED = "What else are ye hidin' ya scallywag?",
			FINISHED = "All beck togetha' again.",
			READY = "Hit th'decks!",
		},
        SCULPTURE_BISHOPHEAD = "Ain't no challenge ol'Woodlegs can't complete!",
		SCULPTURE_BISHOPBODY = 
		{
			COVERED = "Th'marble be bewitched, I tells ye!",
			UNCOVERED = "Whut be we uncoverin'?",
			FINISHED = "No need ta thank me, 'tis a pirate's honor.",
			READY = "Hit th'decks!",
		},
        SCULPTURE_ROOKNOSE = "If I had me boat I could lug ya.",
		SCULPTURE_ROOKBODY = 
		{
			COVERED = "Thet be remindin' me o'th'horrors of them icebergs.",
			UNCOVERED = "Have ye seen better days, lad?",
			FINISHED = "Dug me up an ol'scallywag!",
			READY = "Hit th'decks!",
		},
		GARGOYLE_HOUND = "Thet be a foul warnin' sign! Treasure be lurkin' near!",
        GARGOYLE_WEREPIG = "Some sort'a warnin' sign! Whar be th'treasure?",
		SEEDS = "Woodlegs ain't no landlubbin' farmer!",
		SEEDS_COOKED = "Plant 'em in me belly.",
		SEWING_KIT = "Get me a swab ta do me sewin'.",
		SEWING_TAPE = "Can't fix a 'ole wit some landlubbin' tapes!",
		SHOVEL = "Ima' gold digger!",
		SIESTAHUT =
		{
			BURNT = "No good fer nappin' now.",
			GENERIC = "I hate sleeping on land.",
		},
		SILK = "Used in th'finest pirate shirts!",
		SKELETON = "Yer luck ran out, matey.",
		SKULLCHEST = "Whut treasures it musta devoured.",
		SLURPER = "Yer an ugly, scallywag.",
		SLURPERPELT = "Tha smell!",
		SLURPER_PELT = "Tha smell!",
		SLURTLE = "Look at ye slurtle'n around.",
		SLURTLEHAT = "On me head?",
		SLURTLEHOLE = "They slurtle their way in there, d'they?",
		SLURTLESLIME = "Looks 'ike somethin' I coughed up.",
		SLURTLE_SHELLPIECES = "I ain't one fer puzzlers.",
		SMALLBIRD =
		{
			GENERIC = "Whoot sorta cackler arrr ye?",
			HUNGRY = "Yer feather-belly grumblin'?",
			STARVING = "Yer fit ta waste away.",
			SLEEPING = "Nary a peep ta be!",
		},
		SMALLMEAT = "Tis' small, but tis' meat.",
		SMALLMEAT_DRIED = "Jolly jerky!",
		SNURTLE = "I'd shell ye if I had me boat handy.",
		SPAT = "Yer a crusty ol'beast!",
		PHLEGM = "Are ye sure ol'Woodlegs didn't hock thet one up?",
		SPEAR = "I could jabber on wit' this all day.",
		SPIDER =
		{
			DEAD = "Good!",
			GENERIC = "Ye must really love th'land wit' all them legs.",
			SLEEPING = "Dreaming o'my boot heel.",
		},
		SPIDERDEN = "I prefur' a den o'thieves.",
		SPIDEREGGSACK = "Expectin' ol'Woodlegs play wetnurse ta creepy crawlies?",
		SPIDERGLAND = "I ate worse.",
		SPIDERHAT = "Wear a spider on me head? Bah!",
		SPIDERHOLE = "Whut lives innna hole ain't worth me time.",
		SPIDERHOLE_ROCK = "Whut lives innna hole ain't worth me time.",
		SPIDERQUEEN = "Woodlegs yields ta no queen, spider or udderwise!",
		SPIDER_DROPPER = "Yellow sneaks!",
		SPIDER_HIDER = "Infernal spiders!",
		SPIDER_SPITTER = "I reckon I can spit further than ye.",
		SPIDER_WARRIOR =
		{
			DEAD = "Warrior my eye!",
			GENERIC = "Eight legs ta lub th'land wit'.",
			SLEEPING = "Ye dreaming o'flies?",
		},
		SPOILED_FOOD = "Bin eaten worse.",
		STAFFLIGHT = "Th'stars aren't under th'domain o'man.",
		STAFFCOLDLIGHT = "It be a frigid star!",
		STAFF_TORNADO = "Whut unholy nonsense be 'tis?",
		STALAGMITE = "Me likes rocks whar I can see'em.",
		STALAGMITE_TALL = "Me likes rocks whar I can see'em.",
		LAVA_POND_ROCK = "Me likes rocks whar I can see'em.",
		
		STATUEGLOMMER =
		{
			EMPTY = "It broke up.",
			GENERIC = "Ugly statue.",
		},
		STAGEHAND =
        {
			AWAKE = "Keep yer greasy mitts off me treasure, scallywag!",
			HIDING = "Me nose be catchin' a whiff o'grabby hands!",
        },
		STATUE_MARBLE = 
        {
        	GENERIC = "Thet ain't be havin' any use ta me.",
        	TYPE1 = "Yer missing ye face!",
        	TYPE2 = "Ye be missin' yer head thar, love!",
        	TYPE3 = "It could have a lot of uses on me ol'ship.",
    	},
		STATUEHARP = "Yer missing ye head!",
		STATUEMAXWELL = "I'd know me new matey's face anywheres!",
		STEELWOOL = "It'd give ye quite th'scratchin.",
		STINGER = "Bee dropped its bottom.",
		STRAWHAT = "Not fit fer a captain. But makes fer a cool head.",
		STUFFEDEGGPLANT = "Stuffed wit' whut?",
		SUNKBOAT = "Some mateys luck ran out...",
		SWEATERVEST = "Fit fer a swab.",
		TAFFY = "Sticks ta me no teeth.",
		TALLBIRD = "Ye'd be a might less tall if I had me sabre wit' me!",
		TALLBIRDEGG = "When will th'cackler be hatchin'.",
		TALLBIRDEGG_COOKED = "Me likes me tallbirdy yolk runny!",
		TALLBIRDEGG_CRACKED =
		{
			COLD = "Needs a warm cacklers bum settin' on it.",
			GENERIC = "Do it be hatchin'?",
			HOT = "Don't be gettin' hard boiled on me.",
			LONG = "I don't be seein' this eggy hatchin' any time soon.",
			SHORT = "Th'cacklers comin' anytime!",
		},
		TALLBIRDNEST =
		{
			GENERIC = "Thet thar's a big egg!",
			PICKED = "Th'egg basket's empty.",
		},
		TEENBIRD =
		{
			GENERIC = "Ye ain't too tall arrr ye?",
			HUNGRY = "I best find some food fer yer belly.",
			STARVING = "Don't ye be thinkin' on eatin' ol'Woodlegs!",
			SLEEPING = "An' keep yer peeper shut!",
		},
		TELEBASE =
		{
			GEMS = "Needs more o'them purpley gemmies.",
			VALID = "She be in focus now!",
		},
		TELEPORTATO_BASE =
		{
			ACTIVE = "I don't be knowin' where it leads, but it be ready ta take me.",
			GENERIC = "Could take ol'Woodlegs ta seas-end!",
			LOCKED = "Thar be a piece a'missin'.",
			PARTIAL = "It be nigh ready.",
		},
		TELEPORTATO_BOX = "A simple pirate like me ain't be knowin' whut this be fer.",
		TELEPORTATO_CRANK = "Ta my eye it be a crank fer crankin'.",
		TELEPORTATO_POTATO = "Strange armor be this.",
		TELEPORTATO_RING = "A ring wit' no jewels, bah!",
		TELESTAFF = "I prefer a fast ship and th'wind at me back.",
		TENT =
		{
			BURNT = "Thet's whut ye get fer trustin' yer snoozes ta land.",
			GENERIC = "I prefers a water bed.",
		},
		TENTACLE = "Like in me dreams!",
		TENTACLESPIKE = "Aha! Now th'creepin' dread be in me own hands!",
		TENTACLESPOTS = "Me memory be spotty. Whut's this fer again?",
		TENTACLE_GARDEN = "Thar be slime!",
		TENTACLE_PILLAR = "Would make fer a slippery mast.",
		TENTACLE_PILLAR_ARM = "Thar be slime!",
		THULECITE = "Wonder whut this be worth?",
		THULECITE_PIECES = "Tastes a might goldy.",
		TOPHAT = "Even a dirty ol'pirate could use a dapper day.",
		TORCH = "Now I can spot treasure in th'dark!",
		TRAILMIX = "I prefer sea snacks.",
		TRANSISTOR = "Whut be this?",
		TRAP = "I prefer ta meet me foes head-on.",
		TRAP_TEETH = "I may be toothless, but this trap ain't.",
		TRAP_TEETH_MAXWELL = "I ain't fallin' fer thet.",
		TREASURECHEST =
		{
			BURNT = "Naaaaaaaaaaaaaay!",
			GENERIC = "Aye, th' meaning o'life inna box.",
		},
		TREASURECHEST_TRAP = "Treach-sure!",
		SACRED_CHEST = 
		{
			GENERIC = "Aye, this be what Woodlegs lives fer!",
			LOCKED = "Don't be givin' me ye nasty looks! Give me yer treasures!",
		},
		TREECLUMP = "Get oot o'me way!",
		TRINKET_1 = "Lost me marbles.",
		TRINKET_2 = "If only it were real.",
		TRINKET_3 = "Gord knows his way around a knot.",
		TRINKET_4 = "Red hatted devil!",
		TRINKET_5 = "This is no kind o' ship me knows.",
		TRINKET_6 = "Whut swab tied this knot?",
		TRINKET_7 = "Aye, th'newest gaming craze!",
		TRINKET_8 = "Bung be hard.",
		TRINKET_9 = "Me ain't picky 'bout matcheys..",
		TRINKET_10 = "My prayers arrr answered!",
		TRINKET_11 = "Ye quick-tongued box o'bolts!",
		TRINKET_12 = "Whut'n'th'whut?",
		TRINKET_13 = "Red hatted devil!",
		TRINKET_14 = "Thet wee tea vessel be sporutin' leaks!",
		TRINKET_15 = "Those lubbers be layin' traps for me pegs t'go slippin'!",
		TRINKET_16 = "Those lubbers be layin' traps for me pegs t'go slippin'!",
		TRINKET_17 = "Th'only dagger I be trustin' those layabouts on land wit'!",
		TRINKET_18 = "More loot fer th'cap'n's quarters! Bring 'er on th'ship!",
		TRINKET_19 = "It just ain't gots its sea legs yet!",
		TRINKET_20 = "Which lubber wants ta volunteer t'scratch ol'Woodlegs achin' back?",
		TRINKET_21 = "Bah! Ye beat all th'eggs outta it!",
		TRINKET_22 = "Wee rope be foul, no sail could set wit'it!",
		TRINKET_23 = "'Tis be makin' a mockery of ol'Woodlegs.",
		TRINKET_24 = "No room fer catty antiquities on me ship!",
		TRINKET_25 = "Arrr. I know th'smell o'musty wood anywheres!",
		TRINKET_26 = "Yarr! Woodlegs's own portatohole!",
		TRINKET_27 = "Wrap it around yer mitts if'n ye need a hook.",
		TRINKET_28 = "Seize th'castle!",
        TRINKET_29 = "Seize th'castle!",
        TRINKET_30 = "This toy can't ride me ta victory.",
        TRINKET_31 = "This toy can't ride me ta victory.",	
		TRINKET_32 = "Methinks Woodlegs sees hoards an'hoards o'gold in me future!",
        TRINKET_33 = "Whut kinda treasure be this?",
        TRINKET_34 = "I wished me up some more gold!",
        TRINKET_35 = "Fill 'er wit'sea water fer safe keepin'.",
		TRINKET_36 = "Woodlegs ain't be havin' th'teeth fer this'un!",
		TRINKET_37 = "ARRR! I hope thet don't happen to me pegs.",
		TRINKET_38 = "Hard ta spy wit'th'eye wit'.", -- Binoculars Griftlands trinket
        TRINKET_39 = "Get ye a hook an' ye don't need th'other one!", -- Lone Glove Griftlands trinket
        TRINKET_40 = "Whut land lubbin' snail be havin' scales?", -- Snail Scale Griftlands trinket
        TRINKET_41 = "Whut good comes frum a broken bit o'treasure?", -- Goop Canister Hot Lava trinket
        TRINKET_42 = "Whut kinda wooden devil be this?", -- Toy Cobra Hot Lava trinket
        TRINKET_43= "This ain't be th'proper teachin' fer fightin' reptiles!", -- Crocodile Toy Hot Lava trinket
        TRINKET_44 = "This ain't be treasure worth haulin'!", -- Broken Terrarium ONI trinket
        TRINKET_45 = "'Tis a colorful shell.", -- Odd Radio ONI trinket
        TRINKET_46 = "It be doin' whut?", -- Hairdryer ONI trinket
		-- The numbers align with the trinket numbers above.
        LOST_TOY_1  = "It be a cursed relic!",
        LOST_TOY_2  = "It be a cursed relic!",
        LOST_TOY_7  = "It be a cursed relic!",
        LOST_TOY_10 = "It be a cursed relic!",
        LOST_TOY_11 = "It be a cursed relic!",
        LOST_TOY_14 = "It be a cursed relic!",
        LOST_TOY_18 = "It be a cursed relic!",
        LOST_TOY_19 = "It be a cursed relic!",
        LOST_TOY_42 = "It be a cursed relic!",
        LOST_TOY_43 = "It be a cursed relic!",

		HALLOWEENCANDY_1 = "If I'm gonna lose th'rest o'me teeth it's gonna be on one o'these!",
        HALLOWEENCANDY_2 = "Whut kind o'treat is this?",
        HALLOWEENCANDY_3 = "Thet be corn. Me knows it anywheres!",
        HALLOWEENCANDY_4 = "Black as me teeth.",
        HALLOWEENCANDY_5 = "Be it little land-lubber flavored?",
        HALLOWEENCANDY_6 = "I ain't got nuthin' ta lose.",
        HALLOWEENCANDY_7 = "Bitty lil' grape grub.",
        HALLOWEENCANDY_8 = "How many licks does it take ta get ta th'center?",
        HALLOWEENCANDY_9 = "Gimme some sugar.",
        HALLOWEENCANDY_10 = "How many licks does it take ta get ta th'center?",
        HALLOWEENCANDY_11 = "Whose ta reject a good ol'cocoa brickette?",
		HALLOWEENCANDY_12 = "Th'perfect treat fer ol'Woodlegs!", --ONI meal lice candy
        HALLOWEENCANDY_13 = "Me no-teeth are strong 'nuff fer a crunch!", --Griftlands themed candy
        HALLOWEENCANDY_14 = "Sets a fire in yer belly!", --Hot Lava pepper candy
        CANDYBAG = "Fer a more sugary treasure.",

		HALLOWEEN_ORNAMENT_1 = "Don't go summonin' any spirits!",--Ghost
		HALLOWEEN_ORNAMENT_2 = "Ye be puttin' yer enemies in trees?",--Bat
		HALLOWEEN_ORNAMENT_3 = "Now where's me wee cutlass!",--Spider
		HALLOWEEN_ORNAMENT_4 = "Arr! I don't need ta be reminded of them ship sinkers!",--Tentacle
		HALLOWEEN_ORNAMENT_5 = "Ye'd ne'er get th'jump on ol'Woodlegs!",--Dangling Spider
		HALLOWEEN_ORNAMENT_6 = "I's dun get th'fussin' these lubbers be discussin'.",--Crow

		HALLOWEENPOTION_DRINKS_WEAK = "Arr! Get me somethin' worth chuggin' like rum!",
		HALLOWEENPOTION_DRINKS_POTENT = "Woodlegs has had 'is fair shares of harrrdy brews!",
        HALLOWEENPOTION_BRAVERY = "This'd put some hair on yer chest!",
		HALLOWEENPOTION_MOON = "I'd know th'moon's brew anywheres!",
		HALLOWEENPOTION_FIRE_FX = "'Cano inna bottle!", 
		MADSCIENCE_LAB = "If ye had any marbles left, this'd take em from ye!",
		LIVINGTREE_ROOT = "I spies wit'me eyes a bit o'root!", 
		LIVINGTREE_SAPLING = "Give ye a scare it does!",

		DRAGONHEADHAT = "Gots me th'lucky part!",
        DRAGONBODYHAT = "Git me a longer dragon, bring me s'more luck an' treasures!",
        DRAGONTAILHAT = "Thet be th'most foul smellin' part o'th'piece!",
        PERDSHRINE =
        {
            GENERIC = "Don't be askin' me fer any o'me treasure!",
            EMPTY = "Give 'er th'weakest bush.",
            BURNT = "Thet shrine be worthless now.",
        },
        REDLANTERN = "Ol'Woodlegs loves a festival 'er two!",
        LUCKY_GOLDNUGGET = "It be me luckiest treasure yet!",
        FIRECRACKERS = "It be a bit too light fer a proper popper.",
        PERDFAN = "Beat th'heat.",
        REDPOUCH = "Looks like me luck be turnin'!",
		WARGSHRINE = 
        {
            GENERIC = "Don't be askin' me fer any o'me treasure!",
            EMPTY = "Give 'er th'weakest torch.",
            BURNT = "Thet shrine be worthless now.",
        },
        CLAYWARG = 
        {
        	GENERIC = "Nuthin' an ol'cannon ball can't put a 'ole inta!",
        	STATUE = "I'll have me eyes on ye!",
        },
        CLAYHOUND = 
        {
        	GENERIC = "I'll be smashin' ye ta bits!",
        	STATUE = "Me knows treasure's around if there be guards a barkin'!",
        },
        HOUNDWHISTLE = "Ney good for whistlin' me ol'tunes.",
        CHESSPIECE_CLAYHOUND = "It be a feisty piece'o'stone!",
        CHESSPIECE_CLAYWARG = "Th'fiercest creature be on our ground now!",
		PIGSHRINE =
		{
            GENERIC = "Don't be askin' me fer any o'me treasure!",
            EMPTY = "It be needin' meat!",
            BURNT = "Nary a whiff o'bacon.",
		},
		PIG_TOKEN = "'Tis no string o'jewels, but ol'Woodlegs'll keep 'im!",
		PIG_COIN = "Be it real gold?",
		YOTP_FOOD1 = "Woodlegs'll be feastin' 'is face t'night!.",
		YOTP_FOOD2 = "Slop fit fer th'galley!",
		YOTP_FOOD3 = "A wee meal o'th'sea!",
		PIGELITE1 = "'E's ragin' like th'waves!", --BLUE
		PIGELITE2 = "Away from me gold ye foul swine!", --RED
		PIGELITE3 = "Ain't no pirate afraid o'no dirt! Gimmie yer treasure!", --WHITE
		PIGELITE4 = "'E reeks o'landlubbin'! Th'treasure be mine!", --GREEN
		PIGELITEFIGHTER1 = "'E's ragin' like th'waves!", --BLUE
		PIGELITEFIGHTER2 = "Away from me gold ye foul swine!", --RED
		PIGELITEFIGHTER3 = "Ain't no pirate afraid o'no dirt! Gimmie yer treasure!", --WHITE
		PIGELITEFIGHTER4 = "'E reeks o'landlubbin'! Th'treasure be mine!", --GREEN

		CARRAT_GHOSTRACER = "Ye be a ghostly parrot.",

        YOTC_CARRAT_RACE_START = "It be th'dock.",
        YOTC_CARRAT_RACE_CHECKPOINT = "Waypoints!",
        YOTC_CARRAT_RACE_FINISH =
        {
            GENERIC = "Be it a ship race o'th'land?",
            BURNT = "Arrr!",
            I_WON = "Yo-ho-ho! An' a bag o'gold!",
            SOMEONE_ELSE_WON = "Arrr... {winner} be a gold thief.",
        },

		YOTC_CARRAT_RACE_START_ITEM = "Get started!",
        YOTC_CARRAT_RACE_CHECKPOINT_ITEM = "Markin' tha way doon.",
		YOTC_CARRAT_RACE_FINISH_ITEM = "Th'end o'this piece.",

		YOTC_SEEDPACKET = "A packet o'seeds.",
		YOTC_SEEDPACKET_RARE = "A packet o'riches.",

		MINIBOATLANTERN = "Wee buoys.",

        YOTC_CARRATSHRINE =
        {
            GENERIC = "Don't be askin' me fer any o'me treasure!",
            EMPTY = "It be needin' rat food!",
            BURNT = "Smellin' like a scurvy soup.",
        },

        YOTC_CARRAT_GYM_DIRECTION = 
        {
            GENERIC = "Any sea dog woth 'is salts trusts th'wind's directions!",
            RAT = "Ye'd make a great parrat!",
            BURNT = "Brunt ta a crisp.",
        },
        YOTC_CARRAT_GYM_SPEED = 
        {
            GENERIC = "Puts th'wind in yer sails!",
            RAT = "Keep runnin', ye scallywag!",
            BURNT = "Tis crisp.",
        },
        YOTC_CARRAT_GYM_REACTION = 
        {
            GENERIC = "An old salt's reaction speed!",
            RAT = "Keepin' ye in shape!",
            BURNT = "A wee bit burnt.",
        },
        YOTC_CARRAT_GYM_STAMINA = 
        {
            GENERIC = "Get yer bursts o'wind!",
            RAT = "A parrat fit fer a captain!",
            BURNT = "Ye worked up too much sweat!",
        }, 

        YOTC_CARRAT_GYM_DIRECTION_ITEM = "Get workin'!",
        YOTC_CARRAT_GYM_SPEED_ITEM = "Get workin'!",
        YOTC_CARRAT_GYM_STAMINA_ITEM = "Get workin'!",
        YOTC_CARRAT_GYM_REACTION_ITEM = "Get workin'!",
        YOTC_CARRAT_SCALE_ITEM = "Get workin'!",           
        YOTC_CARRAT_SCALE = 
        {
            GENERIC = "Best be gettin' only th'best results!",
            CARRAT = "Ye ain't ready ta be captain's parrat.",
            CARRAT_GOOD = "Fit fer my shoulder!",
            BURNT = "Burnt ta ashes.",
        },

		YOTB_BEEFALOSHRINE =
        {
            GENERIC = "Don't be askin' me fer any o'me treasure!",
            EMPTY = "It be needin' beast bits!",
            BURNT = "Arrr, th'smell o'burnt hair.",
        },

        BEEFALO_GROOMER =
        {
            GENERIC = "No beast ta lubber about.",
            OCCUPIED = "Let's get ta pirate-makin', beast!",
            BURNT = "Ye can't handle th'captain takin' yer winnin's, aye!",
        },
        BEEFALO_GROOMER_ITEM = "Best be settin' up now.",

		YOTR_RABBITSHRINE =
        {
            GENERIC = "Don't be askin' me fer any o'me treasure!",
            EMPTY = "It be freeloadin' our rations!",
            BURNT = "Smellin' like a scurvy soup.",
        },

        NIGHTCAPHAT = "Th' cap o' sailors sailin' murky depths o' th'mind.",

        YOTR_FOOD1 = "Gives ye th'curse o' rabbit sight.",
        YOTR_FOOD2 = "Th'ocean blue what controls th'tides, ta control me bowels!",
        YOTR_FOOD3 = "Guide me tide-buds across yer jigglin' blue!",
        YOTR_FOOD4 = "Skewered plenty'a swab in me day, nay as tasty o'this!",

        YOTR_TOKEN = "Woodlegs may be missin' teeth but 'e's still got a thumb t'bite!",

        COZY_BUNNYMAN = "Lubber 'asn't set foot on a boat even in its dreams. Bah!",

        HANDPILLOW_BEEFALOWOOL = "It be a fine bit o' soured wool.",
        HANDPILLOW_KELP = "Whut swab dredged this up from th'sea?",
        HANDPILLOW_PETALS = "Lily-livered swabs be lily-covered swabs!",
        HANDPILLOW_STEELWOOL = "Ain't no worse than any bit o' rotted wood.",

        BODYPILLOW_BEEFALOWOOL = "It be a fine bit o' soured wool.",
        BODYPILLOW_KELP = "Ye be sleepin' in th'bilge wit'this.",
        BODYPILLOW_PETALS = "Lily-livered swabs be lily-covered swabs!",
        BODYPILLOW_STEELWOOL = "Ain't no worse than any bit o' rotted wood.",

		BISHOP_CHARGE_HIT = "Arr! Keep ye bolts to ye selves!",
		TRUNKVEST_SUMMER = "Clothes from a nose!",
		TRUNKVEST_WINTER = "Clothes from a nose!",
		TRUNK_COOKED = "Bin eatin' nastier noses than this.",
		TRUNK_SUMMER = "There be jewels and riches in this trunk?",
		TRUNK_WINTER = "There be jewels and riches in this trunk?",
		TUMBLEWEED = "I like me weeds settin' still.",
		TURF_SANDY = "Me ain't no ground lubber.",
		TURF_BADLANDS = "Me ain't no ground lubber.",
		TURF_CARPETFLOOR = "Me ain't no ground lubber.",
		TURF_CAVE = "Me ain't no ground lubber.",
		TURF_CHECKERFLOOR = "Me ain't no ground lubber.",
		TURF_DECIDUOUS = "Me ain't no ground lubber.",
		TURF_DESERTDIRT = "Me ain't no ground lubber.",
		TURF_DIRT = "Me ain't no ground lubber.",
		TURF_FOREST = "Me ain't no ground lubber.",
		TURF_FUNGUS = "Me ain't no ground lubber.",
		TURF_FUNGUS_GREEN = "Me ain't no ground lubber.",
		TURF_GRASS = "Me ain't no ground lubber.",
		TURF_MARSH = "Me ain't no ground lubber.",
		TURF_METEOR = "Me ain't no ground lubber.",
        TURF_PEBBLEBEACH = "Me ain't no ground lubber.",
		TURF_MUD = "Me ain't no ground lubber.",
		TURF_ROAD = "Me ain't no ground lubber.",
		TURF_ROCKY = "Me ain't no ground lubber.",
		TURF_SAVANNA = "Me ain't no ground lubber.",
		TURF_SINKHOLE = "Me ain't no ground lubber.",
		TURF_UNDERROCK = "Me ain't no ground lubber.",
		TURF_WOODFLOOR = "Creaks under me pegleg.",
		TURKEYDINNER = "This cackler is good eatin'!",
		TWIGS = "Too thin fer replacin' me peg leg.",
		UMBRELLA = "I do like ta keep me powder dry.",
		UNAGI = "Eat a eel? It be breakfast already?",
		UNIMPLEMENTED = "It don't be ready.",
		WAFFLES = "Just like me mum used ta open from th'package.",
		WALL_HAY =
		{
			BURNT = "Th'hay wall burnt up, go figure.",
			GENERIC = "Th'only walls I tolerate arr in me cabin.",
		},
		WALL_HAY_ITEM = "Hope there arrr'nt any wild horses about.",
		WALL_RUINS = "These walls be even older than me!",
		WALL_RUINS_ITEM = "Ancient walls ta protect precious treasure.",
		WALL_STONE = "It would sink at sea.",
		WALL_STONE_ITEM = "Stone wall fer all yer land concerns.",
		WALL_WOOD =
		{
			BURNT = "Won't be wallin' anymore.",
			GENERIC = "Keeps yer land safe if thet's yer fancy.",
		},
		WALL_WOOD_ITEM = "Walls arrr fer land and land be fer lubbers.",
		WALL_MOONROCK = "Bah! Walls!",
		WALL_MOONROCK_ITEM = "Walls make me feel cagey.",
		WALL_DREADSTONE = "Wretched walls, curses an' all!",
		WALL_DREADSTONE_ITEM = "Last thingy me be needin' be a set o'bewitched walls!",
		FENCE = "It's goin' and makin' me feel all cagey.",
        FENCE_ITEM = "Fer th'land lubbers.",
        FENCE_GATE = "Me prefers a nice port.",
        FENCE_GATE_ITEM = "Fer th'land lubbers..",
		WALRUS = "Whut's yer story, mate?",
		WALRUSHAT = "Not me style.",
		WALRUS_CAMP =
		{
			EMPTY = "Th'landlubbers ain't here.",
			GENERIC = "Campin's fer landlubbers.",
		},
		WALRUS_TUSK = "Ye'd make a fancy new peg leg.",
		WARDROBE = 
		{
			GENERIC = "Even a dirty ol'pirate could use some freshin' up.",
            BURNING = "Ye won't be standin' fer long.",
			BURNT = "'Tis blackened.",
		},
		WARG = "Yer a big dawg on th'land, but ye'd be fish food at sea.",
		WARGLET = "Fancy ye some shark bait, aye?",
		WASPHIVE = "Leavin' it alone be th'best course.",
		WATERBALLOON = "It be a sack o'th'sea.",
		WATERMELON = "Aye, th'fruit after Woodleg's watery heart.",
		WATERMELONHAT = "I love me water fruits but this be ridiculous.",
		WATERMELONICLE = "A cool treat from me favorite fruit!",
		WATERMELON_COOKED = "Better'n me thought it'd be.",
		WATERMELON_SEEDS = "If only I could plant these in th'water.",
		WEBBERSKULL = "Whut sorta neck did this sit atop?",
		WETGOOP = "Thet's why they never let ol'Woodlegs in th'galley.",
		WHIP = "Keeps yer steeds in shape.",
		WINTERHAT = "Keeps me noggin' from freezin'.",
		WINTEROMETER =
		{
			BURNT = "Thet don't help me now.",
			GENERIC = "Arrr brrrrrr!",
		},
	   
	    WINTER_TREE =
        {
			BURNT = "Thet tree be even more worthless now.",
			BURNING = "Th'tree be burnin' up!",
			CANDECORATE = "Whut be so special about this tree? Ye hidin' treasure in it?",
			YOUNG = "Get ta growin'!",
        },
        WINTER_TREESTAND = "It be needin' a wee' pinecone!",
        WINTER_ORNAMENT = "Ain't fit ta be a cannonball.",
        WINTER_ORNAMENTLIGHT = "Ain't much o' cannonball here.",
		WINTER_ORNAMENTBOSS = "It ain't me preferred treasure, but it's treasure.",
		WINTER_ORNAMENTFORGE = "Whut be so special 'bout this'un?",
		WINTER_ORNAMENTGORGE = "Whut be so special 'bout this'un?",
        
        WINTER_FOOD1 = "Th'legs be breakin' off, must be shaped in me image!", --gingerbread cookie
        WINTER_FOOD2 = "Bah, whut kind o'treat be this?", --sugar cookie
        WINTER_FOOD3 = "This be an edible hook!", --candy cane
        WINTER_FOOD4 = "Be this whut passes fer grub o'th'land?", --fruitcake
		WINTER_FOOD5 = "A taste o'Woodlegs' legs!", --yule log cake
        WINTER_FOOD6 = "Feast fer me face!", --plum pudding
        WINTER_FOOD7 = "Is this whut passes fer rum 'round here?", --apple cider
        WINTER_FOOD8 = "Foams up me beard!", --hot cocoa
        WINTER_FOOD9 = "Git this 'nog a splash o'rum!", --eggnog

		WINTERSFEASTOVEN =
		{
			GENERIC = "Ye won't see Woodlegs' shovellin' it full'a coal.",
			COOKING = "Bah! Get cookin'!",
			ALMOST_DONE_COOKING = "I'd know thet galley smell anywheres!",
			DISH_READY = "Yarrr! Th'feast b'mine!",
		},
		BERRYSAUCE = "These sweets be a captain's pleasure!",
		BIBINGKA = "A loaf o'boat!",
		CABBAGEROLLS = "Ye can't hide yer meaty treasure from Woodlegs!",
		FESTIVEFISH = "Smells of galley makin's!",
		GRAVY = "Nay a boat fit fer th' sea. Fit fer m'gullet!",
		LATKES = "Woodlegs' maw is all it saw!",
		LUTEFISK = "Pass o'er th'grub, lubbers!",
		MULLEDDRINK = "A spot o'rum will do ye good!",
		PANETTONE = "I ain't no fancy-eater, but I ain't complainin'!",
		PAVLOVA = "Would b'crushed under any ol'sailor's hands!",
		PICKLEDHERRING = "We got th'same bit'o'brine!",
		POLISHCOOKIE = "Ye'll have ta hand over all yer sugary goods!",
		PUMPKINPIE = "A pie o'th'sea! Er so it be seemin'!",
		ROASTTURKEY = "Mutton be meetin' m'gullet!",
		STUFFING = "Stuff it down yer throat.",
		SWEETPOTATO = "Me likesd me potateys sweet!",
		TAMALES = "Me likes ta eat 'em.",
		TOURTIERE = "I's spy a pie wit'me eye! An' its got treasure!",
		TABLE_WINTERS_FEAST = 
		{
			GENERIC = "Whut b'special 'bout this land-lubbin' desk?",
			HAS_FOOD = "Avast yer gullets!",
			WRONG_TYPE = "Ye got th'wrong grub!",
			BURNT = "All pirate festivities be endin' this way, lads!",
		},
		GINGERBREADWARG = "Meet m'gullet!", 
		GINGERBREADHOUSE = "Whar's th'ginger bread boat!", 
		GINGERBREADPIG = "Show me yer crumbly treasuress!",
		CRUMBS = "Woodlegs'd know yer tracks anywheres.",
		WINTERSFEASTFUEL = "Whut treasure b'this!? It b'mine!",

        KLAUS = "Ye'd ne'er see such 'n ugly face out at sea!",
        KLAUS_SACK = "It be a whole haul o'treasure!",
		KLAUSSACKKEY = "Yarr! Th'treasure it must be unlockin' must be 'uge!",
		WORM =
		{
			DIRT = "Dirty dirt.",
			PLANT = "Ripe fer th'pickin'.",
			WORM = "'Tis a worm!",
		},
		WORMHOLE =
		{
			GENERIC = "Whut would possess me ta step into thet?",
			OPEN = "Whar'n Hades do thet lead?",
		},
		WORMHOLE_LIMITED = "Thet thar pit looks like it's on its last peglegs.",
		WORMLIGHT = "Set's a fire in yer belly.",
		WORMLIGHT_LESSER = "It be turnin' ta a raisin!",
		WORMLIGHT_PLANT = "Me be sniffin' a hearty trap!",
		YELLOWAMULET = "I be a walkin' lighthouse!",
		YELLOWGEM = "A gem th'color o'me teeth!",
		YELLOWSTAFF = "Th'stars aren't under th'domain o'man.",

		FRESHFRUITCREPES = "Aye, thet be food.",
		FRUITMEDLEY = "'Tis a scrumptious little morsel.",
		FURTUFT = "It be a little furry.", 
		GEARS = "Me gears be turnin'.",
		GUANO = "'Tis from th'backside o'a beast!",
		HOUNDFIRE = "It burns viciously!",
		MONSTERTARTARE = "'Tis a vile thing.",
		TURF_FUNGUS_RED = "Me ain't no ground lubber.",
		TURF_DRAGONFLY = "Me ain't no ground lubber.",
		TURF_SHELLBEACH = "Now thet be ground me pegs be familiar wit'!",

		TURF_RUINSBRICK = "Me ain't no ground lubber.",
		TURF_RUINSBRICK_GLOW = "Me ain't no ground lubber.",
		TURF_RUINSTILES = "YMe ain't no ground lubber.",
		TURF_RUINSTILES_GLOW = "Me ain't no ground lubber.",
		TURF_RUINSTRIM = "Me ain't no ground lubber.",
		TURF_RUINSTRIM_GLOW = "Me ain't no ground lubber.",

		TURF_MONKEY_GROUND = "Now thet be a ground me pegs be familiar wit'!",

        TURF_CARPETFLOOR2 = "Me ain't no ground lubber.",
        TURF_MOSAIC_GREY = "Me ain't no ground lubber.",
        TURF_MOSAIC_RED = "Me ain't no ground lubber..",
        TURF_MOSAIC_BLUE = "Me ain't no ground lubber.",

		TURF_BEARD_RUG = "Me ain't no ground lubber.",

		REVIVER = "Th'heart o'healin'.",
		SHADOWHEART = "Heart o'th'sea.",
		ATRIUM_RUBBLE = 
        {
			LINE_1 = "This kind'a map ain't be useful ta me.",
			LINE_2 = "Bah! Th'message be faded.",
			LINE_3 = "Th'sea's swallowin' 'em up 'ole.",
			LINE_4 = "Th'land lubbers be burstin' ta pieces 'ere.",
			LINE_5 = "Arr! Lubbers! Ye don't be needin' thet treasure!",
		},
        ATRIUM_STATUE = "This be like a mirage at sea!",
        ATRIUM_LIGHT = 
        {
			ON = "Submerged in th'light o'th'dark sea.",
			OFF = "Ain't be up'n'runnin'.",
		},
        ATRIUM_GATE =
        {
			ON = "It be glowin' a devilish light.",
			OFF = "Why would ye land lubbers make a livin' below th'sea?",
			CHARGING = "It be startin' somethin' devilish!",
			DESTABILIZING = "Get oot'a th'way!!",
			COOLDOWN = "It has set into a halt!",
        },
        ATRIUM_KEY = "Treasure from th'boney devil.",
		LIFEINJECTOR = "Ta poke ye wit'health!",
		SKELETON_PLAYER =
		{
			MALE = "%s was sent ta Davy Jones' Locker by %s.",
			FEMALE = "%s was sent ta Davy Jones' Locker by %s.",
			ROBOT = "%s was sent ta Davy Jones' Locker by %s.",
			DEFAULT = "%s was sent ta Davy Jones' Locker by %s.",
		},
		HUMANMEAT = "'Tis a vile thing.",
		HUMANMEAT_COOKED = "Thet be bad eatin'.",
		HUMANMEAT_DRIED = "Th'marooned sailor's last resort.",
		MOONROCKNUGGET = "I prefer me rocks whar I can see'em!",
		ROCK_MOON = "Thet be a strange boulder if I's e'er seen'un.",
		MOONROCKCRATER = "I ain't givin' it me treasure!",

        REDMOONEYE = "It be ready fer watchin'.",
        PURPLEMOONEYE = "X marks th'spot! Arrr... rock.",
        GREENMOONEYE = "Keep a good eye out over yonder.",
        ORANGEMOONEYE = "Lets ye know where ye buried yer treasure!",
        YELLOWMOONEYE = "Ye got them mermaid eyes!",
        BLUEMOONEYE = "Marks yer treasures.",
        --Arena Event
        LAVAARENA_BOARLORD = "Ye'd ne'er last out in th'open seas!",
        BOARRIOR = "If I had me boat I'd take ye myself!",
        BOARON = "Ye be lookin' mighty tasty!",
        PEGHOOK = "A mighty poison what flows through yer veins!",
        TRAILS = "Get back ye brutish devil!",
        TURTILLUS = "I've taken doon mightier turtles than ye wee lubbers!",
        SNAPPER = "I reckon I can spit further than ye!",
		RHINODRILL = "Ye scallywags wouldn't fit in wit' even th'lowest o'swabs!",
		BEETLETAUR = "Git back in yer locks!", --Arr! Blow th'beast down!
        LAVAARENA_PORTAL = 
        {
            ON = "Woodlegs'll be taken 'is leave now.",
            GENERIC = "Be me eyes deceivin' me? Be this th'red sea?",
        },
		LAVAARENA_KEYHOLE = "It be fancyin' a key 'er two.",
		LAVAARENA_KEYHOLE_FULL = "Thar she goes!",
		LAVAARENA_BATTLESTANDARD = "Arr! Take their flag down!",
        LAVAARENA_SPAWNER = "'Tis a most bewitched circle.",
		HEALINGSTAFF = "I's ain't against some helpful magic.",
        FIREBALLSTAFF = "Devil magic thet summons demons from th'sky!",
        HAMMER_MJOLNIR = "These lubbers take pride in smashin'n'crashin'.",
        SPEAR_GUNGNIR = "They'll never see ol'Woodlegs comin'!",
        BLOWDART_LAVA = "Quit ye blubberin' and keep shootin'.",
        BLOWDART_LAVA2 = "Th'swabs be breathin' a mighty fire now!",
        LAVAARENA_LUCY = "I'll chuck ye a good'un!",
        WEBBER_SPIDER_MINION = "Keep ye crawlies on th'land.",
        BOOK_FOSSIL = "Magic like this'd 'ave ye thrown o'reboard!",
		LAVAARENA_BERNIE = "Keep yer dancin' an' I won't send ye out ta sea.",
		SPEAR_LANCE = "'Tis a gut twister.",
		BOOK_ELEMENTAL = "This be some sort'o summoner.",
		LAVAARENA_ELEMENTAL = "Keep swingin' yer cursed stone hands at th'beasts!",
		LAVAARENA_ARMORLIGHT = "This'd be torn apart at sea!",
		LAVAARENA_ARMORLIGHTSPEED = "Bah! Whut lubber's runnin' wit'grass fer gear?",
		LAVAARENA_ARMORMEDIUM = "I won't be hearin' yer knockin'!",
		LAVAARENA_ARMORMEDIUMDAMAGER = "'E's got a bite to 'em.",
		LAVAARENA_ARMORMEDIUMRECHARGER = "This'd keep ye afloat.",
		LAVAARENA_ARMORHEAVY = "Thet's a wee bit 'eavy. Nay a cap'n's armor.",
		LAVAARENA_ARMOREXTRAHEAVY = "Me'd like ta see 'em try ta hit Woodlegs now!",
		LAVAARENA_FEATHERCROWNHAT = "Woodlegs'll run as fast as 'is pegs can carry 'im!",
		LAVAARENA_HEALINGFLOWERHAT = "Ye'd never catch ol'Woodlegs wearin' one o'these!",
		LAVAARENA_EYECIRCLETHAT = "Whut arrre ye lookin' at?",
		LAVAARENA_HEALINGGARLANDHAT = "This'd patch up yer belly-achin'.",
		LAVAARENA_RECHARGERHAT = "I'll keep these treasures on me head.",
        LAVAARENA_LIGHTDAMAGERHAT = "Yarr! Now yer in fer a punchin'!",
		LAVAARENA_STRONGDAMAGERHAT = "Yarrharr! An'I'll be takin' on yer whole crew meself next!",
		LAVAARENA_CROWNDAMAGERHAT = "Ye might want ta start walkin' th'plank yerselves, lubbers! Yarr-har-har!",
		LAVAARENA_TIARAFLOWERPETALSHAT = "Vines fer yer head!",
		LAVAARENA_ARMOR_HP = "Keeps ye in th'fight, nary th'sea!",
		LAVAARENA_FIREBOMB = "This'd keep yer trap shut!",
		LAVAARENA_HEAVYBLADE = "Gives ye a whoopin'!",
        --Quagmire
        QUAGMIRE_ALTAR = 
        {
        	GENERIC = "Get to th'galley!",
        	FULL = "Get'cher grub!",
    	},
		QUAGMIRE_ALTAR_STATUE1 = "It be an ol'bit o'stone.",
		QUAGMIRE_PARK_FOUNTAIN = "Nary a sea left in it.",
		
        QUAGMIRE_HOE = "Land lubbin' tools!",
        
        QUAGMIRE_TURNIP = "Fer keepin' off yer scurvy.",
        QUAGMIRE_TURNIP_COOKED = "Got thet perfect crunchy sting!",
        QUAGMIRE_TURNIP_SEEDS = "Seeds fer lubbers ta grow 'n sow.",
        
        QUAGMIRE_GARLIC = "Back in me teeth-ed days Woodlegs ate 'em as is!",
        QUAGMIRE_GARLIC_COOKED = "A treat fer Ol'Woodlegs!",
        QUAGMIRE_GARLIC_SEEDS = "Th'stench b'th'best part!",
        
        QUAGMIRE_ONION = "I like bitin' 'em raw.",
        QUAGMIRE_ONION_COOKED = "Tis a treasure fer me belly.",
        QUAGMIRE_ONION_SEEDS = "Fit fer th'ground. An' then a belly!",
        
        QUAGMIRE_POTATO = "Potateys be a galley staple!",
        QUAGMIRE_POTATO_COOKED = "Hot grub!",
        QUAGMIRE_POTATO_SEEDS = "Seeds be fer planting, an Woodlegs ain't no farmer.",
        
        QUAGMIRE_TOMATO = "Gives ye a juicy bite.",
        QUAGMIRE_TOMATO_COOKED = "Fer a vegetable brew.",
        QUAGMIRE_TOMATO_SEEDS = "It belongs to th'ground.",
        
        QUAGMIRE_FLOUR = "Gits lost in me beard!",
        QUAGMIRE_WHEAT = "Ye can't beat th'wheat!",
        QUAGMIRE_WHEAT_SEEDS = "Fer lubber ta sow.",
        --NOTE: raw/cooked carrot uses regular carrot strings
        QUAGMIRE_CARROT_SEEDS = "Git me a lubber to lub it!",
        
        QUAGMIRE_ROTTEN_CROP = "It be nasty!",
        
		QUAGMIRE_SALMON = "Aye, th'catch o'th'day!",
		QUAGMIRE_SALMON_COOKED = "Down th'hatch!",
		QUAGMIRE_CRABMEAT = "Bit o'crab.",
		QUAGMIRE_CRABMEAT_COOKED = "Ready fer m'gullet.",
		QUAGMIRE_SUGARWOODTREE = 
		{
			GENERIC = "A tree wit' a pinch o'sweet!",
			STUMP = "Thet be a stump fer stumpin'.",
			TAPPED_EMPTY = "Bah! Fill it up!",
			TAPPED_READY = "She's ready!",
			TAPPED_BUGS = "Blast! Bugs!",
			WOUNDED = "Yer not lookin' so 'ot.",
		},
		QUAGMIRE_SPOTSPICE_SHRUB = 
		{
			GENERIC = "A bit o'land shrub.",
			PICKED = "Woodlegs got 'em!",
		},
		QUAGMIRE_SPOTSPICE_SPRIG = "Grind 'em up!",
		QUAGMIRE_SPOTSPICE_GROUND = "Woodlegs loves a bit o'spice!",
		QUAGMIRE_SAPBUCKET = "Fill 'em up!",
		QUAGMIRE_SAP = "Sticks to yer pegs.",
		QUAGMIRE_SALT_RACK =
		{
			READY = "Thar she is!",
			GENERIC = "If Woodlegs licks 'em ye'll get yer salt!",
		},
		QUAGMIRE_POND_SALT = "Salty as th'sea!",
		QUAGMIRE_SALT_RACK_ITEM = "PLACEHOLDER",

		QUAGMIRE_SAFE = 
		{
			GENERIC = "A safe o'treasure!",
			LOCKED = "Arrr! Gimme yer treasure!",
		},

		QUAGMIRE_KEY = "Show me to tha treasures!",
		QUAGMIRE_KEY_PARK = "Whut treasure do ye open?",
        QUAGMIRE_PORTAL_KEY = "Fer a fancier treasure!",

		QUAGMIRE_MUSHROOMSTUMP =
		{
			GENERIC = "'Shrooms!",
			PICKED = "It ain't growin' no more.",
		},
		QUAGMIRE_MUSHROOMS = "Good fer th'galley's grub.",
        QUAGMIRE_MEALINGSTONE = "Watch Ol'Woodlegs show ye how it's done!",
		QUAGMIRE_PEBBLECRAB = "Thar's a tasty meal!",

		QUAGMIRE_RUBBLE_CARRIAGE = "'Tis good fer nothin'.",
        QUAGMIRE_RUBBLE_CLOCK = "Bah! Rubble!",
        QUAGMIRE_RUBBLE_CATHEDRAL = "Bah! Rubble!",
        QUAGMIRE_RUBBLE_PUBDOOR = "Woodlegs misses a pub!",
        QUAGMIRE_RUBBLE_ROOF = "It be caved in.",
        QUAGMIRE_RUBBLE_CLOCKTOWER = "Bah! Rubble!",
        QUAGMIRE_RUBBLE_BIKE = "Yer luck ran oot, matey.",
        QUAGMIRE_RUBBLE_HOUSE =
        {
            "Rubble'n'stone.",
            "Rubble'n'stone.",
            "Rubble'n'stone.",
        },
        QUAGMIRE_RUBBLE_CHIMNEY = "Bah! Rubble!",
        QUAGMIRE_RUBBLE_CHIMNEY2 = "Bah! Rubble!",
        QUAGMIRE_MERMHOUSE = "Ye should'a lived on th'seas earlier!",
        QUAGMIRE_SWAMPIG_HOUSE = "It be run through!",
        QUAGMIRE_SWAMPIG_HOUSE_RUBBLE = "Ain't kept up.",
        QUAGMIRE_SWAMPIGELDER =
        {
            GENERIC = "Avast ye! Show me to yer treasures!",
            SLEEPING = "Dreamin' o'treasure, are ye?",
        },
        QUAGMIRE_SWAMPIG = "Lubber.",
        
        QUAGMIRE_PORTAL = "Thet ain't no sea.",
        QUAGMIRE_SALTROCK = "Like Ol'Woodlegs!",
        QUAGMIRE_SALT = "Woodlegs knows 'is salt!",
        --food--
        QUAGMIRE_FOOD_BURNT = "'Tis a shame.",
        QUAGMIRE_FOOD =
        {
        	GENERIC = "Grub's on!",
            MISMATCH = "Thet ain't matchin'!",
            MATCH = "'Tis a match!",
            MATCH_BUT_SNACK = "A wee bit light.",
        },
        
        QUAGMIRE_FERN = "Land seaweed.",
        QUAGMIRE_FOLIAGE_COOKED = "I eaten wrose.",
        QUAGMIRE_COIN1 = "Golden coin!",
        QUAGMIRE_COIN2 = "A fine bit o'silver!",
        QUAGMIRE_COIN3 = "Pieces o' Eight!",
        QUAGMIRE_COIN4 = "Yer prized possession b'mine now!",
        QUAGMIRE_GOATMILK = "Woodlegs loves ta drink it up.",
        QUAGMIRE_SYRUP = "Sticks to yer beard!",
        QUAGMIRE_SAP_SPOILED = "It ain't sticky, it be stinky!",
        QUAGMIRE_SEEDPACKET = "Open 'er up!",
        
        QUAGMIRE_POT = "Ye best be puttin' it ta use!",
        QUAGMIRE_POT_SMALL = "It be a wee bit'o'kitchen!",
        QUAGMIRE_POT_SYRUP = "Ta fill!",
        QUAGMIRE_POT_HANGER = "It be a mast fer cookin'.",
        QUAGMIRE_POT_HANGER_ITEM = "Masts!",
        QUAGMIRE_GRILL = "Woodlegs knows how ta grill a good salmon!",
        QUAGMIRE_GRILL_ITEM = "Get goin'!",
        QUAGMIRE_GRILL_SMALL = "A wee bit o'grill!",
        QUAGMIRE_GRILL_SMALL_ITEM = "Get goin'!",
        QUAGMIRE_OVEN = "Roast 'em up, lubbers!",
        QUAGMIRE_OVEN_ITEM = "Get cookin'!",
        QUAGMIRE_CASSEROLEDISH = "A treasure chest for tastin'!",
        QUAGMIRE_CASSEROLEDISH_SMALL = "A treasure chest for tastin'!",
        QUAGMIRE_PLATE_SILVER = "A fine plate o'silver!",
        QUAGMIRE_BOWL_SILVER = "Woodlegs dines rich!",
        
        QUAGMIRE_MERM_CART1 = "What treasures are ye keepin' in thar?", --sammy's wagon
        QUAGMIRE_MERM_CART2 = "What treasures are ye keepin' in thar?", --pipton's cart
        QUAGMIRE_PARK_ANGEL = "Would be good fer a front o'a ship.",
        QUAGMIRE_PARK_ANGEL2 = "Would be good fer a front o'a ship.",
        QUAGMIRE_PARK_URN = "Lay it to sea!",
        QUAGMIRE_PARK_OBELISK = "Lubber keepin's.",
        QUAGMIRE_PARK_GATE =
        {
            GENERIC = "Let 'em  stay open!",
            LOCKED = "Ye can't lock oot Ol'Woodlegs!",
        },
        QUAGMIRE_PARKSPIKE = "A bit o'spike.",
        QUAGMIRE_CRABTRAP = "Fer keepin' crabs.",
        QUAGMIRE_TRADER_MERM = "Ahoy! Give Woodlegs yer treasure!",
        QUAGMIRE_TRADER_MERM2 = "Ahoy! Give Woodlegs yer treasure!",
        
        QUAGMIRE_GOATMUM = "Th'sea ain't so bad when ye gets ta know it!",
        QUAGMIRE_GOATKID = "Ye'll make a fine pirate!",
        QUAGMIRE_PIGEON =
        {
            DEAD = "Tells no tales.",
            GENERIC = "It be a gull o'th'land!",
            SLEEPING = "Catchin' shuteye.",
        },
        QUAGMIRE_LAMP_POST = "A landlamp.",
        QUAGMIRE_LAMP_SHORT = "A landlamp.",

        QUAGMIRE_BEEFALO = "Nay as old'n'seasoned as Ol'Woodlegs!",
        QUAGMIRE_SLAUGHTERTOOL = "Have 'em tell nary a tale!",

        QUAGMIRE_SAPLING = "Bah. Lubbers.",
        QUAGMIRE_BERRYBUSH = "Has no berries ta speak of.",

        QUAGMIRE_ALTAR_STATUE2 = "I'll take yer eyes!",
        QUAGMIRE_ALTAR_QUEEN = "Ahoy, queen!",
        QUAGMIRE_ALTAR_BOLLARD = "Bullocks.",
        QUAGMIRE_ALTAR_IVY = "Tangles in yer riggin'!",
		--v2 Winona
        WINONA_CATAPULT = 
        {
        	GENERIC = "Avast ye! We be swingin' cannons!",
        	OFF = "Ain't doin' nuthin'!",
        	BURNING = "'Tis aflame!",
        	BURNT = "It won't be doin' whatever it done before.",
        },
        WINONA_SPOTLIGHT = 
        {
        	GENERIC = "It be a wee lighthouse!",
        	OFF = "Ain't doin' nuthin'!",
        	BURNING = "'Tis aflame!",
        	BURNT = "It won't be doin' whatever it done before.",
        },
        WINONA_BATTERY_LOW = 
        {
        	GENERIC = "This doohickey be doin' whut now?",
        	LOWPOWER = "'Tis lookin' a wee bit low.",
        	OFF = "It ain't b'workin'!",
        	BURNING = "Arr, who be burnin' yer science things!",
        	BURNT = "It won't be doin' whatever it done before.",
        },
        WINONA_BATTERY_HIGH = 
        {
        	GENERIC = "Thet be gemmy witchcraft!",
        	LOWPOWER = "'Tis lookin' a wee bit low.",
        	OFF = "Don't go askin' Woodlegs ta fix it.",
        	BURNING = "Arr, who be burnin' yer things!",
        	BURNT = "Th'dark arts didn't save ye.",
        },
        COMPOSTWRAP = "Whut swabs be rubbin' thar poop about themselves!",
        ARMOR_BRAMBLE = "Me'd like ta see 'em try ta hit Woodlegs now!",
        TRAP_BRAMBLE = "Ye'll be meetin' a pokey fate.",
		WEREITEM_BEAVER = "Arr, yer no beast o'th'sea!",
        WEREITEM_GOOSE = "Arr, a cursed idol!",
        WEREITEM_MOOSE = "A bit o'rum would put just as much hair on yer chest!",
		-- Walter
        WOBYBIG = 
        {
            "Thar she blows, ye salty dog!",
            "Thar she blows, ye salty dog!",
        },
        WOBYSMALL = 
        {
            "'Tis a fine pup!",
            "'Tis a fine pup!",
        },
		WALTERHAT = "Ain't no proper piratin' hat!",
		SLINGSHOT = "I prefers me cannon.",
		SLINGSHOTAMMO_ROCK = "Arr! Don't be peltin' me ship!",		
		SLINGSHOTAMMO_MARBLE = "Arr! Don't be peltin' me ship!",		
		SLINGSHOTAMMO_THULECITE = "Arr! Don't be peltin' me ship!",	
        SLINGSHOTAMMO_GOLD = "Arr! Don't be peltin' me ship!",
        SLINGSHOTAMMO_SLOW = "Arr! Don't be peltin' me ship!",
        SLINGSHOTAMMO_FREEZE = "Arr! Don't be peltin' me ship!",
		SLINGSHOTAMMO_POOP = "Arr! Don't be peltin' me ship!",
        PORTABLETENT = "Git yer shut eye where ye wants it.",
        PORTABLETENT_ITEM = "Me ship's me portable tent!",

		-- Wigfrid
        BATTLESONG_DURABILITY = "Ol'Woodlegs knows all shanties e'er sung at sea! Even gots some o'me own!",
        BATTLESONG_HEALTHGAIN = "Ol'Woodlegs knows all shanties e'er sung at sea! Even gots some o'me own!",
        BATTLESONG_SANITYGAIN = "Ol'Woodlegs knows all shanties e'er sung at sea! Even gots some o'me own!",
        BATTLESONG_SANITYAURA = "Ol'Woodlegs knows all shanties e'er sung at sea! Even gots some o'me own!",
        BATTLESONG_FIRERESISTANCE = "Ol'Woodlegs knows all shanties e'er sung at sea! Even gots some o'me own!",
        BATTLESONG_INSTANT_TAUNT = "Ain't much of o'pirate's tale.",
        BATTLESONG_INSTANT_PANIC = "Ain't much of o'pirate's tale.",

        -- Webber
        MUTATOR_WARRIOR = "Keep yer biscuits ta yerselves!",
        MUTATOR_DROPPER = "Feed fer them landlubbers!",
        MUTATOR_HIDER = "Keep yer biscuits ta yerselves!",
        MUTATOR_SPITTER = "Feed fer them landlubbers!",
        MUTATOR_MOON = "Keep yer biscuits ta yerselves!",
        MUTATOR_HEALER = "Feed fer them landlubbers!",
        SPIDER_WHISTLE = "Why'd ye ever need a crew o'landlubbers anyhow?",
        SPIDERDEN_BEDAZZLER = "Where be th'port? Starboard? Just a wee mast? Bah!",
        SPIDER_HEALER = "Stop them lubbers from keepin' them legs!",
		SPIDER_REPELLENT = "Gets them lubbers 'way from me legs!",
        SPIDER_HEALER_ITEM = "Bit foul, thet.",

		-- Wendy
		GHOSTLYELIXIR_SLOWREGEN = "A sickly brew.",
		GHOSTLYELIXIR_FASTREGEN = "A sickly brew.",
		GHOSTLYELIXIR_SHIELD = "A sickly brew.",
		GHOSTLYELIXIR_ATTACK = "A sickly brew.",
		GHOSTLYELIXIR_SPEED = "A sickly brew.",
		GHOSTLYELIXIR_RETALIATION = "A sickly brew.",
		SISTURN =
		{
			GENERIC = "Needs a flower 'er two.",
			SOME_FLOWERS = "Sorry lass, all I's got is kelp!",
			LOTS_OF_FLOWERS = "A proper burial is 'un out at sea!",
		},
		--v2 Warly
        PORTABLECOOKPOT_ITEM =
        {
            GENERIC = "Th'pot be fer cookin' th'fanciest grub!",
            DONE = "'Tis ready for eatin'!",
			COOKING_LONG = "Fancy eats take more time ta cook than th'usual slop!",
			COOKING_SHORT = "'Tis comin' up!",
			EMPTY = "Woodlegs ain't bothered by whut 'is grub's cooked in!",
        },
        PORTABLEBLENDER_ITEM = "Whips th'food inta shape!",
        PORTABLESPICER_ITEM =
        {
            GENERIC = "Woodlegs likes 'is spices!",
            DONE = "Ready ta shove me nose inta!",
        },
		SPICE_GARLIC = "Gives tha galley flavor!",
        SPICE_SUGAR = "A treasure worth plungin' fer.",
        SPICE_CHILI = "Thet be one feisty spice!",
		SPICE_SALT = "Bits o'tasty sea rocks.",
		ASPARAGUSSOUP = "Watery grass.",
        VEGSTINGER = "Has th'kick to it",
        BANANAPOP = "I heard ther be gold in th'banana stand.",
        CEVICHE = "Whut tasty creatures dredged up frum th'sea!",
		SALSA = "It be a spicy red sea!",
        PEPPERPOPPER = "Puts up a mighty fight in yer mouth!",
		FROGFISHBOWL = "Like a meal at sea!",
        POTATOTORNADO = "A twister o'taters!",
        DRAGONCHILISALAD = "Grass wit' a bit o'spice!",
        GLOWBERRYMOUSSE = "It be as light as th'sea.",
        VOLTGOATJELLY = "Shockin'ly tasty.",
        NIGHTMAREPIE = "A pie o'th'deep sea!",
        BONESOUP = "A hearty brew, t'be sure.",
        MASHEDPOTATOES = "A treacherous sea'o'taters",
        POTATOSOUFFLE = "A tasty piece o'crumpet.",
        MOQUECA = "'Tis a hearty fish soup!",
        GAZPACHO = "Ah, a fine brew.",
		--
		TURNIP = "Fer keepin' off yer scurvy.",
        TURNIP_COOKED = "Gots a perfect crunchy sting t'it!",
        TURNIP_SEEDS = "Seeds fer lubbers ta grow.",
        GARLIC = "Back in me teeth-ed days Woodlegs ate 'em as is!",
        GARLIC_COOKED = "A treat fer ol'Woodlegs!",
        GARLIC_SEEDS = "Th'grub b'th'best part!",
        ONION = "I like bitin' 'em raw.",
        ONION_COOKED = "'Tis a treasure fer me belly.",
        ONION_SEEDS = "Fit fer th'ground. An' then me belly!",
        POTATO = "Potateys be a galley staple!",
        POTATO_COOKED = "Hot grub!",
        POTATO_SEEDS = "Seeds be fer planting, an Woodlegs ain't no farmer.",
        TOMATO = "Gives ye a juicy bite.",
        TOMATO_COOKED = "Fer a vegetable brew.",
        TOMATO_SEEDS = "It belongs to th'ground.",
		ASPARAGUS = "Lil' vegetable masts.", 
        ASPARAGUS_COOKED = "It fills Woodlegs' belly.",
        ASPARAGUS_SEEDS = "Food fer th'dirt.",
		PEPPER = "Sets a fire in yer belly!",
        PEPPER_COOKED = "Roars a fire fer yer mouth!",
        PEPPER_SEEDS = "Fer puttin' in th'ground.",

		MERMHAT = "'Tis fer foolin' th'fish.",
		MERMTHRONE =
        {
            GENERIC = "Ye aren't hidin' any treasure 'ere, are ya lass?",
            BURNT = "Bah! Th'treasures b'burnt ta a brickette!",
        },        
        MERMTHRONE_CONSTRUCTION =
        {
            GENERIC = "Ye best hope yer buildin' a boat!",
            BURNT = "Thet happens ta wood whut don't float.",
        },  
        MERMHOUSE_CRAFTED = 
        {
            GENERIC = "They smell worse than Woodlegs!",
            BURNT = "Th'smell o'fire set ta sea.",
        },
        MERMWATCHTOWER_REGULAR = "Arrr. Don't ye get too comfy, scallywags!",
        MERMWATCHTOWER_NOKING = "Nows me chance ta take th'riches!",
        MERMKING = "Ye best be givin' up yer treasures ta ol'Woodlegs!",
        MERMGUARD = "I's ain't trustin' so easy!",
        MERM_PRINCE = "Why can't ol'Woodlegs b'th'one ta take th'gold!",
		--RoT
        BOATFRAGMENT03 = "Th'remains o'a ship.",
        BOATFRAGMENT04 = "Th'remains o'a ship.",
        BOATFRAGMENT05 = "Th'remains o'a ship.",
		BOAT_LEAK = "She be goin' doooon!",
        MAST = "Fer catchin' th'blow.",
		SEASTACK = "Avast ye! Boulders on th'way side!",
        FISHINGNET = "Fer trawl'n around.",
        ANTCHOVIES = "Th'perfect grub fer th'galley!",
        STEERINGWHEEL = "Let ol'captain Woodlegs show ye how it's done!",
        ANCHOR = "Arr! Can't get heavier than me anchor!",
        BOATPATCH = "Never set sail wit'out'er.",
        BOAT_ITEM = "Build me a boat!",
		BOAT_GRASS_ITEM = "Thet be no proper sailin' vessel!",
		STEERINGWHEEL_ITEM = "Build me a captain's steerin' wheel!",
		ANCHOR_ITEM = "Get me crew ta put up th'anchor!",
        MAST_ITEM = "Ain't a proper ship without 'er!",
		WALKINGPLANK = "I'll make ye walk it too!",
		WALKINGPLANK_GRASS = "I'll make ye walk it too!",
        OAR = "Bah! For swabs only!",
		OAR_DRIFTWOOD = "I ain't rowed since I was a young swab.",
		MINIFLARE = "It be a cannon fer a sinkin' ship's flare!",
		MEGAFLARE = "Let 'em come! Woodlegs'll show 'em a plunderin' they won't soon forget!",
		BATHBOMB = "Bah! I'll make me own bubbles!",
		TRAP_STARFISH =
        {
            GENERIC = "I ain't fallin' fer thet.",
            CLOSED = "Ye ain't bitin' ol'Woodlegs taday!",
        },
        DUG_TRAP_STARFISH = "Ye can't teach an ol'sea dog new tricks.",
        SPIDER_MOON = 
        {
        	GENERIC = "Man th'cannons!",
        	SLEEPING = "Arg! Cleav'im while ya got th' chance!",
        	DEAD = "Shark bait now.",
        },
        MOONSPIDERDEN = "Thet ain't no normal land-lubbin' den.",
		ICEBERG =
        {
            GENERIC = "Arrg! Ney good fer sailin'!",
            MELTED = "Best like thet than tearin' at me hull.",
        },
		ICEBERG_MELTED = "Need a cold snap so's it can freeze again.",
		MOON_FISSURE = 
		{
			GENERIC = "Me noggin' be pulsin' like an ocean's breeze.", 
			NOLIGHT = "Like th'deepest o'sea trenches.",
		},
        MOON_ALTAR =
        {
            MOON_ALTAR_WIP = "Bah! Whut lubber be wantin' buldin'?",
            GENERIC = "Arrre ye talkin' ta me, matey?",
        },
        MOON_TREE = 
        {
			BURNING = "She burns bright as th'moon.",
			BURNT = "Be smellin' like a bag o'coal.",
			CHOPPED = "Nary a whiff o'tree left.",
            GENERIC = "Strange wood o'th'moon be within.",
        },
		MOON_TREE_BLOSSOM = "Far frum th'tree whut dredged it up!",
        MOON_ALTAR_IDOL = "Yer whispers be tearin' at me mind's hull.",
        MOON_ALTAR_GLASS = "Whut manner o'devil magic b'this?",
        MOON_ALTAR_SEED = "I be hearin' whispers.",
        MOON_ALTAR_ROCK_IDOL = "Thar be somethin' in thar!",
        MOON_ALTAR_ROCK_GLASS = "Thar be somethin' in thar!",
        MOON_ALTAR_ROCK_SEED = "Thar be somethin' in thar!",
		MOON_ALTAR_CROWN = "Fissure treasure!",
        MOON_ALTAR_COSMIC = "Not waitin' fer treasure, are ye?",
		MOON_ALTAR_ASTRAL = "Yarr! She be finally back in pieces!",
        MOON_ALTAR_ICON = "Back to th'boat wit' ye!",
        MOON_ALTAR_WARD = "Get luggin' this ol'lug!", 
		DRIFTWOOD_TREE = 
        {
            BURNING = "Th'drift's alight!",
            BURNT = "Aftermath o'a good plunderin'.",
            CHOPPED = "'Tis th'most useless stump.",
            GENERIC = "A land mast far frum home.",
        },
		DRIFTWOOD_LOG = "Sea's waste.",
		MOONBUTTERFLY = 
        {
        	GENERIC = "Me eyes spies a moon fly!",
        	HELD = "Ye better be worth m'weight in gold!",
        },
		MOONBUTTERFLYWINGS = "A bit o'th'moon fly.",
        MOONBUTTERFLY_SAPLING = "It'll grow inta a moon mast now.",
        ROCK_AVOCADO_FRUIT = "It'd crack me teeth if I's had any left!",
        ROCK_AVOCADO_FRUIT_RIPE = "Must'a cracked plenty o'teeth on these in me ol'days.",
        ROCK_AVOCADO_FRUIT_RIPE_COOKED = "Soft enough ta gum!",
        ROCK_AVOCADO_FRUIT_SPROUT = "Thet be lubber work.",
        ROCK_AVOCADO_BUSH = 
        {
        	BARREN = "Lost its rocks!",
			WITHERED = "Needs a spot o'rum.",
			GENERIC = "It be a shrub o'legend!",
			PICKED = "Th'treasures be missin'!",
			DISEASED = "Ye be lookin' like a case o'scurvy.",
            DISEASING = "I smells a case o'scurvy.",
			BURNING = "It be burnin' right up!",
		},
		DEAD_SEA_BONES = "Nay a good sign fer any sea farer.",
		HOTSPRING = 
        {
        	GENERIC = "A sea dog can't refuse a treasury spring soak.",
        	BOMBED = "I ain't likin' them fancy cannons.",
        	GLASS = "If ye soak too long ye get turned ta glass!",
			EMPTY = "It be an empty vat'o'moon.",
        },
		SEAFARING_PROTOTYPER =
        {
            GENERIC = "Thet b'fer tinkerin' on th'waves!",
            BURNT = "Th'sea's knowledge is one ye gotta work fer!",
        },
		BULLKELP_PLANT = 
        {
            GENERIC = "Gets tangled in me rigging.",
            PICKED = "Picked 'er clean.",
        },
	    FRUITDRAGON =
		{
			GENERIC = "Whut manner o'treasure do ye reckon 'e hoards?",
			RIPE = "Th'dragon's treasure be its flavor!",
			SLEEPING = "It be sleepin', but I ain't peepin' no gold!",
		},
        PUFFIN =
        {
            GENERIC = "Ye live off th'ocean too?",
            HELD = "Tell me yer tales o'th'sea.",
            SLEEPING = "'Tis not on watch.",
        },
		MUTATEDHOUND = 
        {
        	DEAD = "It tells no tales.",
        	GENERIC = "A scurvy ridden sea dog!",
        	SLEEPING = "Keelhaul th'beast, while ye can!",
        },
        MUTATED_PENGUIN = 
        {
			DEAD = "A face too ugly ta be buried at sea.",
			GENERIC = "A terrible foe t'be sure.",
			SLEEPING = "Keelhaul th'beast!",
		},
        CARRAT = 
        {
        	DEAD = "Inta th'stew ye go.",
        	GENERIC = "I knew me carrots were walkin'!",
        	HELD = "Do ye count as a meaty stew?",
        	SLEEPING = "I ain't trustin' no rats on deck.",
        },
		MOONGLASS = "It ain't no glass cannon.",
		MOONGLASS_CHARGED = "It be shinin' like th'sea on a full moon's glow!",
        MOONGLASS_ROCK = "Bits o'th'moon.",
		BULLKELP_ROOT = "Fine bit o'th'sea.",
        KELPHAT = "Th'head piece o'th'keelhauled!",
		KELP = "Gets tangled in me rigging.",
		KELP_COOKED = "Cooking it don't make it food.",
		KELP_DRIED = "This s'pose ta pass fer jerky?",
		GESTALT = "It be whisperin' forbidden tales o'th'seas.",
		GESTALT_GUARD = "It be a guardian o'sorts!",
		MOONGLASSAXE = "Cuttin' edges o'glass!",
		GLASSCUTTER = "This'd make ye captain!",
		COOKIECUTTER = "It'll take a vessel full'a ye ta take down me ship!",
		COOKIECUTTERSHELL = "Th'shell o'victory!",
		COOKIECUTTERHAT = "A fearsome cap!",
		SALTSTACK =
		{
			GENERIC = "Every sea dog's seen one 'er more.",
			MINED_OUT = "Arr. Th'sea's salt b'gone.",
			GROWING = "It be growin' back! Git it on me ship!",
		},
		SALTROCK = "Ye be as salty as me!",
		SALTBOX = "Me favorite food storin' treasure box!",
		TACKLESTATION = "Git yer proper fishin' gear!",
		TACKLESKETCH = "I cen't use this paper at sea! it be soggin' up!",
		GNARWAIL =
        {
            GENERIC = "Ye won't be impalin' ol'Woodlegs t'day!",
            BROKENHORN = "Yer 'orn b'Woodlegs' now! Yarr!",
            FOLLOWER = "Whut kind o'whale ain't be thrashin' 'bout me boats!?",
            BROKENHORN_FOLLOWER = "All whales arrre menaces to me boats!",
        },
        GNARWAIL_HORN = "A fine piece fer th'front o'th'ship.",
        MALBATROSS = "Arr! A bird o'legend! Avast ye!",
        MALBATROSS_FEATHER = "'Tis a mighty bird sail.",
        MALBATROSS_BEAK = "Ye'd ne'er see Woodlegs gettin' gobbled!",
        MAST_MALBATROSS_ITEM = "'Tis light as a feather.",
        MAST_MALBATROSS = "Fancy sail, thet.",
		MALBATROSS_FEATHERED_WEAVE = "Fabric fer a sail!",
		CALIFORNIAROLL = "Them be fancy fish treats.",
		SEAFOODGUMBO = "Down th'hatch!",
		SURFNTURF = "'Could use wit less turf.",
		
		OCEANFISHINGROD = "Woodlegs'seen bigger.",
        OCEANFISHINGBOBBER_BALL = "Th'fish'll be havin' a ball!",
        OCEANFISHINGBOBBER_OVAL = "Ain't no fish nor sailor can resist its glitters!!",
		OCEANFISHINGBOBBER_CROW = "I ain't be knowin' any fish wit' feathers.",
		OCEANFISHINGBOBBER_ROBIN = "I ain't be knowin' any fish wit' feathers.",
		OCEANFISHINGBOBBER_ROBIN_WINTER = "I ain't be knowin' any fish wit' feathers.",
		OCEANFISHINGBOBBER_CANARY = "I ain't be knowin' any fish wit' feathers.",
		OCEANFISHINGBOBBER_GOOSE = "Come git' th'feather, fishes!",
		OCEANFISHINGBOBBER_MALBATROSS = "Put a bit o'silver on th'end and ye'll catch 'em!",

		OCEANFISHINGLURE_SPINNER_RED = "Bait fer th'fish!",
		OCEANFISHINGLURE_SPINNER_GREEN = "Bait fer th'fish!",
		OCEANFISHINGLURE_SPINNER_BLUE = "Bait fer th'fish!",
		OCEANFISHINGLURE_SPOON_RED = "Bait fer th'fish!",
		OCEANFISHINGLURE_SPOON_GREEN = "Bait fer th'fish!",
		OCEANFISHINGLURE_SPOON_BLUE = "Bait fer th'fish!",
		OCEANFISHINGLURE_HERMIT_RAIN = "Bait fer th'fish!",
		OCEANFISHINGLURE_HERMIT_SNOW = "Bait fer th'fish!",
		OCEANFISHINGLURE_HERMIT_DROWSY = "Bait fer th'fish!",
		OCEANFISHINGLURE_HERMIT_HEAVY = "Bait fer th'fish!",

		OCEANFISH_SMALL_1 = "Don't ye b'starin' at ol'Woodlegs!",
		OCEANFISH_SMALL_2 = "Inta th'pot w'ya.",
		OCEANFISH_SMALL_3 = "Ye got thet look t'ya me likes!",
		OCEANFISH_SMALL_4 = "Ol'Woodlegs needs th'big'uns!",
		OCEANFISH_SMALL_5 = "Ye'd go well inta m'gullet!",
		OCEANFISH_SMALL_6 = "It be drippin' in leaves!",
		OCEANFISH_SMALL_7 = "Makes me nose wriggle.",
		OCEANFISH_SMALL_8 = "Hotted fish up ahead!",
		OCEANFISH_SMALL_9 = "Askin' fer a spittin' contest, arre ye?",
		OCEANFISH_MEDIUM_1 = "Been eatin' worse fish.",
		OCEANFISH_MEDIUM_2 = "I see all yer eye's peepin' me fishy-treasure!",
		OCEANFISH_MEDIUM_3 = "I wasn't lion about my aptitude for fishing!",
		OCEANFISH_MEDIUM_4 = "I be testin' m'luck t'night!",
		OCEANFISH_MEDIUM_5 = "I don't like lubbers mixin' in wit' me fish.",
		OCEANFISH_MEDIUM_6 = "Fer m'gullet!",
		OCEANFISH_MEDIUM_7 = "Fer m'gullet!",
		OCEANFISH_MEDIUM_8 = "Brrrr! Me fish be frozen!",
		OCEANFISH_MEDIUM_9 = "I ain't never smell a fish so sweet!",

		PONDFISH = "Fish! One o'me favorites.",
		PONDEEL = "Ye scalawag!",
        FISHMEAT = "Could use a bit'o'heat.",
        FISHMEAT_COOKED = "Th'fish roasted up good.",
        FISHMEAT_SMALL = "Tis hardly a bite'o'fish.",
        FISHMEAT_SMALL_COOKED = "'Tis a bit better.",
		SPOILED_FISH = "Smells like th'devil hisself made wind.",
		FISH_BOX = "Packed 'em tight!",
        POCKET_SCALE = "A wee weight.",
		TACKLECONTAINER = "Fer me prized hooks!",
		SUPERTACKLECONTAINER = "Me fishin' tools!",
		TROPHYSCALE_FISH =
		{
			GENERIC = "We all know Woodlegs catches th'biggest fish 'round 'ere!",
			HAS_ITEM = "Weight: {weight}\nCaught by: {owner}",
			HAS_ITEM_HEAVY = "Weight: {weight}\nCaught by: {owner}\nBah. I's seen bigger!",
			BURNING = "Landlubbers can't appreciate a good catchin'!",
			BURNT = "Arrrr! Whut swab be jealous o'me fish!?",
			OWNER = "Avast ye! Woodlegs' catch o'th'day!\nWeight: {weight}\nCaught by: {owner}",
			OWNER_HEAVY = "Weight: {weight}\nCaught by: {owner}\nOnly th'biggest catches from ol'Woodlegs!",
		},
		OCEANFISHABLEFLOTSAM = "Arr! Not me favorite kind o'loot.",
		WOBSTER_SHELLER = "Don't be scurryin' from me!", 
        WOBSTER_DEN = "'Tis th'shell beast's cabin.",
        WOBSTER_SHELLER_DEAD = "Aye! Now Woodlegs can eat 'em.",
        WOBSTER_SHELLER_DEAD_COOKED = "Yer fond of me lobster, ain't ye?",
        LOBSTERBISQUE = "Fends off th'scurvy!",
        LOBSTERDINNER = "I loves ta eat 'em!",
        WOBSTER_MOONGLASS = "Don't be scurryin' from me!",
        MOONGLASS_WOBSTER_DEN = "A cabin o'glass!",
		TRIDENT = "Get a load o'Woodlegs' moves!",
		
		WINCH =
		{
			GENERIC = "It be a winch!",
			RETRIEVING_ITEM = "It be winchin'.",
			HOLDING_ITEM = "Yo-ho-ho!",
		},
        HERMITHOUSE = {
            GENERIC = "It be a wreck o'a home.",
            BUILTUP = "Me home be th'sea, no time ta lubber about!",
        }, 
        SHELL_CLUSTER = "Get me shells, then get me gold!",
        --
		SINGINGSHELL_OCTAVE3 =
		{
			GENERIC = "I's know a good shanty fer this'un!",
		},
		SINGINGSHELL_OCTAVE4 =
		{
			GENERIC = "Tunes o'th'sea!",
		},
		SINGINGSHELL_OCTAVE5 =
		{
			GENERIC = "Me peg leg's tappin'!",
        },
        CHUM = "It be a meal! Nay meal o'mine.",
        SUNKENCHEST =
        {
            GENERIC = "Git yer land lubbin' grabbers off'a me treasures!",
            LOCKED = "Any pirate worth 'is salt can crack it!",
        },
        RESKIN_TOOL = "Dusts up me beard! ArrCHOOO!!",
        MOON_FISSURE_PLUGGED = "It be smellin' somethin' foul.",
		SQUID = "Yer tentacles ain't big e'nuff ta take down any ship!",
		GHOSTFLOWER = "Arrg! It be a ghostly flower!",
        SMALLGHOST = "Back from Davy Jones' Little Locker.",
        CRABKING = 
        {
            GENERIC = "I'll 'ave yer treasures and yer head fer dinner!",
            INERT = "I ain't givin' it no treasure o'mine!",
        },
		CRABKING_CLAW = "It be from me dreams! Arrg! And mee nightmares!",

		MESSAGEBOTTLE = "Yarr! Be it an 'X'?",
		MESSAGEBOTTLEEMPTY = "Yarr... it be an empty jarr.",
        MEATRACK_HERMIT =
        {
			BURNT = "Thet be a shame.",
			DONE = "She's ready!",
			DRYING = "Swing, swing, into me mouth!",
			DRYINGINRAIN = "Th'rain don't be helpin'.",
			GENERIC = "Hangin' spot fer me meats.",
			DONE_NOTMEAT = "She's ready!",
            DRYING_NOTMEAT = "Swing, swing, into me mouth!",
            DRYINGINRAIN_NOTMEAT = "Th'rain don't be helpin'.",
        },
        BEEBOX_HERMIT =
        {
			BURNT = "I smell burnt honey...",
			FULLHONEY = "Thar be gold in this box!",
			GENERIC = "Thar be bees.",
			NOHONEY = "'Tis empty o'th'golden treasure.",
			SOMEHONEY = "'Tis a wee bit o'gold left.",
			READY = "Thar be gold in this box!",
        },
        HERMITCRAB = "Ah! A fair maiden o'th'sea!",
        HERMIT_PEARL = "Yarrr! Yer treasure be mine!",
        HERMIT_CRACKED_PEARL = "ARR! Who broke it!",
        WATERPLANT = "Yer barnacles be lookin' might tasty!",
        WATERPLANT_BOMB = "Batten down th'hatches! Killer plant on deck!",
        WATERPLANT_BABY = "Thet be a wee plant!",
        WATERPLANT_PLANTER = "Plant 'em where I's want 'em!",

        SHARK = "Come at me an' lose, ye crusty devil!!",

        MASTUPGRADE_LAMP_ITEM = "Hoist up th'what's it!",
        MASTUPGRADE_LIGHTNINGROD_ITEM = "Keeps th'sky bolts off me boat!",

        WATERPUMP = "Don't be floodin' me boat wit' it!",

        BARNACLE = "Ah, barnacles!",
        BARNACLE_COOKED = "Me favorites!",

        BARNACLEPITA = "Yer makin' a mastery o' barnacles!",
        BARNACLESUSHI = "Fish 'n' Barnacles! Me heaven!",
        BARNACLINGUINE = "No barnacle tastes as sweet o'mine!",
        BARNACLESTUFFEDFISHHEAD = "It be a pirate's treat!",

        LEAFLOAF = "It be a loaf o'leaves.",
        LEAFYMEATBURGER = "Land lubber's burgers!",
        LEAFYMEATSOUFFLE = "I bin eatin' wrose!",
        MEATYSALAD = "Th'best part o'any salad be th'meats!",

		MOLEBAT = "Bet 'e hocks th'biggest loogies!",
        BATNOSE = "Keep yer eyes out fer a bat wit' a wooden nose.",
        BATNOSE_COOKED = "Cooked th'snot outta 'em!",
        BATNOSEHAT = "Gots me hands-free milk!",

        MUSHGNOME = "Do ye be spittin' spores at ol'Woodlegs now?",

        SPORE_MOON = "I ain't be sniffin' what yer sproutin'!",

        MOON_CAP = "A pirate eats what 'e can get!",
        MOON_CAP_COOKED = "I's seen worse grub in th'galley.",

        MUSHTREE_MOON = "All ye land-lubbin' trees look strange ta me!",

        LIGHTFLIER = "Be me guidin' light.",
        GROTTO_POOL_BIG = "Woodlegs' heard tales o'ponds thet turn ye ta stone...",
        GROTTO_POOL_SMALL = "Woodlegs' heard tales o'ponds thet turn ye ta stone...",
        DUSTMOTH = "Me ship crew could use a lubber like ye!",

        DUSTMOTHDEN = "Arr! Me ship ne'er been as spotless!",

        ARCHIVE_LOCKBOX = "One of ye lubbers open up this 'ere lock box!",
        ARCHIVE_CENTIPEDE = "I's slain worse at sea! Yarr!!",
        ARCHIVE_CENTIPEDE_HUSK = "Bits o'junk.",

        ARCHIVE_COOKPOT =
        {
			COOKING_LONG = "'Tis a slow recipe.",
			COOKING_SHORT = "Ye'll be done soon 'nuff.",
			DONE = "'Tis ready for eatin'.",
			EMPTY = "Th'ol'pot be empty.",
			BURNT = "Th'ol'pot be burnt up.",
        },

        ARCHIVE_MOON_STATUE = "We be nearin' tales o'legend!",
        ARCHIVE_RUNE_STATUE = 
        {
            LINE_1 = "Bah! I ain't speak lubber.",
            LINE_2 = "I only be knowin' th'language o'th'sea.",
            LINE_3 = "Bah! I ain't speak lubber.",
            LINE_4 = "I only be knowin' th'language o'th'sea.",
            LINE_5 = "Bah! I ain't speak lubber.",
        },        

		ARCHIVE_RESONATOR = {
            GENERIC = "Lookin' fer loot, arrre ye? Not if I's finds it first!",
            IDLE = "I hope yer treasure be worth some salt!",
        },
        ARCHIVE_RESONATOR_ITEM = "Doohickey thet does th'whutsit.",

        ARCHIVE_LOCKBOX_DISPENCER = {
			POWEROFF = "It ain't workin' no more.",
			GENERIC =  "Yer gonna show Woodlegs t'yer treasure whether ye likes it 'er not!",
		  },
  
		  ARCHIVE_SECURITY_DESK = {
			  POWEROFF = "If it used ta work, it don't no more.",
			  GENERIC = "I'll keep me good eye on it.",
		  },

        ARCHIVE_SECURITY_PULSE = "It be sailin' wit'out us!",

        ARCHIVE_SWITCH = {
            VALID = "Ye got yer rocks, now whut?",
            GEMS = "Nary a gem in sight.",
        },

        ARCHIVE_PORTAL = {
            POWEROFF = "It ain't goin' nowhere.",
            GENERIC = "Gettin' tired out me wits wit' this lubber-lunacy!",
        },

        WALL_STONE_2 = "It would sink at sea.",
        WALL_RUINS_2 = "These walls be even older than me!",

        REFINED_DUST = "Don't be travellin' up me nose now.",
        DUSTMERINGUE = "Might we ration it out?",

        SHROOMCAKE = "Me loves some mush.",

        NIGHTMAREGROWTH = "Yer splittin' me noggin!",

        TURFCRAFTINGSTATION = "Me worst enemy!",

        MOON_ALTAR_LINK = "We got sumthin' mixin' about.",
		-- FARMING
        COMPOSTINGBIN =
        {
            GENERIC = "Good dirt in thar.",
            WET = "Too wet, thet.",
            DRY = "It be dryin' up.",
            BALANCED = "Plant grub's ready ta serve!",
            BURNT = "Arrr! Ye didn't have ta do me nose dirty like thet!",
        },
        COMPOST = "Ye could mistake it fer galley grub.",
        SOIL_AMENDER = 
		{ 
			GENERIC = "I won't be waitin' 'round ta see it ripen!",
			STALE = "Me nose is startin' ta tingle!",
			SPOILED = "Pew! Thet's ripe!",
		},

		SOIL_AMENDER_FERMENTED = "Come get yer grub, lubbers!",

        WATERINGCAN = 
        {
            GENERIC = "A wee bit o'sea.",
            EMPTY = "Nay, me sea won't budge fer this'un.",
        },
        PREMIUMWATERINGCAN =
        {
            GENERIC = "Now thet's a cap'n's can.",
            EMPTY = "Nay, me sea won't budge fer this'un.",
        },

		FARM_PLOW = "It ain't no replacement fer a trusty crew!",
		FARM_PLOW_ITEM = "It ain't no replacement fer a trusty crew!",
		FARM_HOE = "Land lubbin' tools!",
		GOLDEN_FARM_HOE = "Fancy land lubbin' tools!",
		NUTRIENTSGOGGLESHAT = "Gots me a fancy eye glass and 'tis just fer dirt? Bah!",
		PLANTREGISTRYHAT = "Glad none o'me mateys can see me now.",

        FARM_SOIL_DEBRIS = "Where's th'cabin boy? Get swabbin'!",

		FIRENETTLES = "Wee volcanoes!",
		FORGETMELOTS = "Whut were me doin' again?",
		SWEETTEA = "Makes stayin' on land a wee bit bearable! Yarr!",
		TILLWEED = "Begone!",
		TILLWEEDSALVE = "Stops yer belly-achin'.",
		WEED_IVY = "Thinks we gots a traitor amongst we!",
        IVY_SNARE = "Yer gonna meet a slicey fate if ye tickle me pegs.",

		TROPHYSCALE_OVERSIZEDVEGGIES =
		{
			GENERIC = "Think ye can best Woodlegs?",
			HAS_ITEM = "Weight: {weight}\nHarvested on day: {day}\nA wee bit!",
			HAS_ITEM_HEAVY = "Weight: {weight}\nHarvested on day: {day}\nNow let's a treasure!",
            HAS_ITEM_LIGHT = "This ain't no grub o'mine!",
			BURNING = "Whut swab left th'scale on?",
			BURNT = "I's never said I was th'best at grubbin'.",
        },
        
        CARROT_OVERSIZED = "We be havin' stew tanight! An' th'night after!",
        CORN_OVERSIZED = "Our days be gettin' luckier!",
        PUMPKIN_OVERSIZED = "Ye could make a home outta thet'un!",
        EGGPLANT_OVERSIZED = "Woodlegs'll be lookin' purple after this'un!",
        DURIAN_OVERSIZED = "Yarr! Me nose be reapin' its rewards!",
        POMEGRANATE_OVERSIZED = "Nay just good for me heart, good fer me whole crew's!",
        DRAGONFRUIT_OVERSIZED = "Haul this o'er to me ship!",
        WATERMELON_OVERSIZED = "We'd need'a dock fer this watery grave!",
        TOMATO_OVERSIZED = "Woodlegs'll be makin' th'sea's biggest soup!",
        POTATO_OVERSIZED = "Get th'cook and lug this big mean mother hubbard to me ship!",
        ASPARAGUS_OVERSIZED = "Mix it in wit'me grub.",
        ONION_OVERSIZED = "Brings a tear to me eye, an I ain't even slicin'!",
        GARLIC_OVERSIZED = "No matey's gonna wanna whiff o'Woodlegs after this!",
        PEPPER_OVERSIZED = "Take a bite outta th'devil!",
        
        VEGGIE_OVERSIZED_ROTTEN = "Blech! Throw 'er o'erboard!",

		FARM_PLANT =
		{
			GENERIC = "Thet's green.",
			SEED = "I be missin' sailin'.",
			GROWING = "Get growin' good!",
			FULL = "Let's get them crops!",
			ROTTEN = "Blast ye!",
			FULL_OVERSIZED = "Get me big treasure box for this'un!",
			ROTTEN_OVERSIZED = "Yer disgracin' me!",
			FULL_WEED = "Mutiny!!",

			BURNING = "Might'n fetch a pale o'water.",
		},

        FRUITFLY = "I'll strike ye dead!",
        LORDFRUITFLY = "Damn ye!",
        FRIENDLYFRUITFLY = "Keep a watch o'me food.",
        FRUITFLYFRUIT = "I'm th'cap'n now.",

        SEEDPOUCH = "Pocket for me seeds.",

		-- Crow Carnival
		CARNIVAL_HOST = "Quit yer squawkin' an' show me ta ye hoard o'tickets!",
		CARNIVAL_CROWKID = "Not hidin' any loose tickets, are ye?",
		CARNIVAL_GAMETOKEN = "Quirky bit o'silver!",
		CARNIVAL_PRIZETICKET =
		{
			GENERIC = "Bah! Needs me more than thet!",
			GENERIC_SMALLSTACK = "Me treasure be growin' in size!",
			GENERIC_LARGESTACK = "Yarr-harr! All ye tickets belong ta me!",
		},

		CARNIVALGAME_FEEDCHICKS_NEST = "Think ye can trick ol'Woodlegs, do ye?",
		CARNIVALGAME_FEEDCHICKS_STATION =
		{
			GENERIC = "I ain't coughin' up me gold fer no kiddy games!",
			PLAYING = "I only feed me crew!",
		},
		CARNIVALGAME_FEEDCHICKS_KIT = "Get to settin' 'er up!",
		CARNIVALGAME_FEEDCHICKS_FOOD = "Grub's grub!",

		CARNIVALGAME_MEMORY_KIT = "Get to settin' 'er up!",
		CARNIVALGAME_MEMORY_STATION =
		{
			GENERIC = "I ain't coughin' up me gold fer no kiddy games!",
			PLAYING = "Woodlegs be th'champion o'spyin' wit' 'is wee eyes!",
		},
		CARNIVALGAME_MEMORY_CARD =
		{
			GENERIC = "Think ye can trick ol'Woodlegs, do ye?",
			PLAYING = "It be this'un! I's seent it!",
		},

		CARNIVALGAME_HERDING_KIT = "Get to settin' 'er up!",
		CARNIVALGAME_HERDING_STATION =
		{
			GENERIC = "I ain't coughin' up me gold fer no kiddy games!",
			PLAYING = "Give me pegs a break, damned thing!",
		},
		CARNIVALGAME_HERDING_CHICK = "Me pegs still be creakin' faster than ye!",

		CARNIVALGAME_SHOOTING_KIT = "Get to settin' 'er up!",
		CARNIVALGAME_SHOOTING_STATION =
		{
			GENERIC = "I ain't coughin' up me gold fer no kiddy games!",
			PLAYING = "Ol'Woodlegs's got an eye fer this'un!",
		},
		CARNIVALGAME_SHOOTING_TARGET =
		{
			GENERIC = "Think ye can trick ol'Woodlegs, do ye?",
			PLAYING = "Hark! To th' ditches wit' ye!",
		},

		CARNIVALGAME_SHOOTING_BUTTON =
		{
			GENERIC = "I ain't coughin' up me gold fer no kiddy games!",
			PLAYING = "Have it no fuse?",
		},

		CARNIVALGAME_WHEELSPIN_KIT = "Get to settin' 'er up!",
		CARNIVALGAME_WHEELSPIN_STATION =
		{
			GENERIC = "I ain't coughin' up me gold fer no kiddy games!",
			PLAYING = "There be no steerin' involved!",
		},

		CARNIVALGAME_PUCKDROP_KIT = "Get to settin' 'er up!",
		CARNIVALGAME_PUCKDROP_STATION =
		{
			GENERIC = "I ain't coughin' up me gold fer no kiddy games!",
			PLAYING = "Blast! To fire wit'cha!",
		},

		CARNIVAL_PRIZEBOOTH_KIT = "Box o'treasure!",
		CARNIVAL_PRIZEBOOTH =
		{
			GENERIC = "I'll be takin' yer whole stock!",
		},

		CARNIVALCANNON_KIT = "Yarr! 'Tis basically cannon!",
		CARNIVALCANNON =
		{
			GENERIC = "A lubber's sad excuse fer a cannon.",
			COOLDOWN = "This whut ye lubbers call a cannon? Bah!",
		},

		CARNIVAL_PLAZA_KIT = "A wee bit o'tree.",
		CARNIVAL_PLAZA =
		{
			GENERIC = "And whut of it!",
			LEVEL_2 = "Could use a speckle o'th'sea methinks.",
			LEVEL_3 = "Now rain upon th'gold!",
		},

		CARNIVALDECOR_EGGRIDE_KIT = "Whut treasure be this?",
		CARNIVALDECOR_EGGRIDE = "I wants me tickets back!",

		CARNIVALDECOR_LAMP_KIT = "It be light.",
		CARNIVALDECOR_LAMP = "Might take this'un back ta me ship.",
		CARNIVALDECOR_PLANT_KIT = "Whut wood be awaitin'?",
		CARNIVALDECOR_PLANT = "Aye, a wee tree.",
		CARNIVALDECOR_BANNER_KIT = "It be no banner o'mine.",
		CARNIVALDECOR_BANNER = "A luxury, t'be sure!",

		CARNIVALDECOR_FIGURE =
		{
			RARE = "Now THIS be a rare treasure!",
			UNCOMMON = "May be worth a pretty penny.",
			GENERIC = "Any seadog gots this'un!",
		},
		CARNIVALDECOR_FIGURE_KIT = "Yarr! Me favorite surprise!",
		CARNIVALDECOR_FIGURE_KIT_SEASON2 = "Yarr! Me favorite surprise!",

        CARNIVAL_BALL = "'Tis a ball.",
		CARNIVAL_SEEDPACKET = "'Tis seeds.",
		CARNIVALFOOD_CORNTEA = "Loves ta drink it, me does!",

        CARNIVAL_VEST_A = "It be a fair piratin' scarf!",
        CARNIVAL_VEST_B = "Cools ye britches.",
        CARNIVAL_VEST_C = "Nay good as a pirate's coat.",

        -- YOTB
        YOTB_SEWINGMACHINE = "I ain't doin' no swab work!",
        YOTB_SEWINGMACHINE_ITEM = "Start settin' 'er up.",
        YOTB_STAGE = "Me captain waters be better.",
        YOTB_POST =  "Let's get hitchin'",
        YOTB_STAGE_ITEM = "Start settin' 'er up.",
        YOTB_POST_ITEM =  "Start settin' 'er up.",

        YOTB_PATTERN_FRAGMENT_1 = "Could fashion this inta a fancy mast!",
        YOTB_PATTERN_FRAGMENT_2 = "Could fashion this inta a fancy mast!",
        YOTB_PATTERN_FRAGMENT_3 = "Could fashion this inta a fancy mast!",

        YOTB_BEEFALO_DOLL_WAR = {
            GENERIC = "E's a cuddly'un.",
            YOTB = "Thet lubber best be givin' ye gold, lest he wants ta walk th'plank.",
        },
        YOTB_BEEFALO_DOLL_DOLL = {
            GENERIC = "E's a cuddly'un.",
            YOTB = "Thet lubber best be givin' ye gold, lest he wants ta walk th'plank.",
        },
        YOTB_BEEFALO_DOLL_FESTIVE = {
            GENERIC = "E's a cuddly'un.",
            YOTB = "Thet lubber best be givin' ye gold, lest he wants ta walk th'plank.",
        },
        YOTB_BEEFALO_DOLL_NATURE = {
            GENERIC = "E's a cuddly'un.",
            YOTB = "Thet lubber best be givin' ye gold, lest he wants ta walk th'plank.",
        },
        YOTB_BEEFALO_DOLL_ROBOT = {
            GENERIC = "E's a cuddly'un.",
            YOTB = "Thet lubber best be givin' ye gold, lest he wants ta walk th'plank.",
        },
        YOTB_BEEFALO_DOLL_ICE = {
            GENERIC = "E's a cuddly'un.",
            YOTB = "Thet lubber best be givin' ye gold, lest he wants ta walk th'plank.",
        },
        YOTB_BEEFALO_DOLL_FORMAL = {
            GENERIC = "E's a cuddly'un.",
            YOTB = "Thet lubber best be givin' ye gold, lest he wants ta walk th'plank.",
        },
        YOTB_BEEFALO_DOLL_VICTORIAN = {
            GENERIC = "E's a cuddly'un.",
            YOTB = "Thet lubber best be givin' ye gold, lest he wants ta walk th'plank.",
        },
        YOTB_BEEFALO_DOLL_BEAST = {
            GENERIC = "E's a cuddly'un.",
            YOTB = "Thet lubber best be givin' ye gold, lest he wants ta walk th'plank.",
        },

        WAR_BLUEPRINT = "These be fancy patterns. Fer a lubber.",
        DOLL_BLUEPRINT = "These be fancy patterns. Fer a lubber.",
        FESTIVE_BLUEPRINT = "These be fancy patterns. Fer a lubber.",
        ROBOT_BLUEPRINT = "These be fancy patterns. Fer a lubber.",
        NATURE_BLUEPRINT = "These be fancy patterns. Fer a lubber.",
        FORMAL_BLUEPRINT = "These be fancy patterns. Fer a lubber.",
        VICTORIAN_BLUEPRINT = "These be fancy patterns. Fer a lubber.",
        ICE_BLUEPRINT = "These be fancy patterns. Fer a lubber.",
        BEAST_BLUEPRINT = "These be fancy patterns. Fer a lubber.",

        BEEF_BELL = "Needs me one of these fer me crew. Yarr!",
		-- YOT Catcoon
		KITCOONDEN = 
		{
			GENERIC = "Th'kits be livin' lavish!",
            BURNT = "Down she goes!",
			PLAYING_HIDEANDSEEK = "We gots kits to spy wit' them eyes!",
			PLAYING_HIDEANDSEEK_TIME_ALMOST_UP = "Work them eye muscles! We're runnin' low o'time!",
		},

		KITCOONDEN_KIT = "A ship full!",

		TICOON = 
		{
			GENERIC = "'E's got an eye fer gold 'e does.",
			ABANDONED = "Nay prey, nay pay!",
			SUCCESS = "Ye struck gold!",
			LOST_TRACK = "Our loot's been pillaged! Arrr!",
			NEARBY = "His deadlights be locked on treasure!",
			TRACKING = "Whut are ye pirate senses tellin' ye?",
			TRACKING_NOT_MINE = "Run a shot across thet bow!",
			NOTHING_TO_TRACK = "Bah!",
			TARGET_TOO_FAR_AWAY = "Needs to get ye a spyglass, we be far out!",
		},
		
		YOT_CATCOONSHRINE =
        {
            GENERIC = "Don't be askin' me fer any o'me treasure!",
            EMPTY = "It be askin' fer cat food!",
            BURNT = "Burnt to a brickette.",
        },

		KITCOON_FOREST = "Yer as scruffy as ol'Woodlegs' beard!",
		KITCOON_SAVANNA = "Yer as scruffy as ol'Woodlegs' beard!",
		KITCOON_MARSH = "Yer as scruffy as ol'Woodlegs' beard!",
		KITCOON_DECIDUOUS = "Yer as scruffy as ol'Woodlegs' beard!",
		KITCOON_GRASS = "Yer as scruffy as ol'Woodlegs' beard!",
		KITCOON_ROCKY = "Yer as scruffy as ol'Woodlegs' beard!",
		KITCOON_DESERT = "Yer as scruffy as ol'Woodlegs' beard!",
		KITCOON_MOON = "Yer as scruffy as ol'Woodlegs' beard!",
		KITCOON_YOT = "Yer as scruffy as ol'Woodlegs' beard!",
        -- Moon Storm
        ALTERGUARDIAN_PHASE1 = {
            GENERIC = "Hark! Let us down this foul machine!",
            DEAD = "Down wit' ye!! Feed th'fishes!",
        },
        ALTERGUARDIAN_PHASE2 = {
            GENERIC = "Let Neptune strike ye dead!",
            DEAD = "Stand down!",
        },
        ALTERGUARDIAN_PHASE2SPIKE = "Nearly missed me pegs!",
        ALTERGUARDIAN_PHASE3 = "We'll be showin' ye th'depths of th'sea!",
        ALTERGUARDIAN_PHASE3TRAP = "No time fer restin', lads!",
        ALTERGUARDIAN_PHASE3DEADORB = "Stay down, lad.",
        ALTERGUARDIAN_PHASE3DEAD = "I gots me good eye on ye.",

        ALTERGUARDIANHAT = "Tell me more of yer stories out at sea.",
        ALTERGUARDIANHATSHARD = "Nice bit o'treasure, thet.",

        MOONSTORM_GLASS = {
            GENERIC = "Glassed up!",
            INFUSED = "Now it be glowin'."
        },

        MOONSTORM_STATIC = "I cans take a spark 'er two.",
        MOONSTORM_STATIC_ITEM = "Yer makin' me britches tingle!",
        MOONSTORM_SPARK = "Nay a spark o'life.",

        BIRD_MUTANT = "Yer not lookin' good, matey.",
        BIRD_MUTANT_SPITTER = "Yer a sight fer sore eyes, lass.",

        WAGSTAFF_NPC = "Whut could ye offer Woodlegs in exchange, ye ol'lubber?",
        WAGSTAFF_NPC_MUTATIONS = "'E's come back t'reap our sowin'! Ye'll be walkin' a plank fer thet!",
        WAGSTAFF_NPC_WAGPUNK = "Arr! Landlubbers be trottin' 'bout wit'thar no pegs!",

        ALTERGUARDIAN_CONTAINED = "Yer sappin' th'wind from its sails!",

        WAGSTAFF_TOOL_1 = "Who's got me doin' cabin work?",
        WAGSTAFF_TOOL_2 = "Yer gold best be th' shiniest.",
        WAGSTAFF_TOOL_3 = "I'd take it fer meself if Is could!",
        WAGSTAFF_TOOL_4 = "All yer lubber tools be lookin' th'same!",
        WAGSTAFF_TOOL_5 = "I can'ts tell th'difference.",

        MOONSTORM_GOGGLESHAT = "I like me grub in my gut, not me head.",

        MOON_DEVICE = {
            GENERIC = "Some of us be tryin' ta bask in moon glow!",
            CONSTRUCTION1 = "Th'tides be changin'.",
            CONSTRUCTION2 = "How th'tides be turnin'!",
        },
		-- Waterlog
        WATERTREE_PILLAR = "Avast ye! Beast o'a tree ahead!",
        OCEANTREE = "Bringin' th'land to th'lubber are ye?",
        OCEANTREENUT = "Thar be a nut.",
        WATERTREE_ROOT = "Don't go gettin' in me riggin'!",

        OCEANTREE_PILLAR = "Yarr. Now thet be th'breeze I need!",
        
        OCEANVINE = "Don't go stranglin' me crew!",
        FIG = "I eats it I do.",
		FIG_COOKED = "Good fer me gums!",

        SPIDER_WATER = "Foul-legged sea creatures be goin' fer me boat!",
        MUTATOR_WATER = "Keep yer biscuits ta yerselves!",
        OCEANVINE_COCOON = "Careful swingin' yer cutlass. We gots trouble.",
		OCEANVINE_COCOON_BURNT = "Blast. They ain't be havin' no spider's gold.",

        GRASSGATOR = "'E's got an eye fer gold 'e does!",

        TREEGROWTHSOLUTION = "Which poor sap'll be chuggin' this moonshine?",

        FIGATONI = "Gots all me figs tucked in tight!",
        FIGKABAB = "Th'stick's fer pickin' it outta yer teeth. If ever thar were any!",
        KOALEFIG_TRUNK = "Thet nose be stuffier than ol'Woodlegs'!",
        FROGNEWTON = "Pop it in yer mouth an get back ta sailin' th'blue!",
		-- The Terrorarium
        TERRARIUM = {
            GENERIC = "A most cursed treasure t'be!",
            CRIMSON = "Whut manner o'treasure be this?",
            ENABLED = "Arrre me eyes deceivin' me?",
			WAITING_FOR_DARK = "Best hit th'deck fer th'night.",
			COOLDOWN = "It be windin' doon.",
			SPAWN_DISABLED = "No more peepers be peepin'!",
        },

        -- Wolfgang
        MIGHTY_GYM = 
        {
            GENERIC = "Ye got wooden arms, ye'd fit right in me crew!",
            BURNT = "Best not happen to me pegs.",
        },

        DUMBBELL = "Me boat's th'one whut does th'liftin'!",
        DUMBBELL_GOLDEN = "Ain't no treasure Woodlegs can't haul!",
		DUMBBELL_MARBLE = "Ye could do wit' liftin' a few brain muscles! Arr!",
        DUMBBELL_GEM = "Ain't no treasure Woodlegs can't haul!",
        POTATOSACK = "Yer hoardin' a whole week o'crew's meals in thet haul!",

		DUMBBELL_HEAT = "Best get a bucket fer all thet sweatin'!",
        DUMBBELL_REDGEM = "Woodlegs always says thar be no better work than liftin' treasure!",
        DUMBBELL_BLUEGEM = "Woodlegs always says thar be no better work than liftin' treasure!",

        TERRARIUMCHEST = 
		{
			GENERIC = "Ain't no feelin' greater than lootin' treasure!",
			BURNT = "'Tis a dark day...",
			SHIMMER = "This coffer be callin' me name! Yarr!",
		},

		EYEMASKHAT = "Woodlegs had 'is good eye on ye th'whole time!",

        EYEOFTERROR = "Avast! We're gonna need th'largest eyepatch we got!",
        EYEOFTERROR_MINI = "Scupper thet!",
        EYEOFTERROR_MINI_GROUNDED = "Back ye devils!",

        FROZENBANANADAIQUIRI = "Gots me feelin' like a wee lad!",
        BUNNYSTEW = "Th'lucky one be me t'night!",
        MILKYWHITES = "I ain't hocked up this 'un!",

        CRITTER_EYEOFTERROR = "Ye be a sailor's first mate, be it they wear an eye-patch!",

        SHIELDOFTERROR ="Gots a nasty bite to it, don't it?",
        TWINOFTERROR1 = "Arr! Woodlegs's felled squids wit' bigger eyes than thet!",
        TWINOFTERROR2 = "Arr! Woodlegs's felled squids wit' bigger eyes than thet!",

		-- Cult of the Lamb
		COTL_TRINKET = "Kinda head fits this crown?",
		TURF_COTL_GOLD = "Gold clinkin' 'neath me pegs be music ta th'ears!",
		TURF_COTL_BRICK = "Me pegs ain't made fer skippin' rocks.",
		COTL_TABERNACLE_LEVEL1 =
		{
			LIT = "A soothin' bit o'light",
			GENERIC = "Thar be near no wind in them sails!",
		},
		COTL_TABERNACLE_LEVEL2 =
		{
			LIT = "Thar be somethin' in th' light, might it let me touch it.",
			GENERIC = "Thar be near no wind in them sails!",
		},
		COTL_TABERNACLE_LEVEL3 =
		{
			LIT = "Yer a mighty skipper with thet enchanting light.",
			GENERIC = "Thar be near no wind in them sails!",
		},

		-- Year of the Catcoon
        CATTOY_MOUSE = "It be a wee wheelie.",
        KITCOON_NAMETAG = "I'll keep callin' ye swabs and mates!",

		KITCOONDECOR1 =
        {
            GENERIC = "Bit fowl, thet.",
            BURNT = "Blasted!",
        },
		KITCOONDECOR2 =
        {
            GENERIC = "Landlubber foolery!",
            BURNT = "Blasted!",
        },

		KITCOONDECOR1_KIT = "Needs a bit o'buildin'.",
		KITCOONDECOR2_KIT = "Needs a bit o'buildin'.",
        -- WX78
        WX78MODULE_MAXHEALTH = "It be colorful as a gem, but nay precious as one!",
		WX78MODULE_MAXSANITY1 = "It be colorful as a gem, but nay precious as one!",
        WX78MODULE_MAXSANITY = "It be colorful as a gem, but nay precious as one!",
        WX78MODULE_MOVESPEED = "It be colorful as a gem, but nay precious as one!",
        WX78MODULE_MOVESPEED2 = "It be colorful as a gem, but nay precious as one!",
        WX78MODULE_HEAT = "It be colorful as a gem, but nay precious as one!",
        WX78MODULE_NIGHTVISION = "It be  colorful as a gem, but nay precious as one!",
        WX78MODULE_COLD = "It be colorful as a gem, but nay precious as one!",
        WX78MODULE_TASER = "It be colorful as a gem, but nay precious as one!",
        WX78MODULE_LIGHT = "It be colorful as a gem, but nay precious as one!",
		WX78MODULE_MAXHUNGER1 = "It be colorful as a gem, but nay precious as one!",
        WX78MODULE_MAXHUNGER = "It be colorful as a gem, but nay precious as one!",
        WX78MODULE_MUSIC = "It be colorful as a gem, but nay precious as one!",
        WX78MODULE_BEE = "It be colorful as a gem, but nay precious as one!",
        WX78MODULE_MAXHEALTH2 = "It be colorful as a gem, but nay precious as one!",

        WX78_SCANNER = 
        {
            GENERIC ="Back ye flyin' devil! Ye won't be scannin' Woodlegs t'day!",
            HUNTING = "'E be on th'hunt!",
            SCANNING = "Ye got anythin' good fer ol'Woodlegs too?",
        },

        WX78_SCANNER_ITEM = "He be loungin' around! Thet gets ye keelhauled on me deck!",
		WX78_SCANNER_SUCCEEDED = "Be yer bulb faulty?",
        WX78_MODULEREMOVER = "Bet it works fer a tooth 'er two!",

        SCANDATA = "Thet ain't th'kind o'map I be wantin'!",

		-- QOL 2022
		JUSTEGGS = "An' they say ol'Woodlegs ain't be cookin' well! Plague on 'em!",
		VEGGIEOMLET = "Me ship o'egg, sailin' to me gullet!",
		TALLEGGS = "Pillaged me th'most treasured fabergé eggs!",
		BEEFALOFEED = "Thet be beast gruel.",
		BEEFALOTREAT = "Ain't a lick o'difference from a ship's grub!",

        -- Pirates
        BOAT_ROTATOR = "Let th'captain give 'er a spin!",
        BOAT_ROTATOR_KIT = "We'll be takin' th'ship fer a spin!",
        BOAT_BUMPER_KELP = "Better use than puttin' it in yer gullet!",
        BOAT_BUMPER_KELP_KIT = "Batten down th'hatches!",
        BOAT_BUMPER_SHELL = "A peg-legged step closer to a man-o-war!",
        BOAT_BUMPER_SHELL_KIT = "Batten down th'hatches!",
        BOAT_CANNON = {
            GENERIC = "Man th'cannons!",
            AMMOLOADED = "Give 'em no quarter! Yarr!",
            NOAMMO = "Arr! Who be forgettin' thar job on me ship?",
        },
        BOAT_CANNON_KIT = "Th'deck were dire without it!",
        CANNONBALL_ROCK_ITEM = "Six pounders ready ta fire!",

        OCEAN_TRAWLER = {
            GENERIC = "Me crew'll be eatin' fer as long as it works!",
            LOWERED = "We got th'seas to plunder, nay wait aboot!",
            CAUGHT = "Avast ye!",
            ESCAPED = "Blasted thing! Arrr!",
            FIXED = "A real fixer-upper!",
        },
        OCEAN_TRAWLER_KIT = "I ain't gots all day ta wait aboot!",

        BOAT_MAGNET =
        {
            GENERIC = "Here be th'devil's work.",
            ACTIVATED = "'Tis witchcraft!",
        },
        BOAT_MAGNET_KIT = "What treachery arrre ye layabouts plottin' on me ship?",

        BOAT_MAGNET_BEACON =
        {
            GENERIC = "Th'mechanical siren song.",
            ACTIVATED = "Witchcraft!",
        },
        DOCK_KIT = "Sea portable!",
        DOCK_WOODPOSTS_ITEM = "Th'dock ain't whole without 'em!",

        MONKEYHUT =
        {
            GENERIC = "Yer homes be plentiful, but so be me cannons!",
            BURNT = "Lest ye forget th'name o'Woodlegs!",
        },
        POWDER_MONKEY = "Ye scurvy dog! Ye'll be shark bait ta me crew!",
        PRIME_MATE = "Woodlegs takes no orders from no bilge-suckin' scallywag!",
		LIGHTCRAB = "A shine o'th'land!",
        CUTLESS = "A swab's cutlass!",
        CURSED_MONKEY_TOKEN = "Be it pieces o'eight?",
        OAR_MONKEY = "Put yer backs into it, lads!",
        BANANABUSH = "Thet brush be sproutin' yeller treasure!",
        DUG_BANANABUSH = "Thet brush be sproutin' yeller treasure!",
        PALMCONETREE = "A tree o'palm!",
        PALMCONE_SEED = "Th'seed o' a mighty tree.",
        PALMCONE_SAPLING = "A wee tree ta be!",
        PALMCONE_SCALE = "Th'scale o' a mighty tree.",
        MONKEYTAIL = "I gots a few swabs I could pin these ta!",
        DUG_MONKEYTAIL = "I gots a few swabs I could pin these ta!",

        MONKEY_MEDIUMHAT = "Th'pirate's life fer me!",
        MONKEY_SMALLHAT = "Fit fer a scallywag!",
        POLLY_ROGERSHAT = "A proper fit fer me head an' no other!",
        POLLY_ROGERS = "Ack! Thet's me Polly!",--SW

        MONKEYISLAND_PORTAL = "Bewitched thing!",
        MONKEYISLAND_PORTAL_DEBRIS = "It be spittin' oot all sort'a machinations!",
        MONKEYQUEEN = "Hand over yer golden pile of delicious treasures!",
        MONKEYPILLAR = "Nay a bit of loot to be seen!",
        PIRATE_FLAG_POLE = "Every pirate worth 'is salt 'as a flag!",

        BLACKFLAG = "Ol'Jolly Roger's tried an' true, fer sailin' th'ocean blue!",
        PIRATE_STASH = "Me favorite kind of buried thing!",
        STASH_MAP = "Marks th'way ta treasure!",

        BANANAJUICE = "An' a splash o'rum! Me clap o'thunder!",

        FENCE_ROTATOR = "A sword fer swabs stickin' it to thar posts!",

        CHARLIE_STAGE_POST = "Me gots pirate's years worth o'tales, told only at sea!",
        CHARLIE_LECTURN = "Ain't gots time ta dillydaddle wit' th'lubbers!",

        CHARLIE_HECKLER = "Keep cacklin' cacklers an' we be keelhaulin' break o'dawn!",

        PLAYBILL_THE_DOLL = "No pirate plays by th'book!",
        STATUEHARP_HEDGESPAWNER = "Claimed by th'barancles o'th'land.",
        HEDGEHOUND = "ARR!! Ye dare run a rig under Woodlegs' nose!?",
        HEDGEHOUND_BUSH = "It be a pretty bush.",

        MASK_DOLLHAT = "It be a bewitched mask o'sorts!",
        MASK_DOLLBROKENHAT = "It be a bewitched mask o'sorts!",
        MASK_DOLLREPAIREDHAT = "It be crumblin'! No worth on me ship!",
        MASK_BLACKSMITHHAT = "It be a bewitched mask o'sorts!",
        MASK_MIRRORHAT = "It be a bewitched mask o'sorts!",
        MASK_QUEENHAT = "It be a bewitched mask o'sorts!",
        MASK_KINGHAT = "It be a bewitched mask o'sorts!",
        MASK_TREEHAT = "It be a bewitched mask o'sorts!",
        MASK_FOOLHAT = "It be a bewitched mask o'sorts!",

        COSTUME_DOLL_BODY = "Th'rags o'lubbers!",
        COSTUME_QUEEN_BODY = "Th'long clothes o'royalty. Bah!",
        COSTUME_KING_BODY = "Th'long clothes o'royalty. Bah!",
        COSTUME_BLACKSMITH_BODY = "Th'rags o'lubbers!",
        COSTUME_MIRROR_BODY = "Th'rags o'lubbers!",
        COSTUME_TREE_BODY = "Th'rags o'lubbers!",
        COSTUME_FOOL_BODY = "Th'rags o'lubbers!",

        STAGEUSHER =
        {
            STANDING = "Keep yer greasy mitts off me treasure, scallywag!",
            SITTING = "Me nose be catchin' a whiff o'grabby hands!",
        },
        SEWING_MANNEQUIN =
        {
            GENERIC = "Gots a fine pair o'legs to 'im!",
            BURNT = "Burnt ta a brickette 'e is.",
        },

		-- Waxwell
		MAGICIAN_CHEST = "A devil's box, full o'infinite splendarrr!",
		TOPHAT_MAGICIAN = "No tricorn o'mine be doin' devil's work!",

        -- Year of the Rabbit
        YOTR_FIGHTRING_KIT = "Don't be fightin' o'er who gets ta unbox it!",
        YOTR_FIGHTRING_BELL =
        {
            GENERIC = "Arr! Give 'em no quarter, lads!",
            PLAYING = "Bah! These landlubbers ain't use cutlasses, noless a bit o'firepower!",
        },

        YOTR_DECOR_1 = {
            GENERAL = "Lantern o' carrot munchin' devils.",
            OUT = "Its soul 'as left its body all cold and lubbery.",
        },
        YOTR_DECOR_2 = {
            GENERAL = "Lantern o' carrot munchin' devils.",
            OUT = "Its soul 'as left its body all cold and lubbery.",
        },

        HAREBALL = "Yarr! Th'loogies ol'Woodlegs 'as hocked up would make ye curdle!",
        YOTR_DECOR_1_ITEM = "Lantern wit' nowhere ta glow.",
        YOTR_DECOR_2_ITEM = "Lantern wit' nowhere ta glow.",

		--
		DREADSTONE = "No cursed stone will be settin' in stone aboard me ship!",
		HORRORFUEL = "Accursed magic risen from th'depths. Strikes fear into th'hearts o' any sea dog.",
		DAYWALKER =
		{
			GENERIC = "Blast thet scurvy dog back down!",
			IMPRISONED = "Locked up in th'brig, few fates worse than 'er.",
		},
		DAYWALKER_PILLAR =
		{
			GENERIC = "Woodlegs 'as a soft spot fer th' caged.",
			EXPOSED = "Arrgh! Bring th'cannons to show it what fer!",
		},
		ARMORDREADSTONE = "Th'devil 'as its grip tight on ye.",
		DREADSTONEHAT = "Makes ol'Woodlegs' brain become couscous!",

        -- Rifts 1
        LUNARRIFT_PORTAL = "Ye can hear th'sounds o'sailors past itchin' at its doorstep.",
        LUNARRIFT_CRYSTAL = "A shiny bit o'treasure as far as ye eye can see!",

        LUNARTHRALL_PLANT = "Sharpen yer cutlasses an' cut its riggin'!",
        LUNARTHRALL_PLANT_VINE_END = "Kraken o' th'land, ye won't be takin' Woodlegs t'day!",

		LUNAR_GRAZER = "Blasted devil o'sea smoke!",

        PUREBRILLIANCE = "No pearl 've seen shines as bright as she!",
        LUNARPLANT_HUSK = "Fair bit o'lubber leather.",

		LUNAR_FORGE = "Let th'moon guide me cutlass like no other!",
		LUNAR_FORGE_KIT = "Me ship's crew be slackin'!",

		LUNARPLANT_KIT = "Blast! It pricks at Woodlegs' wooden fingers!",
		ARMOR_LUNARPLANT = "Wit' all th'safety o' th'moon's guidance!",
		LUNARPLANTHAT = "Woodlegs 'as gone this long wit'out a full moon's curse.",
		BOMB_LUNARPLANT = "Load 'im up, see swabs do a jig in th' moonlight!",
		STAFF_LUNARPLANT = "Contained th'moon with all its wickedness!",
		SWORD_LUNARPLANT = "Wit' th'blessin' o' th'moon Woodlegs will strike 'is foes!",
		PICKAXE_LUNARPLANT = "See how th'moon makes 'er shimmer!",
		SHOVEL_LUNARPLANT = "Break th'dirt wit' th'finest o'moon glow.",

		BROKEN_FORGEDITEM = "Ye broke it.",

        PUNCHINGBAG = "Ahoy, let's see how ol'Woodlegs still can spar!",

        -- Rifts 2
        SHADOWRIFT_PORTAL = "I seen these murky depths asea.",

		SHADOW_FORGE = "Any ol' salt know not ta miss wit' devil's trickery.",
		SHADOW_FORGE_KIT = "Bunch'a layabouts leavin' work about!",

        FUSED_SHADELING = "Bah! This be th'best crew ye got?",
        FUSED_SHADELING_BOMB = "Hit th'deck! It be runnin' a shot on us!",

		VOIDCLOTH = "Dropped its grimy whiskers.",
		VOIDCLOTH_KIT = "Blast! It pricks at Woodlegs' wooden fingers!",
		VOIDCLOTHHAT = "Don't let 'er whispers worm its way inta ya noggin'!",
		ARMOR_VOIDCLOTH = "Th'ill-fated cloak o' th'cursed.",

        VOIDCLOTH_UMBRELLA = "Nay hatches needin' ta be batten wit' 'er!",
        VOIDCLOTH_SCYTHE = "Ain't no sow Woodlegs' 'asn't reaped!",

		SHADOWTHRALL_HANDS = "Me crew woulda had yer hands clean off!",
		SHADOWTHRALL_HORNS = "No whale 'er otherwise be swallowin' Woodlegs t'day!",
		SHADOWTHRALL_WINGS = "Accursed witch be scramblin' fer me bones! Ye won't 'ave 'em!",

        CHARLIE_NPC = "Woodlegs 'as heard tales o' th'dreaded selkie. Be it true?",
        CHARLIE_HAND = "An' whut would Woodlegs be takin' home in return, witch?",

        NITRE_FORMATION = "Thar be rocks below.",
        DREADSTONE_STACK = "Rocks from th' deepest o' th'briny blue.",
        
        SCRAPBOOK_PAGE = "Be it a map? Or be it a landlubber's nonsense?",

        LEIF_IDOL = "Do ye take constructive criticism, matey?",
        WOODCARVEDHAT = "Woodlegs be wood frum head t' toe!",
        WALKING_STICK = "I gots two'a them 'neath me!",

        IPECACSYRUP = "Ain't no different from soup served up at th'galley!",
        BOMB_LUNARPLANT_WORMWOOD = "Ye fancy a cannon, matey?", -- Unused
        WORMWOOD_MUTANTPROXY_CARRAT =
        {
        	DEAD = "Inta th'stew ye go.",
        	GENERIC = "I knew me carrots were walkin'!",
        	HELD = "Do ye count as a meaty stew?",
        	SLEEPING = "I ain't trustin' no rats on deck.",
        },
        WORMWOOD_MUTANTPROXY_LIGHTFLIER = "A wee guide fer a raft.",
		WORMWOOD_MUTANTPROXY_FRUITDRAGON =
		{
			GENERIC = "Whut manner o'treasure do ye reckon 'e hoards?",
			RIPE = "Th'dragon's treasure be its flavor!",
			SLEEPING = "It be sleepin', but I ain't peepin' no gold!",
		},

        SUPPORT_PILLAR_SCAFFOLD = "Thet wall be haunting.",
        SUPPORT_PILLAR = "Yer walls be crumblin' ta bits.",
        SUPPORT_PILLAR_COMPLETE = "No wall should match th'height o' a ship!",
        SUPPORT_PILLAR_BROKEN = "Walls were made t'be broke!",

		SUPPORT_PILLAR_DREADSTONE_SCAFFOLD = "Thet wall be haunting.",
		SUPPORT_PILLAR_DREADSTONE = "Yer walls be crumblin' ta bits.",
		SUPPORT_PILLAR_DREADSTONE_COMPLETE = "A wall only a lubber could love.",
		SUPPORT_PILLAR_DREADSTONE_BROKEN = "Argh! Now its curse be spreading throughout th'land.",

        WOLFGANG_WHISTLE = "Ye'll need a boomin' voice ta yell 'cross th' waves!",

        -- Rifts 3

        MUTATEDDEERCLOPS = "Got thet glassy look to it only a dead lubber could muster!",
        MUTATEDWARG = "Th'haunted look in its eye will send sailor's spines a chill!",
        MUTATEDBEARGER = "Th'undead be restless.",

        LUNARFROG = "Got more warts than ol'Woodlegs!",

        DEERCLOPSCORPSE =
        {
            GENERIC  = "Davy Jones' Locker 'as no room for landlubbers.",
            BURNING  = "'Aye. Let 'er weary soul rest.",
            REVIVING = "Thar be no rest for th'restless!",
        },

        WARGCORPSE =
        {
            GENERIC  = "Send 'er where th'sun don't shine and th'wind ain't blow!",
            BURNING  = "'Aye. Let 'er weary soul rest.",
            REVIVING = "Arrgh! This be why ye send 'em to th'briny blue!",
        },

        BEARGERCORPSE =
        {
            GENERIC  = "Be lucky th'wind don't send them deathly scents yer way!",
            BURNING  = "'Aye. Let 'er weary soul rest.",
            REVIVING = "Th'dead got tales ta tell!",
        },

        BEARGERFUR_SACK = "Got a bearded chest o'me own!",
        HOUNDSTOOTH_BLOWPIPE = "Got th'taste o'sailor's teeth to it.",
        DEERCLOPSEYEBALL_SENTRYWARD =
        {
            GENERIC = "Whut does thet briny eye o'yours peep?",    -- Enabled.
            NOEYEBALL = "Could use an eye-patch methinks.",  -- Disabled.
        },
        DEERCLOPSEYEBALL_SENTRYWARD_KIT = "Got a job fer any layabouts!",

        SECURITY_PULSE_CAGE = "Cruel an' unusual cage ta hang from a gibbet.",
        SECURITY_PULSE_CAGE_FULL = "Better a faerie than ol'Woodlegs!",

		CARPENTRY_STATION =
        {
            GENERIC = "Carve up Woodlegs a new pair o'pegs!",
            BURNT = "Best a horse o'wood than th'horse o'me boat!",
        },

        WOOD_TABLE = -- Shared between the round and square tables.
        {
            GENERIC = "Got spare legs, ever I need 'em!",
            HAS_ITEM = "Arr! Keep them tables clean!",
            BURNT = "Seen a world o'these burnt.",
        },

        WOOD_CHAIR =
        {
            GENERIC = "Keeps yer booty planted!",
            OCCUPIED = "Thar's no restin' yer weary legs aboard me ship!",
            BURNT = "Ain't fit fer no booty.",
        },

        DECOR_CENTERPIECE = "Arr! I be no gentlemanly pirate! Ye make a mockery o'me!",
        DECOR_LAMP = "A lantern fer lubbers.",
        DECOR_FLOWERVASE =
        {
			GENERIC = "Woodlegs be wary.",
			EMPTY = "It be holdin' nuthin'.",
			WILTED = "It be wiltin'.",
			FRESHLIGHT = "Aye, thet's bright.",
			OLDLIGHT = "Get me some better lights, lubbers.",
        },
        DECOR_PICTUREFRAME =
        {
            GENERIC = "Would make a mockery o'me treasure hoard.",
            UNDRAWN = "Thar be no value t'it!",
        },
        DECOR_PORTRAITFRAME = "Would look a might better wit'some daggers wedged in.",

        PHONOGRAPH = "Get yer pegs tappin' wit'some o'me shanties!",
        RECORD = "A bewitched tune it plays.",
        RECORD_CREEPYFOREST = "They be makin' boats fer shanties!",
        RECORD_DANGER = "Every old salt knows a good shanty!",
        RECORD_DAWN = "Every old salt knows a good shanty!",
        RECORD_DRSTYLE = "They be makin' boats fer shanties!",
        RECORD_DUSK = "Every old salt knows a good shanty!",
        RECORD_EFS = "Let ol'Woodlegs show ye how a true sailor sings!",
        RECORD_END = "They be makin' boats fer shanties!",
        RECORD_MAIN = "Every old salt knows a good shanty!",
        RECORD_WORKTOBEDONE = "Let ol'Woodlegs show ye how a true sailor sings!",

        ARCHIVE_ORCHESTRINA_MAIN = "...Would ye happen to 'ave a map?",

        WAGPUNKHAT = "Woodlegs be a walkin' binnacle!",
        ARMORWAGPUNK = "Ye can hear th'endless tickin' o'time.",
        WAGSTAFF_MACHINERY = "Might be a bit o'silver an' copper hereabouts.",
        WAGPUNK_BITS = "Bits o' lubber's junk.",
        WAGPUNKBITS_KIT = "Might be a bit o'silver an' copper hereabouts.",

        WAGSTAFF_MUTATIONS_NOTE = "Arrgh! Get me a quartermaster ta read all landlubbin' blabberin'!",

        -- Meta 3

        BATTLESONG_INSTANT_REVIVE = "It be lookin' like dead men got plenty o'tales t'tell.",

        WATHGRITHR_IMPROVEDHAT = "A landlubber's idea o' \"tricorn\".",
        SPEAR_WATHGRITHR_LIGHTNING = "A sword o'sky bolts t'strike down any fish o'me choosin'!",

        BATTLESONG_CONTAINER = "Me noggin' holds all me shanties!",

        SADDLE_WATHGRITHR = "Fer th'cap'n o'beasts t'sail them hairy seas.",

        WATHGRITHR_SHIELD = "Batten down yer hatches!",

        BATTLESONG_SHADOWALIGNED = "Ol'Woodlegs knows all shanties e'er sung at sea! Even gots some o'me own!",
        BATTLESONG_LUNARALIGNED = "Ol'Woodlegs knows all shanties e'er sung at sea! Even gots some o'me own!",

		SHARKBOI = "Ye won't be digestin' Woodlegs t'day!",
        BOOTLEG = "Be it true? Has th'briny blue returned ye ta ol'Woodlegs?",
        OCEANWHIRLPORTAL = "Avast! Once th'whirlpool's got ye whirlin' thar be no turnin' back!",

		--Shipwrecked Quotes
		FLOTSAM = "Th'remains o'a ship.",
		SUNKEN_BOAT =
		{
			GENERIC = "I'd throw ye a line if me had one.",
            ABANDONED = "Mate o'reboard.",
		},
		SUNKEN_BOAT_BURNT = "Sunk and burnt. Thet's a whole lotta bad luck.",
		
		BOAT_LOGRAFT = "She don't look too sturdy.",
		BOAT_RAFT = "This vessel be an embarrassment!",
		BOAT_ROW = "I ain't rowed since I was a young swab.",
		BOAT_CARGO = "Fer th'hoarder at sea.",
		BOAT_ARMOURED = "She's a tough 'un.",
		BOAT_ENCRUSTED = "A vessel'o th'sea, t'be sure.",
		CAPTAINHAT = "Look smart mateys! Captain on deck!",
		
		BOAT_TORCH = "Fer a torch on me boat, not fer torchin' me boat.",
		BOAT_LANTERN = "Fer night sails.",
		BOATREPAIRKIT = "Never set sail wit'out'er.",
		BOATCANNON = "Ready ta fire six pounders!",
		
		BOTTLELANTERN = "Thet's one use fer a bottle.",
		BIOLUMINESCENCE = "They light up me life.",
		
		BALLPHIN = "These devil's really have a ball.",
        BALLPHINHOUSE = "They be at home on th'sea, jus' like ol'Woodlegs!",
        DORSALFIN = "Th'fin o'a sea beast.",
		TUNACAN = "Fish inna can!",
		
		JELLYFISH = "Shockin'ly squishy.",
		JELLYFISH_DEAD = "Shockin'ly dead.",
        JELLYFISH_COOKED = "Shockin'ly tasty.",
		JELLYFISH_PLANTED = "Shockin'ly dangerous.",
		JELLYJERKY = "Who be knowin' such a thing be possible?",
        RAINBOWJELLYFISH = "Aye, thet b'a jellyfish.",
        RAINBOWJELLYFISH_DEAD = "'Tis gon' belly up!",
        RAINBOWJELLYFISH_COOKED = "Now Woodlegs' belly'll soon b'full'o colors!",
        RAINBOWJELLYFISH_PLANTED = "Th'sea b'full'o colors!",
		JELLYOPOP = "Woodlegs' specialty!",
		
        CROCODOG = "A mangy reptile!",
        POISONCROCODOG = "A mighty poison infests thet one!",
        WATERCROCODOG = "A terr'ble fido, t'be sure!",
		
        PURPLE_GROUPER = "Ye got thet look t'ya.",
        PIERROT_FISH = "Don't ye b'starin' at ol'Woodlegs!",
        NEON_QUATTRO = "Inta th'pot w'ya.",
        PURPLE_GROUPER_COOKED = "Ready fer m'gullet.",
        PIERROT_FISH_COOKED = "Down th'hatch!",
        NEON_QUATTRO_COOKED = "Th'bread'n'butter'o'th'sea!",
        TROPICALBOUILLABAISSE = "Tis a'hearty fish soup!",
		
        FISH_FARM = 
        {
            EMPTY = "Nary a fish in sight!",
            STOCKED = "Th'fish b'growin', aye.",
            ONEFISH = "Tharr be a bite!",
            TWOFISH = "More fish fer ol'Woodlegs!",
            REDFISH = "They b'ready fer a cookin'!",
            BLUEFISH  = "Thet be a netful!",
        },
		
        ROE = "Egg'o'th'fish.",
        ROE_COOKED = "Roast egg'o'th'fish.",
        CAVIAR = "Snooty landlubb'rin' eats!",
		
        CORMORANT = "A most foul seabird!",
        SEAGULL = "A rat wit'win's.",
        SEAGULL_WATER = "Ahoy, seafarer!",
        TOUCAN = "Yer name wouldn't be Sam, would it?",
        PARROT = "Ye's a smart pretty bird.",
        PARROT_PIRATE = "Ack! Thet's me Polly!",
		
        SEA_YARD = 
        {
            ON = "Thet'll give Sea Legs a leg up! Harr!",
            OFF = "Who'll take care'a ol'Sea Legs now?",
            LOWFUEL = "Thar be near no wind in them sails!",
        },
		
        SEA_CHIMINEA =
        {
            EMBERS = "Thet needs a stokin'.",
            GENERIC = "Prote'tion frum th'waves.",
            HIGH = "Burnin' bright, aye!",
            LOW = "Tis a wee bit low.",
            NORMAL = "Th'bitter cold'll not get Woodlegs, na!",
            OUT = "It be out... Like a light!",
        }, 
		
        CHIMINEA = "Keeps th'wind off a good fire.",
		
        TAR_EXTRACTOR =
        {
            ON = "Tis workin'up a mighty sweat!",
            OFF = "Ain't doin' nuthin'!",
            LOWFUEL = "Thet b'lookin' mor'n a tad low.",
        },
		
        TAR = "Tis a m'ghty sticky sludge!",
        TAR_TRAP = "Ol' Woodlegs' pegs'd get stuck f'sure!",
        TAR_POOL = "There be sludgy treasure down ther'.",
        TARLAMP = "Looks like a wee boat!",
        TARSUIT = "Y'arr, cov'r me in tarr!",
		
        PIRATIHATITATOR =
        {
            BURNT = "What a shame.",
            GENERIC = "She looks great in thet hat.",
        },
		
		PIRATEHAT = "Fit for a cutthroat scallywag. Or me.",
		
        MUSSEL_FARM =
        {
            GENERIC = "Scoop 'em up!",
            STICKPLANTED = "Waitin' fer food don't be me strong suit...",
        },

        MUSSEL = "Woodlegs loves a mussel!",
        MUSSEL_COOKED = "Yarrr, thet's good.",
        MUSSELBOUILLABAISE = "'Tis a delicacy.",
        MUSSEL_BED = "Ye'll b'returned t'th'sea.",	
        MUSSEL_STICK = "Fer lurin' mussels into me belly.",
		
        LOBSTER = "Don't be scurryin' from me!",
        LOBSTER_DEAD = "Aye! Now Woodlegs can eat 'em.",
        LOBSTER_DEAD_COOKED = "Hot meat o'th'sea!",
        LOBSTERHOLE = "'Tis th'shellbeast's cabin.",
		WOBSTERBISQUE = "Fends off th'scurvy!",
        WOBSTERDINNER = "I loves ta eat 'em!",
        SEATRAP = "Tis' a pirate's lunch fer me!",
		
        BUSH_VINE =
        {
            BURNING = "Burnin'.",
            BURNT = "Thet's one way ta solve a knot.",
            CHOPPED = "Thet straightened ya out.",
            GENERIC = "Ya look like a tangled pile o'riggin'.",
        },
        VINE = "Them's be th'stranglin' kind.",
        DUG_BUSH_VINE = "Thet be mine fer th'takin'.",
		
        ROCK_LIMPET =
        {
            GENERIC = "Thet rock is covered in good eatin'.",
            PICKED = "'Tis just a rock now.",
        },
		
        LIMPETS = "Ye'll be in me tummy soon 'nuff.",
        LIMPETS_COOKED = "Yaaarm!",
        BISQUE = "A delightful snack t'pour down m'gullet!",
		
        MACHETE = "Ye be slicin'!",
        GOLDENMACHETE = "'Tis a mighty 'eavy blade.",
		
        THATCHPACK = "Thatch th'ticket!",
        PIRATEPACK = "Me thought me lost thet!",
        SEASACK = "Me travels moist'ly hereout.",
		
		SEAWEED_PLANTED =
        {
            GENERIC = "Gets tangled in me rigging.",
            PICKED = "'Urry up an' grow back.",
        },
		
        SEAWEED = "Get's tangled in my rigging.",
        SEAWEED_COOKED = "Cooking it don't make it food.",
        SEAWEED_DRIED = "This s'pose ta pass fer jerky?",
        SEAWEED_STALK = "Fine bit o' tha sea veg.",
		
        DUBLOON = "Me treasures!",
        SLOTMACHINE = "I've always thoughta myself a bettin' man.",
		
        SOLOFISH = "Ye salty dog!",
        SOLOFISH_DEAD = "'Tis a seadog n'more.",
        SWORDFISH = "Arrr ye'd like ta bury thet nose in ol'Woodlegs eh?",
        SWORDFISH_DEAD = "Ye put up a good fight.",
        CUTLASS = "Aye, 'tis a true pirate's blade.",
		
        SUNKEN_BOAT_TRINKET_1 = "A captain worth his salt don't need this.",
        SUNKEN_BOAT_TRINKET_2 = "Fer a pirate's tyke.",
        SUNKEN_BOAT_TRINKET_3 = "Candles and water don't mix.",
        SUNKEN_BOAT_TRINKET_4 = "Bah! Hokum!",
        SUNKEN_BOAT_TRINKET_5 = "I already got one good boot.",
        TRINKET_IA_13 = "Sodey pop!",
        TRINKET_IA_14 = "Me don't tangle wit' witchcraft.",
        TRINKET_IA_15 = "Can't play a note.",
        TRINKET_IA_16 = "A strange plate b'this.",
        TRINKET_IA_17 = "Boot-y!",
        TRINKET_IA_18 = "Me don't have a taste fer delicate antiquities.",
        TRINKET_IA_19 = "Fer brain storms.",
        TRINKET_IA_20 = "Wit' this me'll traverse th'seas!",
        TRINKET_IA_21 = "'Tis a mockery o'th'mighty vessels o'th'sea!",
        TRINKET_IA_22 = "Where be th'wine?",
        TRINKET_IA_23 = "Even if it weren't broken, me wouldn't be knowing whut ta do wit' it.",
        EARRING = "Ay! 'Tis me ol'earring!",
		
		TURF_BEACH = "Me ain't no ground lubber.",
		TURF_JUNGLE = "Me ain't no ground lubber.",
		TURF_MAGMAFIELD = "Me ain't no ground lubber.",
		TURF_TIDALMARSH = "Me ain't no ground lubber.",
		TURF_ASH = "Me ain't no ground lubber.",
		TURF_MEADOW = "Me ain't no ground lubber.",
		TURF_VOLCANO = "Th'devil's ground.",
		TURF_SNAKESKIN = "Me ain't no ground lubber.",
		
        WHALE_BLUE = "Arrr ye th'devil ta take Woodlegs ta th'depths?",
        WHALE_CARCASS_BLUE = "Why don't ye sink and clear th'air!",
        WHALE_WHITE = "Now ye arr a devil fit ta claim ol'Woodlegs!",
        WHALE_CARCASS_WHITE = "Thet's a whale o'a smell!",
        WHALE_TRACK = "Whale, ahoy!",
		WHALE_BUBBLES = "Thar be a devil'n'th'depths!",
        BLUBBERSUIT = "Ye has gotta be kiddin' me.",
        BLUBBER = "Woodlegs'd never be caught blubberin'.",
        HARPOON = "T'whales cow'r in fear 'fore ol'Woodlegs!",
		
        SAIL_PALMLEAF = "Fer catchin' th'blow.",
        SAIL_CLOTH = "Fancy sail, thet.",
        SAIL_SNAKESKIN = "Thet be a sail.",
        SAIL_FEATHER = "'Tis light as a feather.",
        IRONWIND = "Woodlegs prefers 'is sail.",
		
        BERMUDATRIANGLE = "Whar they lead is anybody's guess.",
		
        PACKIM_FISHBONE = "Th'heart o'me bird. Inna manner o'speakin'.",
        PACKIM = "Thet's me mate!",
		
        TIGERSHARK = "Come at me ye striped devil!",
        MYSTERYMEAT = "Aye, me'll be eatin' well tonight!",
        SHARK_GILLS = "Ye won't be breathin' through these namore!",
        TIGEREYE = "Reminds me o'me ol'first mate, Wildeye Weston.",
        DOUBLE_UMBRELLAHAT = "'Tis th'work of a madman.",
        SHARKITTEN = "Don't ye look at me wit' those adorable eyes...",
        SHARKITTENSPAWNER =
        {
            GENERIC = "Th'kitties sharpen their claws here.",
            INACTIVE = "Here kitty-kitty?",
        },
		
        WOODLEGS_KEY1 = "A useless thing if ever there were one.",
        WOODLEGS_KEY2 = "A useless thing if ever there were one.",
        WOODLEGS_KEY3 = "A useless thing if ever there were one.",
        WOODLEGS_CAGE = "'Tis a wretched thing.",
		
        CORAL = "This'll tear up a boat right quick!",
        ROCK_CORAL = "Ye better steer clear, Woodlegs.",
        LIMESTONENUGGET = "No good fer ship buildin'.",
        NUBBIN = "N'ta hair t'be seen.",
        CORALLARVE = "Not th'most intimidatin' sea monster!",
        WALL_LIMESTONE = "Bah! Walls!",
        WALL_LIMESTONE_ITEM = "Walls make me feel cagey.",
        WALL_ENFORCEDLIMESTONE = "Wall'o'th'sea!",
        WALL_ENFORCEDLIMESTONE_ITEM = "A wall t'be.",
        ARMORLIMESTONE = "This'd sink like a stone on th'sea.",
        CORAL_BRAIN_ROCK = "Th'brains o'th'ocean.",
        CORAL_BRAIN = "Ye can't think yer way outta me pocket.",
        BRAINJELLYHAT = "A jelly ta rest upon me dainty head.",
		
        SEASHELL = "Me hears th'blue.",
        SEASHELL_BEACHED = "Sea trinket.",
        ARMORSEASHELL = "Me mateys would laugh at me...",
		
        ARMOR_LIFEJACKET = "Bah! For swabs only!",
        ARMOR_WINDBREAKER = "Ol'Woodlegs be an expert windbreaker!",
		
        SNAKE = "Ya split-tongued devil!",
        SNAKE_POISON = "Stay away ye forked tongued devil!",
        SNAKESKIN = "Me leathers do need replacin'.",
        SNAKEOIL = "Me knows a good deal when me sees it.",
        SNAKESKINHAT = "On me head? Wear thet?",
        ARMOR_SNAKESKIN = "Leathers ta slick th'rain away.",
        SNAKEDEN =
        {
            BURNING = "Burnin'.",
            BURNT = "Thet's one way ta solve a knot.",
            CHOPPED = "Thet straightened ya out.",
            GENERIC = "Ya look like a tangled pile o'riggin'.",
        },
		
        OBSIDIANFIREPIT =
        {
            EMBERS = "'Tis nearly burnt out.",
            GENERIC = "Need ta be careful ta not burn me legs.",
            HIGH = "'Tis a mercy thet me ship is ney nearby.",
            LOW = "'Tis goin' ta need some fuel soon.",
            NORMAL = "Hot by th'fire.",
            OUT = "Th'fire has left.",
        },
		
        OBSIDIAN = "A powerful stone.", -- Obsidian item
        ROCK_OBSIDIAN = "Fire rock from fire mountain.", -- Obsidian boulder
        OBSIDIAN_WORKBENCH = "Th'legends be true...",
        OBSIDIANAXE = "Keep thet away from me legs!",
        OBSIDIANMACHETE = "Thet thing gets hot!",
        SPEAR_OBSIDIAN = "Poke ye wit' fire!",
        VOLCANOSTAFF = "Ye'll do me biddin' now, 'cano!",
        ARMOROBSIDIAN = "It be heavy 'n hot.",
        COCONADE =
        {
            BURNING = "Surprise! 'Tis explodin' fruit!",
            GENERIC = "Packs a fruit punch!",
        },
		
		OBSIDIANCOCONADE =
		{
            BURNING = "Surprise!",
			GENERIC = "Ka-boom!",
		},
		
        VOLCANO_ALTAR =
        {
            GENERIC = "'Tis be whar ye bribe th'cano.",
            OPEN = "Ready fer bribin'.",
        },
		
        VOLCANO = "Me ol'prison.",
        VOLCANO_EXIT = "'Tis nigh time me be leavin' this dread place.",
        ROCK_CHARCOAL = "Could start quite a barby-q wit' this.", -- Charcoal boulder (Volcano biome)
        VOLCANO_SHRUB = "Ye look burnt out.",
        LAVAPOOL = "Hope me legs don't catch.",
        COFFEEBUSH =
        {
            BARREN = "It needs a spot o'poop.",
            WITHERED = "It needs a mug o'java ta perk up.",
            GENERIC = "A crop after me bitter, black heart!",
            PICKED = "I hope they be comin' back.",
        },
		
        COFFEEBEANS = "Just add water.",
        COFFEEBEANS_COOKED = "No hot water?",
        DUG_COFFEEBUSH = "Thet will be needin' some ground ta be useful.",
        COFFEE = "Arrr thet be th'real treasure.",
		
		--[[ELEPHANTCACTUS =
		{
			BARREN = "It could use some ash, but I better stand back afterward.",
			WITHERED = "It could use some ash, but I better stand back afterward.",
			GENERIC = "Yikes! I could poke an eye out!",
			PICKED = "It's safe to approach for now.",
		},]]

        ELEPHANTCACTUS = "Woodlegs be wary.",	
        DUG_ELEPHANTCACTUS = "Thet will be needin' some ground ta be useful.",
        ELEPHANTCACTUS_ACTIVE = "'Tis a dangerous plant!",
        ELEPHANTCACTUS_STUMP = "Th'fiendish plant is gatherin' its energy ta attack again!",	
        NEEDLESPEAR = "A dagger o'sorts.",
        ARMORCACTUS = "Me'd like ta see them try ta hit Woodlegs now!",
		
        TWISTER = "Thet wind is whippin' like th'devil's sneeze!",
        TWISTER_SEAL = "Don't ye be lookin' at me wit' them sad eyes.",
        TURBINE_BLADES = "Methinks thar's a use fer this.",
        MAGIC_SEAL = "Whut th'devil is thet?",
        WIND_CONCH = "Brings th'storm down.",
        WINDSTAFF = "Keeps th'wind at me back.",
		
        DRAGOON = "Keep yer flames away from Woodlegs, landlubber!",
        DRAGOONHEART = "Th'heart o'th'fearsome creature.",
        DRAGOONSPIT = "How uncouth! Woodlegs approves!",
        DRAGOONEGG = "Me feels a storm comin'.",
        DRAGOONDEN = "D'ya think 'tis full o'treasure?",
		
        ICEMAKER =
        {
            HIGH = "'Tis chuggin' along.",
            LOW = "'Tis sputterin'.",
            NORMAL = "'Tis runnin' fine.",
            OUT = "'Tis outta fuel.",
            VERYLOW = "'Tis chokin'!",
        },
		
        HAIL_ICE = "Me timbers is shiverin'!",
		
        BAMBOOTREE =
        {
            BURNING = "Burnin'.",
            BURNT = "Thet's th'end o'thet.",
            CHOPPED = "Thar be no bamboo fer th'moment.",
            GENERIC = "Ye sproutin' wood.",
        },
		
        BAMBOO = "Ye'd be too big ta pick me teeth wit'. If me had teeth...",
        FABRIC = "Fabric fer a sail!",
        DUG_BAMBOOTREE = "Thet will be needin' some ground ta be useful.",
		
        JUNGLETREE =
        {
            BURNING = "She burns bright.",
            BURNT = "Be smellin' like a bag o'coal.",
            CHOPPED = "Nary a wiff o'treasure left.",
            GENERIC = "Thet tree be hidin' all kinds o'treasures.",
        },
		
        JUNGLETREESEED = "Ye might grow in ta a new leg fer me.",
        JUNGLETREESEED_SAPLING = "Meybe sev'ral legs!",
        LIVINGJUNGLETREE = "Thet tree be givin' Woodlegs th'evil eye.",
		
        OX = "She could use a pirate hug.",
        BABYOX = "'Tis lubbable how ye don't lub th'land.",
        OX_HORN = "A terrible beastly horn.",
        OXHAT = "'Tis a hat o'th'ox.",
        OX_FLUTE = "A flute ta play a lil diddy.",
		
        MOSQUITO_POISON = "Keep back ye maggot!",
        MOSQUITOSACK_YELLOW = "T'was a yellowbellied beast!", -- Yellow Mosquito Sack (from Poison Mosquito)
		
        STUNGRAY = "Back ye foul stink-shooters!",
        POISONHOLE = "Th'worst kind o'hole.",
        GASHAT = "Smells o'freshness when me wears it.",
		
        ANTIVENOM = "'Tis th'sea thet'll claim me, not some sickly biter.",
        VENOMGLAND = "Aye, 'tis th'source o'th'venom.",
		POISONBALM = "Aye, that'll take th' sting out.",
		
        SPEAR_POISON = "A coward's weapon.",
        BLOWDART_POISON = "A swab's weapon.",
		
        SHARX = "Sea devils!",
        SHARK_FIN = "Ye left yer rudder, devil!",
        SHARKFINSOUP = "Now Woodlegs gits ta bite ya back!",
        SHARK_TEETHHAT = "Madness ta put th'devil's knives on me head.",
        AERODYNAMICHAT = "Cuts me walkin' time in half!",
		
        IA_MESSAGEBOTTLE = "Oh ho! Be it a map?",
        IA_MESSAGEBOTTLEEMPTY = "She's empty.",
        BURIEDTREASURE = "Me favorite kind of buried thing!", -- X Marks the Spot
		
        SAND = "Handful o'land.",
        SANDDUNE = "Nothin' like th'feel o'sand 'neath me pegs.",
        SANDBAGSMALL = "Fer keepin' th'flood waters at bay.",
        SANDBAGSMALL_ITEM = "Woodlegs'd never lug th'land with'im!",
        SANDCASTLE =
        {
            GENERIC = "I be a sandcastle master builder!",
            SAND = "No pirate can make a better sandcastle than ol'Woodlegs!",
        },
		
        SUPERTELESCOPE = "Eye see clear ta th'horizon!",
        TELESCOPE = "Aye eye!",
		
        DOYDOY = "'Tis a stupid lookin bird.",
        DOYDOYBABY = "'Tis a tiny dumb bird.",
        DOYDOYEGG = "'Tis a free meal!",
        DOYDOYEGG_CRACKED = "'Tis useless.",
        DOYDOYFEATHER = "'Tis a bird sail.",
        DOYDOYNEST = "'Tis a bird home.",
        TROPICALFAN = "Beat th'heat.",
		
        PALMTREE =
        {
            BURNING = "She burns bright.",
            BURNT = "Be smellin' like a bag o'coal.",
            CHOPPED = "Nary a wiff o'treasure left.",
            GENERIC = "A nice bit o'shade thar.",
        },

        COCONUT = "Aye, me favorite tree fruit.",
        COCONUT_HALVED = "Its flesh be prone fer th'takin'.",
        COCONUT_SAPLING = "Tis a fruit no more!",
        COCONUT_COOKED = "'Tis even better cooked o'er a fire.",
        PALMLEAF = "Whut can I make wit' this here?",
        PALMLEAF_UMBRELLA = "I's a pirate wit' an umbrella.",
        PALMLEAF_HUT = "Needs a hammock!",
        LEIF_PALM = "Yarr, wit' those tree legs we're pract'ly kin!",
		
        CRAB =
        {
            GENERIC = "Thar's a tasty meal!",
            HIDDEN = "Me knowin' thet crabbit be nearby.",
        },
		
        CRABHOLE = "Ye got a nice 'ome thar, crabbit.",
		
        TRAWLNETDROPPED =
        {
            GENERIC = "Yield yer haul!",
            SOON = "Yer haul be mine soon.",
            SOONISH = "Give up yer haul.",
        },
		
        TRAWLNET = "Fer trawl'n around.",
        IA_TRIDENT = "Me thought this was just from th'stories!",
		
        KRAKEN = "'Tis th'mightiest beast in th'sea!",
        KRAKENCHEST = "Aye, this be what Woodlegs lives fer!",
        KRAKEN_TENTACLE = "Have at ye!",
        QUACKENBEAK = "Twas a mighty maw on thet one.",
        QUACKENDRILL = "Thar b'a drill.",
        QUACKERINGRAM = "Tis time fer a quackerin'!",
		
        MAGMAROCK = "Could be something buried thar.",
        MAGMAROCK_GOLD = "Golden rocks!",
        FLAMEGEYSER = "Woodlegs must be standin' on a sea o'fire.",
		
        TELEPORTATO_SW_RING = "I'd give it a bite, but I know it don't be gold.",
        TELEPORTATO_SW_BOX = "Thar be secrets hidden in this here box.",
        TELEPORTATO_SW_CRANK = "Aye, Woodlegs'll give it a crank.",
        TELEPORTATO_SW_POTATO = "Whut a potatey needs a helmet fer me never be knowin'.",
        TELEPORTATO_SW_BASE = "What a curious piece.",
		
        PRIMEAPE = "Like lookin' in a cracked, greasy mirror.",
        PRIMEAPEBARREL = "They ain't much fer housekeepin'.",
        MONKEYBALL = "This be unsettlin'.",
        WILBUR_CROWN = "Kinda head fits this crown?", --unused
        WILBUR_UNLOCK = "Look at this fancy monkey!", --unused
		
        MERMFISHER = "At least'e be catchin'me fish!",
        MERMHOUSE_FISHER = "They smell worse than Woodlegs!",
		
        OCTOPUSKING = "Ahoy, ye salty dog!",
        OCTOPUSCHEST = "Whut's in thar fer ol'Woodlegs?",
		
        SWEET_POTATO = "Me likes me potateys sweet!",
        SWEET_POTATO_COOKED = "Hot grub!",
        SWEET_POTATO_PLANTED = "A different kind o'buried treasure.",
        SWEET_POTATO_SEEDS = "Seeds be fer planting, an Woodlegs ain't no farmer.",
		SWEET_POTATO_OVERSIZED = "'Tis sweet hot grub fer days!",
		SWEETPOTATOSOUFFLE = "A tasty piece o'crumpet.",

		----------------------- WOODLEGS SPECIFIC ITEMS ------------------------

		WOODLEGSBOAT = "Me ship. She's me Sea Legs.", -- Eh? -crimeraaa
        BOAT_WOODLEGS = "Me ship. She's me Sea Legs.",
        WOODLEGSHAT = "'Tis me lucky hat!", -- Lucky Hat
        SAIL_WOODLEGS = "Thet's me sail!",
		
        PEG_LEG = "Woodlegs had better hold on ta this fer future use.",
        PIRATEGHOST = "'Tis a pirate's life, even in death.",
		
        WILDBORE = "Not wild about ye.",
        WILDBOREHEAD = "Quit yer starin'.",
        WILDBOREHOUSE = "Bet them bores be knowin' how ta parrrrrty.",
		
        MANGROVETREE = "Th'waters be shallow 'ere.",
        MANGROVETREE_BURNT = "'Tis cooked.",
		
        PORTAL_SHIPWRECKED = "Looks a delightful lil ride!",
        SHIPWRECKED_ENTRANCE = "Ahoy me mateys!",
        SHIPWRECKED_EXIT = "Farewell ye land lubbers!",
		
        TIDALPOOL = "I sees me future...",
        FISHINHOLE = "A fishin'ole! Make a note on me map!",
        FISH_TROPICAL = "Aye, me favorite food!",
        TIDAL_PLANT = "Leafy greens from high tide.",
        MARSH_PLANT_TROPICAL = "Green like me mermaid's eyes.",
		
        FLUP = "Git back in th'ground!",
        BLOWDART_FLUP = "Flup you.",
		
        SEA_LAB = "Thet b'fer tinkerin' on th'waves!",
        BUOY = "Buoy am me glad ta see ya.",
        WATERCHEST = "Woodlegs' treasure, buried at sea!",
		
        LUGGAGECHEST = "Thar might be gold inside.",
        WATERYGRAVE = "Th'only proper end fer a man o'th'sea.",
        SHIPWRECK = "Rest well, ye weary seafarer.",
        BARREL_GUNPOWDER = "Skull 'n crossbones only means one thing whur I come from.",
        RAWLING = "Reminds me o'me old parrot.",
        GRASS_WATER = "Ye must 'ave a 'uge thirst!", -- Mangrove grass (slightly submerged)
        KNIGHTBOAT = "Th'navy is'ere!",
		
        DEPLETED_BAMBOOTREE = "'Tis no more.",
        DEPLETED_BUSH_VINE = "Th'bush needs some shuteye.",
        DEPLETED_GRASS_WATER = "Thet'll not be comin' back fer some time.",
		
        WALLYINTRO_DEBRIS = "Th'sea swallowed me boat but spit up ol'Woodlegs.",
        BOOK_METEOR = "Devil magic thet summons demons from th'sky!",
        CRATE = "There be a crate.",
        SPEAR_LAUNCHER = "Load a spear an'let'er fly!",
		MUTATOR_TROPICAL_SPIDER_WARRIOR = "Keep yer biscuits ta yerselves!",
		
		--SWC
		BOAT_SURFBOARD = "An' it be barely floatin'! Bah!",
		SURFBOARD_ITEM = "Thet not be a proper boat!",
	},

	DESCRIBE_GENERIC = "'Tis a... somethin'er tother.",
	DESCRIBE_SMOLDERING = "'Tis like ta catch fire.",
	DESCRIBE_TOODARK = "'Tis too dark ta spy wit' ye eye!",

	DESCRIBE_PLANTHAPPY = "'E's a happy'un!",
    DESCRIBE_PLANTVERYSTRESSED = "Ye learn ta take th'hits on me deck, matey!",
    DESCRIBE_PLANTSTRESSED = "Yer only cranky cus yer angry!",
    DESCRIBE_PLANTSTRESSORKILLJOYS = "Back, back foul weeds!",
    DESCRIBE_PLANTSTRESSORFAMILY = "Aye, yer ne'er alone when yer at sea.",
    DESCRIBE_PLANTSTRESSOROVERCROWDING = "This lubber ship's got too many workin' mates!",
    DESCRIBE_PLANTSTRESSORSEASON = "It be out o'season.",
    DESCRIBE_PLANTSTRESSORMOISTURE = "Ye drinkin' th'salt water again, lubber?",
    DESCRIBE_PLANTSTRESSORNUTRIENTS = "Lookin' like ye ran outta rations.",
    DESCRIBE_PLANTSTRESSORHAPPINESS = "Let me tell ye th'story o'me'ol'bones!",

	EAT_FOOD =
	{
		TALLBIRDEGG_CRACKED = "Inta' ma belly wit ya!",
		WINTERSFEASTFUEL = "Yo-Ho-Ho!",
	},
}
