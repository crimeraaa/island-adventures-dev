PrefabFiles = {
    "antivenom",
    "ashfx",
    "babyox",
    "ballphin",
    "ballphin_spawner",
    "ballphinhouse",
    "ballphinpod",
    "bamboo",
    "barrel_gunpowder",
    "bermudatriangle",
    "berrybush2_snake",
    "bioluminescence",
    "bioluminescence_spawner",
    "blubber",
    "boatcannon",
    "boatcontainer_classified",
    "boatlamps",
    "boatrepairkit",
    "boats",
    "bottlelantern",
    "buoy",
    "buriedtreasure",
    "cannonshot",
    "chess_navy_spawner",
    "chiminea",
    "chimineafire",
    "coconade",
    "coconut",
    "coffeebush",
    "coral",
    "coral_brain",
    "coral_brain_rock",
    "corallarve",
    "crab",
    "crabhole",
    "crate",
    "critterlab_water",
    "crocodog",
    "crocodogwarning",
    "cutlass",
    "dorsalfin",
    "doydoy",
    "doydoyegg",
    "doydoyfeather",
    "doydoynest",
    "dragoon",
    "dragoonheart",
    "dragoonspit",
    "dragoonden",
    "dragoonfire",
    "dragoonegg",
    "dubloon",
    "earring",
    "edgefog",
    "elephantcactus",
    "fabric",
    "fish_med",
    "fish_tropical",
    "fishfarm",
    "fishfarm_sign",
    "fishinhole",
    "flamegeyser",
    "flood",
    "flotsam",
    "flotsam_forest",
    "flup",
    "flupspawner",
    "grass_water",
    "guano_wilbur",
    "hail",
    "haildrop",
    "hail_ice",
    "harpoon",
    "ia_armor",
    "ia_birds",
    "ia_blowdart",
    "ia_books",
    "ia_chests",
    "ia_chesspieces",
    "ia_fertilizer_nutrient_defs",
    "ia_hats",
    "ia_marsh_plant",
    "ia_messagebottle",
    "ia_monsterwarningsounds",
    "ia_oceanshadowcreature",
    "ia_sketch",
    "ia_spoiledfood",
    "ia_placeholder", --exactly what it says on the tin: a temporary file for registering undefined prefabs for worldgen
    "ia_plantables",
    "ia_pondfish",
    "ia_preparedfoods",
    "ia_skinprefabs",
    "ia_spear",
    "ia_staffs",
    "ia_trident",
    "ia_trinkets",
    "ia_veggies",
    "ia_walls",
    "ia_winter_ornaments",
    "ia_wintertree",
    "icemaker",
    "inv_bamboo",
    "inv_vine",
    "jellyfish",
    "jellyfish_planted",
    "jellyfish_spawner",
    "jungletrees",
    "jungletreeseed",
    "knightboat",
    "knightboat_cannon",
    "kraken",
    "kraken_projectile",
    "kraken_tentacle",
    "kraken_laser",
    "leif_tropical",
    "limestonenugget",
    "limpets",
    "livingjungletree",
    "lobster",
    "lobsterhole",
    "machete",
    "magic_seal",
    "magma_rocks",
    "mangrove",
    "mermfisher",
    "mermhouse_fisher",
    "mermhouse_tropical",
    "monkeyball",
    "mosquito_poison",
    "mosquitosack_yellow",
    "mussel",
    "mussel_farm",
    "mussel_stick",
    "mysterymeat",
    "obsidian",
    "obsidian_workbench",
    "obsidianaxe",
    "obsidianfirefire",
    "obsidianfirepit",
    "obsidiantoollight",
    "oceanfog",
    "octopusking",
    "ox",
    "ox_flute",
    "ox_horn",
    "oxherd",
    "packim_fishbone",
    "packim",
    "palmleaf",
    "palmleaf_umbrella",
    "palmleafhut",
    "palmtrees",
    "pandoraschest_tropical",
    "pirateghost",
    "piratepack",
    "piratihatitator",
    "poisonbalm",
    "poisonbubble",
    "poisonhole",
    "poisonmistarea",
    "poisonmistparticle",
    -- "portablecookpot",
    "portal_shipwrecked",
    "primeape",
    "primeapebarrel",
    "puddle",
    "quackenbeak",
    "quackendrill",
    "quackeringram",
    "quackering_wave",
    "quackering_wake",
    "rainbowjellyfish",
    "rainbowjellyfish_planted",
    "rainbowjellyfish_spawner",
    "rainbowjellyfishlight",
    "rawling",
    "rock_coral",
    "rock_limpet",
    "rock_obsidian",
    "roe",
    "rowboat_wake",
    "sail",
    "sand",
    -- "sandbag", --Not used, does not work, neither do the animations, just don't use it! -M
    "sandbagsmall",
    "sandcastle",
    "sanddune",
    "sea_chiminea",
    "sea_lab",
    "sea_yard",
    "sea_yard_arms_fx",
    "seagullspawner",
    "seasack",
    "seashell",
    "seashell_beached",
    "seatrap",
    "seaweed",
    "seaweed_planted",
    "shark_fin",
    "shark_gills",
    "sharkitten",
    "sharkittenspawner",
    "sharx",
    "shipwreck",
    "shipwrecked_entrance",
    "slotmachine",
    "snake",
    "snakeden",
    "snakeoil",
    "snakeskin",
    "solofish",
    "solofish_spawner",
    "spearlauncher",
    "stungray",
    "stungray_spawner",
    "sunken_boat",
    "sunkenprefab",
    "sweet_potato",
    "swordfish",
    "swordfish_spawner",
    "tar",
    "tar_extractor",
    "tar_pool",
    "tarlamp",
    "telescope",
    "terraformstaff", -- debug item
    "thatchpack",
    "tidalpool",
    "tigereye",
    "tigershark",
    "tigersharkshadow",
    "trawlnet",
    "treeguard_cannon",
    "tropicalfan",
    "tropical_spider_warrior",
    "tropical_spider_mutators",
    "tunacan",
    "turbine_blades",
    "twister",
    "twister_seal",
    "venomgland",
    "vine",
    "volcano",
    "volcano_altar",
    "volcano_exit",
    "volcano_shrub",
    "volcanolavafx",

    "firerain",
    "lavapool",
    "meteor_impact",
    "walani",
    "walani_none",
    "wallyintro",
    -- "warly",
    -- "warly_none",
    "waterygrave",
    "ia_wave",
    "ia_wave_shimmer",
    "ia_wave_shore",
    "whale",
    "whale_bubbles",
    "whale_carcass",
    "wilbur",
    "wilbur_none",
    "wildbore",
    "wildborehead",
    "wildborehouse",
    "wind_conch",
    "woodlegs",
    "woodlegs_none"
}

Assets = {
    -- Asset("SOUND", "sound/ia_hurricane.fsb"),
    -- Asset("SOUND", "sound/ia_rain.fsb"),
    -- Asset("SOUND", "sound/ia_green.fsb"),
    -- Asset("SOUND", "sound/ia_mild.fsb"),
    -- Asset("SOUND", "sound/ia_wet.fsb"),
    -- Asset("SOUND", "sound/ia_dry.fsb"),
    -- Asset("SOUND", "sound/ia_voice.fsb"),
    -- Asset("SOUND", "sound/ia_sfx.fsb"),
    -- Asset("SOUND", "sound/ia_music.fsb"),
    -- Asset("SOUNDPACKAGE", "sound/ia.fev"),

    ------------------------------------------------------------

    Asset("IMAGE", "images/colour_cubes/sw_mild_day_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/SW_mild_dusk_cc.tex"),
    -- Asset("IMAGE", "images/colour_cubes/SW_mild_night_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/SW_wet_day_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/SW_wet_dusk_cc.tex"),
    -- Asset("IMAGE", "images/colour_cubes/SW_wet_night_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/sw_green_day_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/sw_green_dusk_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/SW_dry_day_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/SW_dry_dusk_cc.tex"),
    -- Asset("IMAGE", "images/colour_cubes/SW_dry_night_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/sw_volcano_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/sw_volcano_active_cc.tex"),

    Asset("ANIM", "anim/poison_meter_overlay.zip"),
    Asset("ANIM", "anim/trawlnet_meter.zip"),
    Asset("ANIM", "anim/obsidian_tool_meter.zip"),

    Asset("ATLAS", "images/overlays/fx3.xml"),
    Asset("IMAGE", "images/overlays/fx3.tex"),

    Asset("IMAGE", "images/ia_cookbook.tex"),
    Asset("ATLAS", "images/ia_cookbook.xml"),

    Asset("IMAGE", "images/ia_scrapbook.tex"),
	Asset("ATLAS", "images/ia_scrapbook.xml"),

    Asset("IMAGE", "images/ia_inventoryimages.tex"),
    Asset("ATLAS", "images/ia_inventoryimages.xml"),
    Asset("IMAGE", "images/hud/ia_hud.tex"),
    Asset("ATLAS", "images/hud/ia_hud.xml"),
    Asset("IMAGE", "images/hud/hud_shipwrecked.tex"),
    Asset("ATLAS", "images/hud/hud_shipwrecked.xml"),

    Asset("ATLAS", "images/minimap/ia_minimap.xml"),
    Asset("IMAGE", "images/minimap/ia_minimap.tex"),

    Asset("IMAGE", "images/volcano_waves/lava_active.tex"),
    Asset("IMAGE", "images/volcano_waves/volcano_cloud.tex"),

    Asset("ATLAS", "images/ia_skins.xml"),
    Asset("IMAGE", "images/ia_skins.tex"),
    Asset("ATLAS_BUILD", "images/ia_skins.xml", 256),

    Asset("ANIM", "anim/lava_bubbling.zip"),

    -- Asset("IMAGE", "images/saveslot_portraits/warly.tex"),
    -- Asset("ATLAS", "images/saveslot_portraits/warly.xml"),

    -- Asset("IMAGE", "images/selectscreen_portraits/warly.tex"),
    -- Asset("ATLAS", "images/selectscreen_portraits/warly.xml"),

    -- Asset("IMAGE", "images/selectscreen_portraits/warly_silho.tex"),
    -- Asset("ATLAS", "images/selectscreen_portraits/warly_silho.xml"),

    -- Asset("IMAGE", "bigportraits/warly.tex"),
    -- Asset("ATLAS", "bigportraits/warly.xml"),

    -- Asset("IMAGE", "bigportraits/warly_none.tex"),
    -- Asset("ATLAS", "bigportraits/warly_none.xml"),

    -- Asset("IMAGE", "images/avatars/avatar_warly.tex"),
    -- Asset("ATLAS", "images/avatars/avatar_warly.xml"),

    -- Asset("IMAGE", "images/avatars/avatar_ghost_warly.tex"),
    -- Asset("ATLAS", "images/avatars/avatar_ghost_warly.xml"),

    -- Asset("IMAGE", "images/avatars/self_inspect_warly.tex"),
    -- Asset("ATLAS", "images/avatars/self_inspect_warly.xml"),

    -- Asset("IMAGE", "images/names_warly.tex"),
    -- Asset("ATLAS", "images/names_warly.xml"),

    -- Asset("IMAGE", "images/names_gold_warly.tex"),
    -- Asset("ATLAS", "images/names_gold_warly.xml"),
    --end of character images

    Asset("ANIM", "anim/dragoon_build.zip"),
    Asset("ANIM", "anim/dragoon_basic.zip"),
    Asset("ANIM", "anim/dragoon_actions.zip"),
    Asset("ANIM", "anim/dragoon_den.zip"),
    Asset("ANIM", "anim/dragoon_heart.zip"),
    Asset("ANIM", "anim/lava_vomit.zip"),

    Asset("ANIM", "anim/ia_bird_cage.zip"),  -- fix pirate parrot has no hat in its cage

    Asset("ANIM", "anim/ia_player_actions_twister.zip"),
    Asset("ANIM", "anim/ia_sailing.zip"),
    Asset("ANIM", "anim/ia_sailing_sailfaceless.zip"), --if a player has a skin without a custom sailface use this anim
    Asset("ANIM", "anim/walani_paddle.zip"),
    Asset("ANIM", "anim/ia_jumping.zip"),

    Asset("ANIM", "anim/ripple_build.zip"),

    Asset("ANIM", "anim/werebeaver_boat_death.zip"),
    Asset("ANIM", "anim/player_tornado.zip"),
    Asset("ANIM", "anim/player_boat_death.zip"),
    Asset("ANIM", "anim/swap_paddle.zip"),
    Asset("ANIM", "anim/player_actions_telescope.zip"),
    Asset("ANIM", "anim/player_actions_speargun.zip"),

     --loading this here because wilbur the all knowing needs them
    Asset("ANIM", "anim/swap_poop.zip"),

    --might be a good idea to seperate the scroll anim from the rest -Half
    Asset("ANIM", "anim/player_mount_unique_actions_with_scroll.zip"),
    Asset("ANIM", "anim/player_actions_uniqueitem_with_scroll.zip"),

    Asset("ANIM", "anim/player_portal_shipwrecked.zip"),

    Asset("ANIM", "anim/player_idles_poison.zip"),

    Asset("ANIM", "anim/pocket_scale_weigh_jellyfish.zip"),
    Asset("ANIM", "anim/pocket_scale_weigh_rainbowjellyfish.zip"),

    Asset("ANIM", "anim/shadow_skittish_ocean.zip"),

    Asset("ANIM", "anim/player_actions_hack.zip"),

    Asset("ANIM", "anim/player_mount_actions_speargun.zip"),
    Asset("ANIM", "anim/player_mount_actions_telescope.zip"),
    Asset("ANIM", "anim/player_mount_idles_poison.zip"),

    Asset("ANIM", "anim/swap_sailface.zip"),

    Asset("ANIM", "anim/boat_hud_cargo.zip"),
    Asset("ANIM", "anim/boat_hud_encrusted.zip"),
    Asset("ANIM", "anim/boat_hud_raft.zip"),
    Asset("ANIM", "anim/boat_hud_row.zip"),
    Asset("ANIM", "anim/boat_inspect_cargo.zip"),
    Asset("ANIM", "anim/boat_inspect_encrusted.zip"),
    Asset("ANIM", "anim/boat_inspect_raft.zip"),
    Asset("ANIM", "anim/boat_inspect_row.zip"),

    Asset("ANIM", "anim/boat_health.zip"),

    Asset("ANIM", "anim/sail_visual.zip"),
    Asset("ANIM", "anim/sail_visual_idle.zip"),
    Asset("ANIM", "anim/sail_visual_trawl.zip"),

    --For minisign
    Asset("ATLAS_BUILD", "images/ia_inventoryimages.xml", 256),

    --I am loading these here because the birdcage needs them
    Asset("ANIM", "anim/seagull_build.zip"),
    Asset("ANIM", "anim/parrot_build.zip"),
    Asset("ANIM", "anim/parrot_pirate_build.zip"),
    Asset("ANIM", "anim/toucan_build.zip"),
    Asset("ANIM", "anim/cormorant_build.zip"),
    --I am loading this here because the meatrack needs them
    Asset("ANIM", "anim/meat_rack_food_sw.zip"),
    --I am loading these here because the cookpot needs them
    -- Asset("ANIM", "anim/bananapop.zip"),
    Asset("ANIM", "anim/bisque.zip"),
    -- Asset("ANIM", "anim/californiaroll.zip"),
    Asset("ANIM", "anim/caviar.zip"),
    -- Asset("ANIM", "anim/ceviche.zip"),
    Asset("ANIM", "anim/coffee.zip"),
    -- Asset("ANIM", "anim/freshfruitcrepes.zip"),
    Asset("ANIM", "anim/jellyopop.zip"),
    Asset("ANIM", "anim/wobsterbisque.zip"),
    Asset("ANIM", "anim/wobsterdinner.zip"),
    -- Asset("ANIM", "anim/monstertartare.zip"),
    Asset("ANIM", "anim/musselbouillabaise.zip"),
    -- Asset("ANIM", "anim/seafoodgumbo.zip"),
    Asset("ANIM", "anim/sharkfinsoup.zip"),
    -- Asset("ANIM", "anim/surfnturf.zip"),
    Asset("ANIM", "anim/sweetpotatosouffle.zip"),
    Asset("ANIM", "anim/tropicalbouillabaisse.zip"),
    --I am loading these here because the farm crop needs them
    --This SHOULD be handled in postinit/sim.lua, but apparently not -M
    Asset("ANIM", "anim/sweet_potato.zip"),

    --Dug Turf, doesn't get handled by TileManager
    Asset("ANIM", "anim/turf_ia.zip"),

    --Variant animations
    --could be loaded lazy once their vanilla counterpart loads, but I hope this way is not too bad either. -M
    Asset("ANIM", "anim/krampus_hawaiian_build.zip"),
    Asset("ANIM", "anim/grassgecko_green_build.zip"),
    Asset("ANIM", "anim/grassgreen_build.zip"),
    Asset("ANIM", "anim/cutgrassgreen.zip"),
    Asset("ANIM", "anim/log_tropical.zip"),
    Asset("ANIM", "anim/butterfly_tropical_basic.zip"),
    Asset("ANIM", "anim/butterfly_tropical_wings.zip"),
    Asset("ANIM", "anim/bananas.zip"),
    Asset("ANIM", "anim/resurrection_stone_sw.zip"),
    Asset("ANIM", "anim/flare_large_blubber.zip"),

    --for strong wind gust
    --not loaded lazy because they're almost guaranteed to load immediately anyways -M
    Asset("ANIM", "anim/floatable_anims/grass.zip"),
    Asset("ANIM", "anim/floatable_anims/grass1.zip"),
    Asset("ANIM", "anim/floatable_anims/sapling.zip"),

    --for flooding
    Asset("IMAGE", "images/flood_particle.tex"),
    Asset("SHADER", "shaders/floodstate.ksh"),
}

AddMinimapAtlas("images/minimap/ia_minimap.xml")

local replaceAnims = { --these few could probably be loaded parallel -M
    "tree_leaf_normal",
    "evergreen_short_normal",
    "evergreen_tall_old",
}

local REPLACE_ANIM = {}

for _, v in ipairs(replaceAnims) do
    --table.insert(Assets, Asset("ANIM", "anim/floatable_anims/" .. v .. ".zip"))
    REPLACE_ANIM["anim/" .. v .. ".zip"] = "anim/floatable_anims/" .. v .. ".zip"
end

local _RegisterPrefabsImpl = GLOBAL.RegisterPrefabsImpl
GLOBAL.RegisterPrefabsImpl = function(prefab, ...)
    for _, asset in ipairs(prefab.assets) do
        if REPLACE_ANIM[asset.file] then
            asset.file = REPLACE_ANIM[asset.file]
        end
    end
    _RegisterPrefabsImpl(prefab, ...)
end

if not GLOBAL.TheNet:IsDedicated() then
    table.insert(Assets, Asset("SOUND", "sound/ia_amb_hurricane.fsb"))
    table.insert(Assets, Asset("SOUND", "sound/ia_amb_rain.fsb"))
    table.insert(Assets, Asset("SOUND", "sound/ia_amb_green.fsb"))
    table.insert(Assets, Asset("SOUND", "sound/ia_amb_mild.fsb"))
    table.insert(Assets, Asset("SOUND", "sound/ia_amb_wet.fsb"))
    table.insert(Assets, Asset("SOUND", "sound/ia_amb_dry.fsb"))
    table.insert(Assets, Asset("SOUND", "sound/ia_amb_misc.fsb"))
    table.insert(Assets, Asset("SOUND", "sound/ia_creature.fsb"))
    table.insert(Assets, Asset("SOUND", "sound/ia_music.fsb"))
    table.insert(Assets, Asset("SOUND", "sound/ia_sfx.fsb"))
    table.insert(Assets, Asset("SOUND", "sound/ia_sfx_lp.fsb"))
    table.insert(Assets, Asset("SOUND", "sound/ia_voice.fsb"))
    table.insert(Assets, Asset("SOUND", "sound/ia_volcano.fsb"))
    table.insert(Assets, Asset("SOUNDPACKAGE", "sound/ia.fev"))

    -- table.insert(Assets, Asset("SOUND", "sound/common.fsb"))
    -- table.insert(Assets, Asset("SOUNDPACKAGE", "sound/dontstarve.fev"))
    -- These are loaded by the characters directly once needed
    table.insert(Assets, Asset("SOUND", "sound/ia_walani.fsb"))
    -- table.insert(Assets, Asset("SOUND", "sound/ia_warly.fsb"))
    table.insert(Assets, Asset("SOUND", "sound/ia_wilbur.fsb"))
    table.insert(Assets, Asset("SOUND", "sound/ia_woodlegs.fsb"))
end
